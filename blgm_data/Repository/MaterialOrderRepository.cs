using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using blgm_data.models;
using blgm_data.views;
using Microsoft.EntityFrameworkCore;

namespace blgm_data.Repository
{
    public class MaterialOrderRepository
    {
        private BLGMDbContext context;
        public MaterialOrderRepository(BLGMDbContext context)
        {
            this.context = context;
        }

        public async Task<List<MaterialOrder>> GetAllAsync()
        {
            var query = this.context.MaterialOrders
                .Include(x => x.OrderLine)
                .ThenInclude(x => x.Material)
                .Include(x => x.Customer)
                .Where(x => x.IsActive == true);
            return await query.ToListAsync();
        }

        public async Task<List<MaterialOrder>> Search(SearchView search)
        {
            DateTime? completeTime = null;
            if(search.OrderedCompleteDateObj != null) 
                completeTime = DateTime.ParseExact(string.Format("{0:00}", search.OrderedCompleteDateObj.Day) + "/" + string.Format("{0:00}", search.OrderedCompleteDateObj.Month) + "/" + search.OrderedCompleteDateObj.Year, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            var query = this.context.MaterialOrders
                .Include(x => x.OrderLine)
                .ThenInclude(x => x.Material)
                .Include(x => x.Customer)
                .Where(x => x.IsActive == true
                    && (x.CustomerId == search.CustomerId || search.CustomerId == 0)
                    && (x.OrderedCompleteDate == completeTime || completeTime == null)
                    && (x.IsRecive == search.IsRecive || search.IsRecive == null)
                    && (x.IsComplete == search.IsComplete || search.IsComplete == null));
            return await query.ToListAsync();
        }

        public async Task<List<MaterialOrder>> GetAllCompleteToday()
        {
            var query = this.context.MaterialOrders
                .Include(x => x.OrderLine)
                .ThenInclude(x => x.Material)
                .Include(x => x.Customer)
                .Where(x => x.IsActive == true && x.OrderedCompleteDate.Date == DateTime.Now.Date);
            return await query.ToListAsync();
        }

        public async Task<MaterialOrder> Get(int id)
        {
            var data = await this.context.MaterialOrders
                .Include(x => x.OrderLine)
                .ThenInclude(x => x.Material)
                .Where(x => x.Id == id)
                .FirstOrDefaultAsync();
            return await this.context.MaterialOrders
                .Include(x => x.OrderLine)
                .ThenInclude(x => x.Material)
                .Where(x => x.Id == id)
                .Select(chOr => new MaterialOrder
                {
                    Id = chOr.Id,
                    IsComplete = chOr.IsComplete,
                    OrderedCompleteDateObj = new Date { Day = chOr.OrderedCompleteDate.Day, Month = chOr.OrderedCompleteDate.Month, Year = chOr.OrderedCompleteDate.Year },
                    CustomerId = chOr.CustomerId,
                    OrderLine = chOr.OrderLine,
                    IsRecive = chOr.IsRecive,
                    Amount = chOr.Amount,
                    Reason = chOr.Reason,
                })
                .FirstOrDefaultAsync();
        }

        public async Task<MaterialOrder> CreateAsync(MaterialOrder data)
        {
            var orderLines = data.OrderLine;
            data.OrderLine = null;
            data.IsActive = true;
            data.OrderedCompleteDate = DateTime.ParseExact(string.Format("{0:00}", data.OrderedCompleteDateObj.Day) + "/" + string.Format("{0:00}", data.OrderedCompleteDateObj.Month) + "/" + data.OrderedCompleteDateObj.Year, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            this.context.MaterialOrders.Add(data);
            await this.context.SaveChangesAsync();
            foreach (var orderLine in orderLines)
            {
                orderLine.MaterialOrderId = data.Id;
                var material = this.context.Materials.Where(x => x.Id == orderLine.MaterialId).FirstOrDefault();
                var quantity = orderLine.Quantity;
                if (!data.IsRecive)
                {
                    quantity = quantity * -1;
                }
                if (data.IsComplete)
                {
                    var his = new MaterialHistory
                    {
                        MaterialId = material.Id,
                        Quantity = quantity,
                        Reference = "OrderId. " + data.Id,
                        Reason = null,
                        CreatedBy = data.CreatedBy,
                        CreatedDate = data.CreatedDate
                    };
                    material.Quantity = material.Quantity + quantity;
                    material.UpdatedDate = data.UpdatedDate;
                    material.UpdatedBy = data.UpdatedBy;
                    this.context.MaterialHistories.Add(his);
                }

                this.context.MaterialOrderLines.Add(orderLine);
            }

            await this.context.SaveChangesAsync();

            return data;
        }

        public async Task<MaterialOrder> UpdateAsync(MaterialOrder data)
        {
            var order = this.context.MaterialOrders.Where(x => x.Id == data.Id).FirstOrDefault();
            order.OrderedCompleteDate = DateTime.ParseExact(string.Format("{0:00}", data.OrderedCompleteDateObj.Day) + "/" + string.Format("{0:00}", data.OrderedCompleteDateObj.Month) + "/" + data.OrderedCompleteDateObj.Year, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            order.CustomerId = data.CustomerId;
            order.Reason = data.Reason;
            order.UpdatedDate = data.UpdatedDate;
            order.UpdatedBy = data.UpdatedBy;
            order.Amount = data.Amount;
            var orderLineId = data.OrderLine.Select(x => x.Id).ToList();
            var deleteOrderLine = await this.context.MaterialOrderLines.Where(x => !orderLineId.Contains(x.Id) && x.MaterialOrderId == data.Id).ToListAsync();
            var newOrderLine = data.OrderLine.Where(x => x.Id == 0).ToList();
            var editOrderLine = data.OrderLine.Where(x => x.Id != 0).ToList();

            foreach (var orderLine in newOrderLine)
            {
                orderLine.MaterialOrderId = data.Id;
                var material = this.context.Materials.Where(x => x.Id == orderLine.MaterialId).FirstOrDefault();
                var quantity = orderLine.Quantity;
                if (!order.IsRecive)
                {
                    quantity = quantity * -1;
                }
                if (order.IsComplete)
                {
                    var his = new MaterialHistory
                    {
                        MaterialId = material.Id,
                        Quantity = quantity,
                        Reference = "OrderId. " + data.Id,
                        Reason = null,
                        CreatedBy = data.UpdatedBy,
                        CreatedDate = data.UpdatedDate
                    };
                    material.Quantity = material.Quantity + quantity;
                    material.UpdatedDate = data.UpdatedDate;
                    material.UpdatedBy = data.UpdatedBy;
                    this.context.MaterialHistories.Add(his);
                }

                this.context.MaterialOrderLines.Add(orderLine);
            }
            await this.context.SaveChangesAsync();

            foreach (var orderLine in editOrderLine)
            {
                orderLine.MaterialOrderId = data.Id;
                var material = this.context.Materials.Where(x => x.Id == orderLine.MaterialId).FirstOrDefault();
                var oldOrderLine = await this.context.MaterialOrderLines.Where(x => x.Id == orderLine.Id).FirstOrDefaultAsync();
                var quantity = orderLine.Quantity - oldOrderLine.Quantity;
                if (!order.IsRecive)
                {
                    quantity = quantity * -1;
                }
                if (order.IsComplete)
                {
                    if (quantity != 0)
                    {
                        var his = new MaterialHistory
                        {
                            MaterialId = material.Id,
                            Quantity = quantity,
                            Reference = "OrderId. " + data.Id,
                            Reason = "แก้ไข Order(แก้ไข)",
                            CreatedBy = data.UpdatedBy,
                            CreatedDate = data.UpdatedDate
                        };
                        material.Quantity = material.Quantity + quantity;
                        material.UpdatedDate = data.UpdatedDate;
                        material.UpdatedBy = data.UpdatedBy;
                        this.context.MaterialHistories.Add(his);
                    }
                }
                oldOrderLine.Quantity = orderLine.Quantity;
                oldOrderLine.TotalPrice = orderLine.TotalPrice;
                oldOrderLine.Price = orderLine.Price;
            }
            await this.context.SaveChangesAsync();

            foreach (var orderLine in deleteOrderLine)
            {
                orderLine.MaterialOrderId = data.Id;
                var material = this.context.Materials.Where(x => x.Id == orderLine.MaterialId).FirstOrDefault();
                var quantity = orderLine.Quantity;
                if (order.IsRecive)
                {
                    quantity = quantity * -1;
                }
                if (order.IsComplete)
                {
                    var his = new MaterialHistory
                    {
                        MaterialId = material.Id,
                        Quantity = quantity,
                        Reference = "OrderId. " + data.Id,
                        Reason = "แก้ไข Order(ยกเลิก)",
                        CreatedBy = data.UpdatedBy,
                        CreatedDate = data.UpdatedDate
                    };
                    material.Quantity = material.Quantity + quantity;
                    material.UpdatedDate = data.UpdatedDate;
                    material.UpdatedBy = data.UpdatedBy;
                    this.context.MaterialHistories.Add(his);
                }
                this.context.MaterialOrderLines.Remove(orderLine);
            }
            await this.context.SaveChangesAsync();

            return data;
        }

        public async Task<MaterialOrder> SetCompleteOrder(int id, string user)
        {
            var order = this.context.MaterialOrders.Where(x => x.Id == id).FirstOrDefault();
            order.IsComplete = true;
            order.UpdatedBy = user;
            order.UpdatedDate = DateTime.Now;
            var orderLines = await this.context.MaterialOrderLines.Where(x => x.MaterialOrderId == id).ToListAsync();
            foreach (var orderLine in orderLines)
            {
                var material = this.context.Materials.Where(x => x.Id == orderLine.MaterialId).FirstOrDefault();
                var quantity = orderLine.Quantity;
                if (!order.IsRecive)
                {
                    quantity = quantity * -1;
                }
                if (order.IsComplete)
                {
                    var his = new MaterialHistory
                    {
                        MaterialId = orderLine.MaterialId,
                        Quantity = quantity,
                        Reference = "OrderId. " + order.Id,
                        CreatedBy = order.UpdatedBy,
                        CreatedDate = order.UpdatedDate
                    };
                    material.Quantity = material.Quantity + quantity;
                    material.UpdatedDate = order.UpdatedDate;
                    material.UpdatedBy = order.UpdatedBy;
                    this.context.MaterialHistories.Add(his);
                }
            }
            await this.context.SaveChangesAsync();
            return order;
        }

        public async Task<MaterialOrder> CancelOrder(int id, string user)
        {
            var order = this.context.MaterialOrders.Where(x => x.Id == id).FirstOrDefault();
            order.IsActive = false;
            order.UpdatedBy = user;
            order.UpdatedDate = DateTime.Now;
            var orderLines = await this.context.MaterialOrderLines.Where(x => x.MaterialOrderId == id).ToListAsync();
            foreach (var orderLine in orderLines)
            {
                var material = this.context.Materials.Where(x => x.Id == orderLine.MaterialId).FirstOrDefault();
                var quantity = orderLine.Quantity;
                if (order.IsRecive)
                {
                    quantity = quantity * -1;
                }
                if (order.IsComplete)
                {
                    var his = new MaterialHistory
                    {
                        MaterialId = orderLine.MaterialId,
                        Quantity = quantity,
                        Reference = "Cancel OrderId. " + order.Id,
                        CreatedBy = order.UpdatedBy,
                        CreatedDate = order.UpdatedDate
                    };
                    material.Quantity = material.Quantity + quantity;
                    material.UpdatedDate = order.UpdatedDate;
                    material.UpdatedBy = order.UpdatedBy;
                    this.context.MaterialHistories.Add(his);
                }
            }
            await this.context.SaveChangesAsync();
            return order;
        }
    }
}