using System.Net.Mime;
using System;
using System.Reflection.Emit;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using blgm_data.models;
using Microsoft.EntityFrameworkCore;
using blgm_data.views;

namespace blgm_data.Repository
{
    public class ProductRepository
    {
        private BLGMDbContext context;
        public ProductRepository(BLGMDbContext context)
        {
            this.context = context;
        }

        public async Task<List<Product>> GetAllAsync()
        {
            var data = await this.context.Products
                .Include(x => x.ProductMaterial)
                .ThenInclude(x => x.Material)
                .Include(x => x.ProductPrice)
                .ThenInclude(x => x.Customer)
                .Include(x => x.ProductImage)
                .Where(x => x.IsActive == true).ToListAsync();
            foreach (var item in data)
            {
                item.WaitingImport = (from ol in this.context.ProductOrderLines
                                    join o in this.context.ProductOrders on ol.ProductOrderId equals o.Id
                                    where ol.ProductId == item.Id && o.IsActive == true && o.IsComplete == false && o.IsRecive == true
                                    select ol.Quantity
                                    ).Sum();
                item.WaitingExport = (from ol in this.context.ProductOrderLines
                                    join o in this.context.ProductOrders on ol.ProductOrderId equals o.Id
                                    where ol.ProductId == item.Id && o.IsActive == true && o.IsComplete == false && o.IsRecive == false
                                    select ol.Quantity
                                    ).Sum();
            }
            return data;
        }

        public async Task<List<Product>> GetAllAsync(Product search)
        {
            var data = await this.context.Products
                .Include(x => x.ProductMaterial)
                .ThenInclude(x => x.Material)
                .Include(x => x.ProductPrice)
                .ThenInclude(x => x.Customer)
                .Include(x => x.ProductImage)
                .Where(x => x.IsActive == true
                    && (x.Name.Contains(search.Name) || String.IsNullOrEmpty(search.Name))
                    && (x.Code.Contains(search.Code) || String.IsNullOrEmpty(search.Code))
                    && (x.Size.Contains(search.Size) || String.IsNullOrEmpty(search.Size))).ToListAsync();
            foreach (var item in data)
            {
                item.WaitingImport = (from ol in this.context.ProductOrderLines
                                    join o in this.context.ProductOrders on ol.ProductOrderId equals o.Id
                                    where ol.ProductId == item.Id && o.IsActive == true && o.IsComplete == false && o.IsRecive == true
                                    select ol.Quantity
                                    ).Sum();
                item.WaitingExport = (from ol in this.context.ProductOrderLines
                                    join o in this.context.ProductOrders on ol.ProductOrderId equals o.Id
                                    where ol.ProductId == item.Id && o.IsActive == true && o.IsComplete == false && o.IsRecive == false
                                    select ol.Quantity
                                    ).Sum();
            }
            return data;
        }

        public async Task<List<Product>> GetAllOutOfStock()
        {
            var data = await this.context.Products
                .Include(x => x.ProductMaterial)
                .ThenInclude(x => x.Material)
                .Include(x => x.ProductPrice)
                .ThenInclude(x => x.Customer)
                .Include(x => x.ProductImage)
                .Where(x => x.IsActive == true && x.Quantity < 100).ToListAsync();
            foreach (var item in data)
            {
                item.WaitingImport = (from ol in this.context.ProductOrderLines
                                    join o in this.context.ProductOrders on ol.ProductOrderId equals o.Id
                                    where ol.ProductId == item.Id && o.IsActive == true && o.IsComplete == false && o.IsRecive == true
                                    select ol.Quantity
                                    ).Sum();
                item.WaitingExport = (from ol in this.context.ProductOrderLines
                                    join o in this.context.ProductOrders on ol.ProductOrderId equals o.Id
                                    where ol.ProductId == item.Id && o.IsActive == true && o.IsComplete == false && o.IsRecive == false
                                    select ol.Quantity
                                    ).Sum();
            }
            return data;
        }

        public async Task<Product> Get(int id)
        {
            return await this.context.Products
                .Include(x => x.ProductMaterial)
                .ThenInclude(x => x.Material)
                .Include(x => x.ProductPrice)
                .ThenInclude(x => x.Customer)
                .Include(x => x.ProductImage).Where(x => x.Id == id).FirstOrDefaultAsync();
        }

        public async Task<int> GetMaxQuantityValue(int id,int orderId)
        {
            var package = await this.context.Products.Where(x => x.Id == id).FirstOrDefaultAsync();
            var bookingPackage = this.context.ProductOrderLines
                    .Include(x => x.ProductOrder)
                    .Where(x => x.ProductId == id && x.ProductId != orderId
                        && x.ProductOrder.IsComplete == false
                        && x.ProductOrder.IsRecive == false
                        && x.ProductOrder.IsActive == true).Sum(x => x.Quantity);

            return package.Quantity - bookingPackage;
        }

        public async Task<Product> CreateAsync(Product data)
        {
            var productPrices = data.ProductPrice;
            var productMaterials = data.ProductMaterial;
            data.ProductMaterial = null;
            data.ProductPrice = null;
            data.IsActive = true;
            this.context.Products.Add(data);
            await this.context.SaveChangesAsync();

            var his = new ProductHistory
            {
                ProductId = data.Id,
                Quantity = data.Quantity,
                Reason = "สร้างข้อมูลใหม่",
                CreatedBy = data.UpdatedBy,
                CreatedDate = data.UpdatedDate
            };
            this.context.ProductHistories.Add(his);
            await this.context.SaveChangesAsync();

            foreach (var productPrice in productPrices)
            {
                var newProductPrice = new ProductPrice
                {
                    CustomerId = productPrice.CustomerId,
                    ProductId = data.Id,
                    Price = productPrice.Price,
                    Type = productPrice.Type
                };
                this.context.ProductPrices.Add(newProductPrice);
            }
            await this.context.SaveChangesAsync();

            foreach (var productMaterial in productMaterials)
            {
                var newProductMaterial = new ProductMaterial
                {
                    ProductId = data.Id,
                    MaterialId = productMaterial.MaterialId,
                    Quantity = productMaterial.Quantity
                };
                this.context.ProductMaterials.Add(newProductMaterial);
            }
            await this.context.SaveChangesAsync();

            return data;
        }

        public async Task<Product> UpdateAsync(Product data)
        {
            var product = await this.context.Products.Where(x => x.Id == data.Id).FirstOrDefaultAsync();
            if (product != null)
            {
                var changeValue = product.Quantity - data.Quantity;
                if(changeValue != 0) {
                    var productHist = new ProductHistory
                    {
                        ProductId = data.Id,
                        Quantity = changeValue,
                        Reason = "แก้ไขข้อมูล",
                        CreatedBy = data.UpdatedBy,
                        CreatedDate = data.UpdatedDate
                    };
                    this.context.ProductHistories.Add(productHist);
                    await this.context.SaveChangesAsync();
                }
                product.Code = data.Code;
                product.Name = data.Name;
                product.CratfTime = data.CratfTime;
                product.Size = data.Size;
                product.Width = data.Width;
                product.Height = data.Height;
                product.Length = data.Length;
                product.Weight = data.Weight;
                product.Cost = data.Cost;
                product.Price = data.Price;
                product.Quantity = data.Quantity;
                await this.context.SaveChangesAsync();

                //image
                var productImages = this.context.ProductImages.Where(x => data.DeleteImage.Contains(x.Id)).ToList();
                this.context.ProductImages.RemoveRange(productImages);
                await this.context.SaveChangesAsync();

                //Material
                var materialIds = data.ProductMaterial.Select(x => x.Id).ToList();
                var deleteMaterialIds = await this.context.ProductMaterials
                    .Where(x => !materialIds.Contains(x.Id) && x.ProductId == data.Id)
                    .Select(x => x.Id)
                    .ToListAsync();
                var newMaterial = data.ProductMaterial.Where(x => x.Id == 0).ToList();
                var editMaterial = data.ProductMaterial.Where(x => x.Id != 0).ToList();
                //New Mat
                foreach (var mat in newMaterial)
                {
                    var newProductMaterial = new ProductMaterial
                    {
                        ProductId = data.Id,
                        MaterialId = mat.MaterialId,
                        Quantity = mat.Quantity
                    };
                    this.context.ProductMaterials.Add(newProductMaterial);
                }
                await this.context.SaveChangesAsync();
                //edit material
                foreach (var editMat in editMaterial)
                {
                    var mat = this.context.ProductMaterials.Where(x => x.Id == editMat.Id).FirstOrDefault();
                    if (mat != null)
                    {
                        mat.MaterialId = editMat.MaterialId;
                        mat.Quantity = editMat.Quantity;
                        await this.context.SaveChangesAsync();
                    }
                }

                //delete material
                var deleteMats = this.context.ProductMaterials.Where(x => deleteMaterialIds.Contains(x.Id)).ToList();
                this.context.ProductMaterials.RemoveRange(deleteMats);
                await this.context.SaveChangesAsync();

                //Price
                var priceIds = data.ProductPrice.Select(x => x.Id).ToList();
                var deletePriceIds = await this.context.ProductPrices
                    .Where(x => !priceIds.Contains(x.Id) && x.ProductId == data.Id)
                    .Select(x => x.Id)
                    .ToListAsync();
                var newPrice = data.ProductPrice.Where(x => x.Id == 0).ToList();
                var editPrice = data.ProductPrice.Where(x => x.Id != 0).ToList();
                //new price
                foreach (var price in newPrice)
                {
                    var newProductPrice = new ProductPrice
                    {
                        CustomerId = price.CustomerId,
                        ProductId = data.Id,
                        Price = price.Price,
                        Type = price.Type
                    };
                    this.context.ProductPrices.Add(newProductPrice);
                }
                await this.context.SaveChangesAsync();

                foreach (var edit in editPrice)
                {
                    var price = this.context.ProductPrices.Where(x => x.Id == edit.Id).FirstOrDefault();
                    if (price != null)
                    {
                        price.CustomerId = edit.CustomerId;
                        price.Price = edit.Price;
                        await this.context.SaveChangesAsync();
                    }
                }

                var deletePrices = this.context.ProductPrices.Where(x => deletePriceIds.Contains(x.Id)).ToList();
                this.context.ProductPrices.RemoveRange(deletePrices);
                await this.context.SaveChangesAsync();
            }
            return data;
        }

        public async Task UploadImage(int productId, byte[] image)
        {
            var productImage = new ProductImage
            {
                ProductId = productId,
                Image = image,
            };

            this.context.ProductImages.Add(productImage);
            await this.context.SaveChangesAsync();
        }

        public async Task<Chemical> UpdateAsync(Chemical data)
        {
            var chemical = await this.context.Chemicals.Where(x => x.Id == data.Id).FirstOrDefaultAsync();
            var changeValue = data.Quantity - chemical.Quantity;
            if (changeValue != 0)
            {
                var his = new ChemicalHistory
                {
                    ChemicalId = data.Id,
                    Quantity = changeValue,
                    Reason = "แก้ไขข้อมูล",
                    CreatedBy = data.UpdatedBy,
                    CreatedDate = data.UpdatedDate
                };
                this.context.ChemicalHistories.Add(his);
            }
            if (chemical != null)
            {
                chemical.Name = data.Name;
                chemical.Code = data.Code;
                chemical.Color = data.Color;
                chemical.Size = data.Size;
                chemical.Cost = data.Cost;
                chemical.Price = data.Price;
                chemical.Quantity = data.Quantity;
                chemical.UpdatedBy = data.UpdatedBy;
                chemical.UpdatedDate = data.UpdatedDate;
            }
            this.context.SaveChanges();

            return data;
        }

        public async Task<Product> Delete(int id, string user)
        {
            var data = await this.context.Products.Where(x => x.Id == id).FirstOrDefaultAsync();
            if (data != null)
            {
                data.IsActive = false;
                data.UpdatedBy = user;
                data.UpdatedDate = DateTime.Now;
                await this.context.SaveChangesAsync();
            }

            return data;

        }

    }
}