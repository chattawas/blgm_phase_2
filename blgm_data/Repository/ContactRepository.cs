﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using blgm_data;
using blgm_data.models;
using Microsoft.EntityFrameworkCore;

namespace blgm_data.Repository
{
    public class ContactRepository
    {
        private BLGMDbContext context;
        public ContactRepository(BLGMDbContext context)
        {
            this.context = context;
        }

        public Contact AddContact(Contact data)
        {
            data.CreatedDate = DateTime.Now;
            this.context.Contacts.Add(data);
            this.context.SaveChanges();
            return data;
        }
        
        public async Task<List<Contact>> GetAll()
        {
            return await this.context.Contacts.OrderByDescending(x => x.CreatedDate).ToListAsync();
        }
    }
}
