using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using blgm_data.models;
using Microsoft.EntityFrameworkCore;

namespace blgm_data.Repository
{
    public class MaterialHistoryRepository
    {
        private BLGMDbContext context;
        public MaterialHistoryRepository(BLGMDbContext context)
        {
            this.context = context;
        }

        public async Task<List<MaterialHistory>> GetAll(int id)
        {
            var query = this.context.MaterialHistories.Where(x => x.MaterialId == id).OrderByDescending(x => x.CreatedDate);
            return await query.ToListAsync();
        }
    }
}