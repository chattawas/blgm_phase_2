using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using blgm_data.models;
using blgm_data.views;
using Microsoft.EntityFrameworkCore;

namespace blgm_data.Repository
{
    public class ProductOrderRepository
    {
        private BLGMDbContext context;
        public ProductOrderRepository(BLGMDbContext context)
        {
            this.context = context;
        }

        public async Task<List<ProductOrder>> GetAllAsync()
        {
            var query = this.context.ProductOrders
                .Include(x => x.ProductOrderLines)
                .ThenInclude(x => x.Product)
                .Include(x => x.ProductOrderLines)
                .ThenInclude(x => x.MaterialOrderLines)
                .ThenInclude(x => x.Material)
                .Include(x => x.Customer)
                .Where(x => x.IsActive == true);
            return await query.ToListAsync();
        }

        public async Task<List<ProductOrder>> GetAllAsync(SearchView search)
        {
             DateTime? completeTime = null;
            if(search.OrderedCompleteDateObj != null) 
                completeTime = DateTime.ParseExact(string.Format("{0:00}", search.OrderedCompleteDateObj.Day) + "/" + string.Format("{0:00}", search.OrderedCompleteDateObj.Month) + "/" + search.OrderedCompleteDateObj.Year, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            
            var query = this.context.ProductOrders
                .Include(x => x.ProductOrderLines)
                .ThenInclude(x => x.Product)
                .Include(x => x.ProductOrderLines)
                .ThenInclude(x => x.MaterialOrderLines)
                .ThenInclude(x => x.Material)
                .Include(x => x.Customer)
                .Where(x => x.IsActive == true
                    && (x.CustomerId == search.CustomerId || search.CustomerId == 0)
                    && (x.OrderedCompleteDate == completeTime || completeTime == null)
                    && (x.IsRecive == search.IsRecive || search.IsRecive == null)
                    && (x.IsComplete == search.IsComplete || search.IsComplete == null));
            return await query.ToListAsync();
        }

        public async Task<List<ProductOrder>> GetAllCompleteToday()
        {
            var query = this.context.ProductOrders
                .Include(x => x.ProductOrderLines)
                .ThenInclude(x => x.Product)
                .Include(x => x.ProductOrderLines)
                .ThenInclude(x => x.MaterialOrderLines)
                .ThenInclude(x => x.Material)
                .Include(x => x.Customer)
                .Where(x => x.IsActive == true && x.OrderedCompleteDate.Date == DateTime.Now.Date);
            return await query.ToListAsync();
        }

        public async Task<ProductOrder> Get(int id)
        {
            var data = await this.context.ProductOrders
                .Include(x => x.ProductOrderLines)
                .ThenInclude(x => x.Product)
                .ThenInclude(x => x.ProductMaterial)
                .Include(x => x.ProductOrderLines)
                .ThenInclude(x => x.MaterialOrderLines)
                .ThenInclude(x => x.Material)
                .Include(x => x.Customer)
                .Where(x => x.Id == id)
                .FirstOrDefaultAsync();
            data.OrderedCompleteDateObj = new Date { Day = data.OrderedCompleteDate.Day, Month = data.OrderedCompleteDate.Month, Year = data.OrderedCompleteDate.Year };
            return data;
        }

        public async Task<ProductOrder> CreateAsync(ProductOrder data)
        {
            var productOrderLines = data.ProductOrderLines;
            data.ProductOrderLines = null;
            data.IsActive = true;
            data.OrderedCompleteDate = DateTime.ParseExact(string.Format("{0:00}", data.OrderedCompleteDateObj.Day) + "/" + string.Format("{0:00}", data.OrderedCompleteDateObj.Month) + "/" + data.OrderedCompleteDateObj.Year, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            this.context.ProductOrders.Add(data);
            await this.context.SaveChangesAsync();
            foreach (var productOrderLine in productOrderLines)
            {
                productOrderLine.ProductOrderId = data.Id;
                var materialOrderLines = productOrderLine.MaterialOrderLines;
                productOrderLine.MaterialOrderLines = null;
                var product = this.context.Products.Where(x => x.Id == productOrderLine.ProductId).FirstOrDefault();
                var quantity = productOrderLine.Quantity;
                if (!data.IsRecive)
                {
                    quantity = quantity * -1;
                }
                if (data.IsComplete)
                {
                    var his = new ProductHistory
                    {
                        ProductId = product.Id,
                        Quantity = quantity,
                        Reference = "OrderId. " + data.Id,
                        Reason = null,
                        CreatedBy = data.CreatedBy,
                        CreatedDate = data.CreatedDate
                    };
                    product.Quantity = product.Quantity + quantity;
                    product.UpdatedDate = data.UpdatedDate;
                    product.UpdatedBy = data.UpdatedBy;
                    this.context.ProductHistories.Add(his);
                }
                this.context.ProductOrderLines.Add(productOrderLine);
                await this.context.SaveChangesAsync();

                foreach (var materialOrderLine in materialOrderLines)
                {
                    materialOrderLine.ProductOrderLineId = productOrderLine.Id;
                    var material = this.context.Materials.Where(x => x.Id == materialOrderLine.MaterialId).FirstOrDefault();
                    var matQuantity = materialOrderLine.Quantity;
                    if (!data.IsRecive)
                    {
                        matQuantity = matQuantity * -1;
                    }
                    if (data.IsComplete)
                    {
                        var his = new MaterialHistory
                        {
                            MaterialId = material.Id,
                            Quantity = quantity,
                            Reference = "OrderId. " + data.Id,
                            Reason = "Product Order",
                            CreatedBy = data.CreatedBy,
                            CreatedDate = data.CreatedDate
                        };
                        material.Quantity = material.Quantity + quantity;
                        material.UpdatedDate = data.UpdatedDate;
                        material.UpdatedBy = data.UpdatedBy;
                        this.context.MaterialHistories.Add(his);
                    }

                    this.context.MaterialOrderLines.Add(materialOrderLine);
                    await this.context.SaveChangesAsync();
                }
            }
            return data;
        }

        public async Task<ProductOrder> UpdateAsync(ProductOrder data)
        {
            var order = this.context.ProductOrders.Where(x => x.Id == data.Id).FirstOrDefault();
            order.OrderedCompleteDate = DateTime.ParseExact(string.Format("{0:00}", data.OrderedCompleteDateObj.Day) + "/" + string.Format("{0:00}", data.OrderedCompleteDateObj.Month) + "/" + data.OrderedCompleteDateObj.Year, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            order.CustomerId = data.CustomerId;
            order.Reason = data.Reason;
            order.UpdatedDate = data.UpdatedDate;
            order.UpdatedBy = data.UpdatedBy;
            order.Amount = data.Amount;
            await this.context.SaveChangesAsync();

            var productOrderLineId = data.ProductOrderLines.Select(x => x.Id).ToList();
            var deleteProductOrderLine = await this.context.ProductOrderLines.Where(x => !productOrderLineId.Contains(x.Id) && x.ProductOrderId == data.Id).ToListAsync();
            var newProductOrderLine = data.ProductOrderLines.Where(x => x.Id == 0).ToList();
            var editProductOrderLine = data.ProductOrderLines.Where(x => x.Id != 0).ToList();

            foreach (var productOrderLine in newProductOrderLine)
            {
                productOrderLine.ProductOrderId = data.Id;
                var materialOrderLines = productOrderLine.MaterialOrderLines;
                productOrderLine.MaterialOrderLines = null;
                var product = this.context.Products.Where(x => x.Id == productOrderLine.ProductId).FirstOrDefault();
                var quantity = productOrderLine.Quantity;
                if (!order.IsRecive)
                {
                    quantity = quantity * -1;
                }
                if (order.IsComplete)
                {
                    var his = new ProductHistory
                    {
                        ProductId = product.Id,
                        Quantity = quantity,
                        Reference = "OrderId. " + data.Id,
                        Reason = "แก้ไข Order(เพิ่มเติม)",
                        CreatedBy = data.CreatedBy,
                        CreatedDate = data.CreatedDate
                    };
                    product.Quantity = product.Quantity + quantity;
                    product.UpdatedDate = data.UpdatedDate;
                    product.UpdatedBy = data.UpdatedBy;
                    this.context.ProductHistories.Add(his);
                }
                this.context.ProductOrderLines.Add(productOrderLine);
                await this.context.SaveChangesAsync();

                foreach (var materialOrderLine in materialOrderLines)
                {
                    materialOrderLine.ProductOrderLineId = productOrderLine.Id;
                    var material = this.context.Materials.Where(x => x.Id == materialOrderLine.MaterialId).FirstOrDefault();
                    var matQuantity = materialOrderLine.Quantity;
                    if (!order.IsRecive)
                    {
                        matQuantity = matQuantity * -1;
                    }
                    if (order.IsComplete)
                    {
                        var his = new MaterialHistory
                        {
                            MaterialId = material.Id,
                            Quantity = matQuantity,
                            Reference = "OrderId. " + data.Id,
                            Reason = "แก้ไข Product Order(เพิ่มเติม)",
                            CreatedBy = data.UpdatedBy,
                            CreatedDate = data.UpdatedDate
                        };
                        material.Quantity = material.Quantity + matQuantity;
                        material.UpdatedDate = data.UpdatedDate;
                        material.UpdatedBy = data.UpdatedBy;
                        this.context.MaterialHistories.Add(his);
                    }

                    this.context.MaterialOrderLines.Add(materialOrderLine);
                    await this.context.SaveChangesAsync();
                }
            }

            foreach (var productOrderLine in editProductOrderLine)
            {
                productOrderLine.ProductOrderId = data.Id;
                var product = this.context.Products.Where(x => x.Id == productOrderLine.ProductId).FirstOrDefault();
                var oldProductOrderLine = await this.context.ProductOrderLines.Where(x => x.Id == productOrderLine.Id).FirstOrDefaultAsync();
                var productQuantity = productOrderLine.Quantity - oldProductOrderLine.Quantity;
                if (productQuantity != 0)
                {
                    if (!order.IsRecive)
                    {
                        productQuantity = productQuantity * -1;
                    }
                    if (order.IsComplete)
                    {
                        var his = new ProductHistory
                        {
                            ProductId = product.Id,
                            Quantity = productQuantity,
                            Reference = "OrderId. " + data.Id,
                            Reason = "แก้ไข Order(แก้ไข)",
                            CreatedBy = data.UpdatedBy,
                            CreatedDate = data.UpdatedDate
                        };
                        product.Quantity = product.Quantity + productQuantity;
                        product.UpdatedDate = data.UpdatedDate;
                        product.UpdatedBy = data.UpdatedBy;
                        this.context.ProductHistories.Add(his);
                    }
                    oldProductOrderLine.Quantity = productOrderLine.Quantity;
                    oldProductOrderLine.TotalPrice = productOrderLine.TotalPrice;
                    oldProductOrderLine.Price = productOrderLine.Price;
                    await this.context.SaveChangesAsync();
                    foreach (var materialOrderLine in productOrderLine.MaterialOrderLines)
                    {
                        materialOrderLine.ProductOrderLineId = productOrderLine.Id;
                        var material = this.context.Materials.Where(x => x.Id == materialOrderLine.MaterialId).FirstOrDefault();
                        var oldMaterialOrderLine = await this.context.MaterialOrderLines.Where(x => x.Id == materialOrderLine.Id).FirstOrDefaultAsync();
                        var materialQuantity = materialOrderLine.Quantity - oldMaterialOrderLine.Quantity;
                        if (!order.IsRecive)
                        {
                            materialQuantity = materialQuantity * -1;
                        }
                        if (order.IsComplete)
                        {
                            var his = new MaterialHistory
                            {
                                MaterialId = material.Id,
                                Quantity = materialQuantity,
                                Reference = "OrderId. " + data.Id,
                                Reason = "แก้ไข Product Order(แก้ไข)",
                                CreatedBy = data.UpdatedBy,
                                CreatedDate = data.UpdatedDate
                            };
                            material.Quantity = product.Quantity + materialQuantity;
                            material.UpdatedDate = data.UpdatedDate;
                            material.UpdatedBy = data.UpdatedBy;
                            this.context.MaterialHistories.Add(his);
                        }
                        oldMaterialOrderLine.Quantity = materialOrderLine.Quantity;
                        await this.context.SaveChangesAsync();
                    }
                }
            }

            foreach (var productOrderLine in deleteProductOrderLine)
            {
                var product = this.context.Products.Where(x => x.Id == productOrderLine.ProductId).FirstOrDefault();
                var productQuantity = productOrderLine.Quantity;
                if (order.IsRecive)
                {
                    productQuantity = productQuantity * -1;
                }
                if (order.IsComplete)
                {
                    var his = new ProductHistory
                    {
                        ProductId = product.Id,
                        Quantity = productQuantity,
                        Reference = "OrderId. " + data.Id,
                        Reason = "แก้ไข Order(ยกเลิก)",
                        CreatedBy = data.UpdatedBy,
                        CreatedDate = data.UpdatedDate
                    };
                    product.Quantity = product.Quantity + productQuantity;
                    product.UpdatedDate = data.UpdatedDate;
                    product.UpdatedBy = data.UpdatedBy;
                    this.context.ProductHistories.Add(his);
                }
                var deleteMaterialOrderLines = this.context.MaterialOrderLines.Where(x => x.ProductOrderLineId == productOrderLine.Id).ToList();
                foreach (var materialOrderLines in deleteMaterialOrderLines)
                {
                    var material = this.context.Materials.Where(x => x.Id == materialOrderLines.MaterialId).FirstOrDefault();
                    var materialQuantity = materialOrderLines.Quantity;
                    if (order.IsRecive)
                    {
                        materialQuantity = materialQuantity * -1;
                    }
                    if (order.IsComplete)
                    {
                        var his = new MaterialHistory
                        {
                            MaterialId = material.Id,
                            Quantity = materialQuantity,
                            Reference = "OrderId. " + data.Id,
                            Reason = "แก้ไข Product Order(ยกเลิก)",
                            CreatedBy = data.UpdatedBy,
                            CreatedDate = data.UpdatedDate
                        };
                        material.Quantity = material.Quantity + materialQuantity;
                        material.UpdatedDate = data.UpdatedDate;
                        material.UpdatedBy = data.UpdatedBy;
                        this.context.MaterialHistories.Add(his);
                    }
                    this.context.MaterialOrderLines.Remove(materialOrderLines);
                }
                this.context.ProductOrderLines.Remove(productOrderLine);
            }
            await this.context.SaveChangesAsync();

            return data;
        }

        public async Task<ProductOrder> SetCompleteOrder(int id, string user)
        {
            var order = this.context.ProductOrders.Where(x => x.Id == id).FirstOrDefault();
            order.IsComplete = true;
            order.UpdatedBy = user;
            order.UpdatedDate = DateTime.Now;
            await this.context.SaveChangesAsync();

            var productOrderLines = await this.context.ProductOrderLines.Where(x => x.ProductOrderId == id).ToListAsync();
            foreach (var productOrderLine in productOrderLines)
            {
                var product = this.context.Products.Where(x => x.Id == productOrderLine.ProductId).FirstOrDefault();
                var productQuantity = productOrderLine.Quantity;
                if (!order.IsRecive)
                {
                    productQuantity = productQuantity * -1;
                }
                if (order.IsComplete)
                {
                    var his = new ProductHistory
                    {
                        ProductId = productOrderLine.ProductId,
                        Quantity = productQuantity,
                        Reference = "OrderId. " + order.Id,
                        CreatedBy = order.UpdatedBy,
                        CreatedDate = order.UpdatedDate
                    };
                    product.Quantity = product.Quantity + productQuantity;
                    product.UpdatedDate = order.UpdatedDate;
                    product.UpdatedBy = order.UpdatedBy;
                    this.context.ProductHistories.Add(his);
                }
                await this.context.SaveChangesAsync();

                var materialOrderLines = await this.context.MaterialOrderLines.Where(x => x.ProductOrderLineId == productOrderLine.Id).ToListAsync();
                foreach (var materialOrderLine in materialOrderLines)
                {
                    var material = this.context.Materials.Where(x => x.Id == materialOrderLine.MaterialId).FirstOrDefault();
                    var materialQuantity = materialOrderLine.Quantity;
                    if (!order.IsRecive)
                    {
                        materialQuantity = materialQuantity * -1;
                    }
                    if (order.IsComplete)
                    {
                        var his = new MaterialHistory
                        {
                            MaterialId = materialOrderLine.MaterialId,
                            Quantity = materialQuantity,
                            Reference = "OrderId. " + order.Id,
                            Reason = "Product Order",
                            CreatedBy = order.UpdatedBy,
                            CreatedDate = order.UpdatedDate
                        };
                        material.Quantity = material.Quantity + materialQuantity;
                        material.UpdatedDate = order.UpdatedDate;
                        material.UpdatedBy = order.UpdatedBy;
                        this.context.MaterialHistories.Add(his);
                    }
                    await this.context.SaveChangesAsync();
                }
            }

            return order;
        }

        public async Task<ProductOrder> CancelOrder(int id, string user)
        {
            var order = this.context.ProductOrders.Where(x => x.Id == id).FirstOrDefault();
            order.IsActive = false;
            order.UpdatedBy = user;
            order.UpdatedDate = DateTime.Now;
            await this.context.SaveChangesAsync();
            var productOrderLines = await this.context.ProductOrderLines.Where(x => x.ProductOrderId == id).ToListAsync();
            foreach (var productOrderLine in productOrderLines)
            {
                var product = this.context.Products.Where(x => x.Id == productOrderLine.ProductId).FirstOrDefault();
                var productQuantity = productOrderLine.Quantity;
                if (order.IsRecive)
                {
                    productQuantity = productQuantity * -1;
                }
                if (order.IsComplete)
                {
                    var his = new ProductHistory
                    {
                        ProductId = productOrderLine.ProductId,
                        Quantity = productQuantity,
                        Reference = "Cancel OrderId. " + order.Id,
                        CreatedBy = order.UpdatedBy,
                        CreatedDate = order.UpdatedDate
                    };
                    product.Quantity = product.Quantity + productQuantity;
                    product.UpdatedDate = order.UpdatedDate;
                    product.UpdatedBy = order.UpdatedBy;
                    this.context.ProductHistories.Add(his);
                }
                await this.context.SaveChangesAsync();

                var materialOrderLines = await this.context.MaterialOrderLines.Where(x => x.ProductOrderLineId == productOrderLine.Id).ToListAsync();
                foreach (var materialOrderLine in materialOrderLines)
                {
                    var material = this.context.Materials.Where(x => x.Id == materialOrderLine.MaterialId).FirstOrDefault();
                    var materialQuantity = materialOrderLine.Quantity;
                    if (order.IsRecive)
                    {
                        materialQuantity = materialQuantity * -1;
                    }
                    if (order.IsComplete)
                    {
                        var his = new MaterialHistory
                        {
                            MaterialId = materialOrderLine.MaterialId,
                            Quantity = materialQuantity,
                            Reference = "Cancel OrderId. " + order.Id,
                            Reason = "Product Order",
                            CreatedBy = order.UpdatedBy,
                            CreatedDate = order.UpdatedDate
                        };
                        material.Quantity = material.Quantity + materialQuantity;
                        material.UpdatedDate = order.UpdatedDate;
                        material.UpdatedBy = order.UpdatedBy;
                        this.context.MaterialHistories.Add(his);
                    }
                    await this.context.SaveChangesAsync();
                }
            }
            return order;
        }
    }
}