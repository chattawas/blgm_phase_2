using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using blgm_data.models;
using Microsoft.EntityFrameworkCore;

namespace blgm_data.Repository
{
    public class ProductHistoryRepository
    {
        private BLGMDbContext context;
        public ProductHistoryRepository(BLGMDbContext context)
        {
            this.context = context;
        }

        public async Task<List<ProductHistory>> GetAll(int id)
        {
            var query = this.context.ProductHistories.Where(x => x.ProductId == id).OrderByDescending(x => x.CreatedDate);
            return await query.ToListAsync();
        }
    }
}