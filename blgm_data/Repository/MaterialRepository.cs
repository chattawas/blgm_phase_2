using System;
using System.Reflection.Emit;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using blgm_data.models;
using Microsoft.EntityFrameworkCore;

namespace blgm_data.Repository
{
    public class MaterialRepository
    {
        private BLGMDbContext context;
        public MaterialRepository(BLGMDbContext context)
        {
            this.context = context;
        }

        public async Task<List<Material>> GetAllAsync()
        {
            var data = await (from c in this.context.Materials
                              where c.IsActive == true
                              select new Material
                              {
                                  Id = c.Id,
                                  Code = c.Code,
                                  Name = c.Name,
                                  Size = c.Size,
                                  Color = c.Color,
                                  Cost = c.Cost,
                                  Price = c.Price,
                                  Quantity = c.Quantity,
                                  WaitingImport = (from ol in this.context.MaterialOrderLines
                                                   join o in this.context.MaterialOrders on ol.MaterialOrderId equals o.Id
                                                   where ol.MaterialId == c.Id && o.IsActive == true && o.IsComplete == false && o.IsRecive == true
                                                   select ol.Quantity
                                                  ).Sum(),
                                  WaitingExport = (from ol in this.context.MaterialOrderLines
                                                   join o in this.context.MaterialOrders on ol.MaterialOrderId equals o.Id
                                                   where ol.MaterialId == c.Id && o.IsActive == true && o.IsComplete == false && o.IsRecive == false
                                                   select ol.Quantity
                                                  ).Sum()
                              }).ToListAsync();
            foreach (var item in data)
            {
                var mat = this.context.MaterialOrderLines
                    .Include(x => x.ProductOrderLine)
                    .ThenInclude(x => x.ProductOrder)
                    .Where(x => x.MaterialId == item.Id
                        && x.ProductOrderLine.ProductOrder.IsActive == true
                        && x.ProductOrderLine.ProductOrder.IsComplete == false
                        && x.ProductOrderLine.ProductOrder.IsRecive == false)
                    .Sum(x => x.Quantity);
                item.WaitingExport += mat;
            }
            return data;
        }

        public async Task<List<Material>> GetAllAsync(Material search)
        {
            var data = await (from c in this.context.Materials
                              where c.IsActive == true
                                && (c.Name.Contains(search.Name) || String.IsNullOrEmpty(search.Name))
                                && (c.Code.Contains(search.Code) || String.IsNullOrEmpty(search.Code))
                                && (c.Size.Contains(search.Size) || String.IsNullOrEmpty(search.Size))
                                && (c.Color.Contains(search.Color) || String.IsNullOrEmpty(search.Color))
                              select new Material
                              {
                                  Id = c.Id,
                                  Code = c.Code,
                                  Name = c.Name,
                                  Size = c.Size,
                                  Color = c.Color,
                                  Cost = c.Cost,
                                  Price = c.Price,
                                  Quantity = c.Quantity,
                                  WaitingImport = (from ol in this.context.MaterialOrderLines
                                                   join o in this.context.MaterialOrders on ol.MaterialOrderId equals o.Id
                                                   where ol.MaterialId == c.Id && o.IsActive == true && o.IsComplete == false && o.IsRecive == true
                                                   select ol.Quantity
                                                  ).Sum(),
                                  WaitingExport = (from ol in this.context.MaterialOrderLines
                                                   join o in this.context.MaterialOrders on ol.MaterialOrderId equals o.Id
                                                   where ol.MaterialId == c.Id && o.IsActive == true && o.IsComplete == false && o.IsRecive == false
                                                   select ol.Quantity
                                                  ).Sum()
                              }).ToListAsync();
            foreach (var item in data)
            {
                var mat = this.context.MaterialOrderLines
                    .Include(x => x.ProductOrderLine)
                    .ThenInclude(x => x.ProductOrder)
                    .Where(x => x.MaterialId == item.Id
                        && x.ProductOrderLine.ProductOrder.IsActive == true
                        && x.ProductOrderLine.ProductOrder.IsComplete == false
                        && x.ProductOrderLine.ProductOrder.IsRecive == false)
                    .Sum(x => x.Quantity);
                item.WaitingExport += mat;
            }
            return data;
        }

        public async Task<List<Material>> GetAllOutOfStock()
        {
            var data = await (from c in this.context.Materials
                              where c.IsActive == true && c.Quantity < 100
                              select new Material
                              {
                                  Id = c.Id,
                                  Code = c.Code,
                                  Name = c.Name,
                                  Size = c.Size,
                                  Color = c.Color,
                                  Cost = c.Cost,
                                  Price = c.Price,
                                  Quantity = c.Quantity,
                                  WaitingImport = (from ol in this.context.MaterialOrderLines
                                                   join o in this.context.MaterialOrders on ol.MaterialOrderId equals o.Id
                                                   where ol.MaterialId == c.Id && o.IsActive == true && o.IsComplete == false && o.IsRecive == true
                                                   select ol.Quantity
                                                  ).Sum(),
                                  WaitingExport = (from ol in this.context.MaterialOrderLines
                                                   join o in this.context.MaterialOrders on ol.MaterialOrderId equals o.Id
                                                   where ol.MaterialId == c.Id && o.IsActive == true && o.IsComplete == false && o.IsRecive == false
                                                   select ol.Quantity
                                                  ).Sum()
                              }).ToListAsync();
            foreach (var item in data)
            {
                var mat = this.context.MaterialOrderLines
                    .Include(x => x.ProductOrderLine)
                    .ThenInclude(x => x.ProductOrder)
                    .Where(x => x.MaterialId == item.Id
                        && x.ProductOrderLine.ProductOrder.IsActive == true
                        && x.ProductOrderLine.ProductOrder.IsComplete == false
                        && x.ProductOrderLine.ProductOrder.IsRecive == false)
                    .Sum(x => x.Quantity);
                item.WaitingExport += mat;
            }
            return data;
        }

        public async Task<Material> Get(int id)
        {
            return await this.context.Materials.Where(x => x.Id == id).FirstOrDefaultAsync();
        }

        public async Task<int> GetMaxQuantityValue(int id, int orderId)
        {
            var package = await this.context.Materials.Where(x => x.Id == id).FirstOrDefaultAsync();
            var bookingPackage = this.context.MaterialOrderLines
                    .Include(x => x.MaterialOrder)
                    .Where(x => x.MaterialId == id && x.MaterialOrderId != orderId
                        && x.MaterialOrder.IsComplete == false
                        && x.MaterialOrder.IsRecive == false
                        && x.MaterialOrder.IsActive == true).Sum(x => x.Quantity);

            return package.Quantity - bookingPackage;
        }

        public async Task<Material> CreateAsync(Material data)
        {
            data.IsActive = true;
            this.context.Materials.Add(data);
            var his = new MaterialHistory
            {
                MaterialId = data.Id,
                Quantity = data.Quantity,
                Reason = "สร้างข้อมูลใหม่",
                CreatedBy = data.UpdatedBy,
                CreatedDate = data.UpdatedDate
            };
            this.context.MaterialHistories.Add(his);
            await this.context.SaveChangesAsync();
            return data;
        }

        public async Task<Material> UpdateAsync(Material data)
        {
            var material = await this.context.Materials.Where(x => x.Id == data.Id).FirstOrDefaultAsync();
            var changeValue = data.Quantity - material.Quantity;
            if (changeValue != 0)
            {
                var his = new MaterialHistory
                {
                    MaterialId = data.Id,
                    Quantity = changeValue,
                    Reason = "แก้ไขข้อมูล",
                    CreatedBy = data.UpdatedBy,
                    CreatedDate = data.UpdatedDate
                };
                this.context.MaterialHistories.Add(his);
            }
            if (material != null)
            {
                material.Name = data.Name;
                material.Code = data.Code;
                material.Color = data.Color;
                material.Size = data.Size;
                material.Cost = data.Cost;
                material.Price = data.Price;
                material.Quantity = data.Quantity;
                material.UpdatedBy = data.UpdatedBy;
                material.UpdatedDate = data.UpdatedDate;
            }
            this.context.SaveChanges();

            return data;
        }

        public async Task<Material> Delete(int id, string user)
        {
            var data = await this.context.Materials.Where(x => x.Id == id).FirstOrDefaultAsync();
            if (data != null)
            {
                data.IsActive = false;
                data.UpdatedBy = user;
                data.UpdatedDate = DateTime.Now;
                await this.context.SaveChangesAsync();
            }

            return data;

        }

    }
}