using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using blgm_data.models;
using blgm_data.views;
using Microsoft.EntityFrameworkCore;

namespace blgm_data.Repository
{
    public class ChemicalOrderRepository
    {
        private BLGMDbContext context;
        public ChemicalOrderRepository(BLGMDbContext context)
        {
            this.context = context;
        }

        public async Task<List<ChemicalOrder>> GetAllAsync()
        {
            var query = this.context.ChemicalOrder
                .Include(x => x.OrderLine)
                .ThenInclude(x => x.Chemical)
                .Include(x => x.Customer)
                .Where(x => x.IsActive == true);
            return await query.ToListAsync();
        }

        public async Task<List<ChemicalOrder>> GetAllAsync(SearchView search)
        {
            DateTime? completeTime = null;
            if(search.OrderedCompleteDateObj != null) 
                completeTime = DateTime.ParseExact(string.Format("{0:00}", search.OrderedCompleteDateObj.Day) + "/" + string.Format("{0:00}", search.OrderedCompleteDateObj.Month) + "/" + search.OrderedCompleteDateObj.Year, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            var query = this.context.ChemicalOrder
                .Include(x => x.OrderLine)
                .ThenInclude(x => x.Chemical)
                .Include(x => x.Customer)
                .Where(x => x.IsActive == true
                    && (x.CustomerId == search.CustomerId || search.CustomerId == 0)
                    && (x.OrderedCompleteDate == completeTime || completeTime == null)
                    && (x.IsRecive == search.IsRecive || search.IsRecive == null)
                    && (x.IsComplete == search.IsComplete || search.IsComplete == null));
            return await query.ToListAsync();
        }


        public async Task<List<ChemicalOrder>> GetAllCompleteToday()
        {
            var query = this.context.ChemicalOrder
                .Include(x => x.OrderLine)
                .ThenInclude(x => x.Chemical)
                .Include(x => x.Customer)
                .Where(x => x.IsActive == true && x.OrderedCompleteDate.Date == DateTime.Now.Date);
            return await query.ToListAsync();
        }

        public async Task<ChemicalOrder> Get(int id)
        {
            var data = await this.context.ChemicalOrder
                .Include(x => x.OrderLine)
                .ThenInclude(x => x.Chemical)
                .Where(x => x.Id == id)
                .FirstOrDefaultAsync();
            return await this.context.ChemicalOrder
                .Include(x => x.OrderLine)
                .ThenInclude(x => x.Chemical)
                .Where(x => x.Id == id)
                .Select(chOr => new ChemicalOrder{
                        Id = chOr.Id,
                        IsComplete = chOr.IsComplete,
                        OrderedCompleteDateObj = new Date { Day = chOr.OrderedCompleteDate.Day, Month = chOr.OrderedCompleteDate.Month, Year = chOr.OrderedCompleteDate.Year },
                        CustomerId = chOr.CustomerId,
                        OrderLine =  chOr.OrderLine,
                        IsRecive = chOr.IsRecive,
                        Amount = chOr.Amount,
                        Reason = chOr.Reason,
                    })
                .FirstOrDefaultAsync();
        }

        public async Task<ChemicalOrder> CreateAsync(ChemicalOrder data)
        {
            var orderLines = data.OrderLine;
            data.OrderLine = null;
            data.IsActive = true;
            data.OrderedCompleteDate = DateTime.ParseExact(string.Format("{0:00}", data.OrderedCompleteDateObj.Day) + "/" + string.Format("{0:00}", data.OrderedCompleteDateObj.Month) + "/" + data.OrderedCompleteDateObj.Year, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            this.context.ChemicalOrder.Add(data);
            await this.context.SaveChangesAsync();
            foreach (var orderLine in orderLines)
            {
                orderLine.ChemicalOrderId = data.Id;
                var chemical = this.context.Chemicals.Where(x => x.Id == orderLine.ChemicalId).FirstOrDefault();
                var quantity = orderLine.Quantity;
                if(!data.IsRecive){
                    quantity = quantity * -1;
                }
                if(data.IsComplete) {
                    var his = new ChemicalHistory
                    {
                        ChemicalId = chemical.Id,
                        Quantity = quantity,
                        Reference = "OrderId. " + data.Id ,
                        Reason = null,
                        CreatedBy = data.CreatedBy,
                        CreatedDate = data.CreatedDate
                    };
                    chemical.Quantity = chemical.Quantity + quantity;
                    chemical.UpdatedDate = data.UpdatedDate;
                    chemical.UpdatedBy = data.UpdatedBy;
                    this.context.ChemicalHistories.Add(his);
                }

                this.context.ChemicalOrderLines.Add(orderLine);
            }

            await this.context.SaveChangesAsync();

            return data;
        }

        public async Task<ChemicalOrder> UpdateAsync(ChemicalOrder data)
        {
            var order = this.context.ChemicalOrder.Where(x => x.Id == data.Id).FirstOrDefault();
            order.OrderedCompleteDate = DateTime.ParseExact(string.Format("{0:00}", data.OrderedCompleteDateObj.Day) + "/" + string.Format("{0:00}", data.OrderedCompleteDateObj.Month) + "/" + data.OrderedCompleteDateObj.Year, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            order.CustomerId = data.CustomerId;
            order.Reason = data.Reason;
            order.UpdatedBy = data.UpdatedBy;
            order.UpdatedDate = data.UpdatedDate;
            order.Amount = data.Amount;
            var orderLineId = data.OrderLine.Select(x => x.Id).ToList();
            var deleteOrderLine = await this.context.ChemicalOrderLines.Where(x => !orderLineId.Contains(x.Id) && x.ChemicalOrderId == data.Id).ToListAsync();
            var newOrderLine = data.OrderLine.Where(x => x.Id == 0).ToList();
            var editOrderLine = data.OrderLine.Where(x => x.Id != 0).ToList();

            foreach (var orderLine in newOrderLine)
            {
                orderLine.ChemicalOrderId = data.Id;
                var chemical = this.context.Chemicals.Where(x => x.Id == orderLine.ChemicalId).FirstOrDefault();
                var quantity = orderLine.Quantity;
                if(!order.IsRecive){
                    quantity = quantity * -1;
                }
                if(order.IsComplete) {
                    var his = new ChemicalHistory
                    {
                        ChemicalId = chemical.Id,
                        Quantity = quantity,
                        Reference = "OrderId. " + data.Id ,
                        Reason = null,
                        CreatedBy = data.UpdatedBy,
                        CreatedDate = data.UpdatedDate
                    };
                    chemical.Quantity = chemical.Quantity + quantity;
                    chemical.UpdatedDate = data.UpdatedDate;
                    chemical.UpdatedBy = data.UpdatedBy;
                    this.context.ChemicalHistories.Add(his);
                }

                this.context.ChemicalOrderLines.Add(orderLine);
            }
            await this.context.SaveChangesAsync();

            foreach (var orderLine in editOrderLine)
            {
                orderLine.ChemicalOrderId = data.Id;
                var chemical = this.context.Chemicals.Where(x => x.Id == orderLine.ChemicalId).FirstOrDefault();
                var oldOrderLine = await this.context.ChemicalOrderLines.Where(x => x.Id == orderLine.Id).FirstOrDefaultAsync();
                var quantity = orderLine.Quantity - oldOrderLine.Quantity;
                if(!order.IsRecive){
                    quantity = quantity * -1;
                }
                if(order.IsComplete) {
                    var his = new ChemicalHistory
                    {
                        ChemicalId = chemical.Id,
                        Quantity = quantity,
                        Reference = "OrderId. " + data.Id ,
                        Reason = "แก้ไข Order(แก้ไข)",
                        CreatedBy = data.UpdatedBy,
                        CreatedDate = data.UpdatedDate
                    };
                    chemical.Quantity = chemical.Quantity + quantity;
                    chemical.UpdatedDate = data.UpdatedDate;
                    chemical.UpdatedBy = data.UpdatedBy;
                    this.context.ChemicalHistories.Add(his);
                }
                oldOrderLine.Quantity = orderLine.Quantity;
            }
            await this.context.SaveChangesAsync();

            foreach (var orderLine in deleteOrderLine){
                orderLine.ChemicalOrderId = data.Id;
                var chemical = this.context.Chemicals.Where(x => x.Id == orderLine.ChemicalId).FirstOrDefault();
                var quantity = orderLine.Quantity;
                if(order.IsRecive){
                    quantity = quantity * -1;
                }
                if(order.IsComplete) {
                    var his = new ChemicalHistory
                    {
                        ChemicalId = chemical.Id,
                        Quantity = quantity,
                        Reference = "OrderId. " + data.Id ,
                        Reason = "แก้ไข Order(ยกเลิก)",
                        CreatedBy = data.UpdatedBy,
                        CreatedDate = data.UpdatedDate
                    };
                    chemical.Quantity = chemical.Quantity + quantity;
                    chemical.UpdatedDate = data.UpdatedDate;
                    chemical.UpdatedBy = data.UpdatedBy;
                    this.context.ChemicalHistories.Add(his);
                }
                this.context.ChemicalOrderLines.Remove(orderLine);
            }
            await this.context.SaveChangesAsync();

            return data;
        }

        public async Task<ChemicalOrder> SetCompleteOrder(int id, string user)
        {
            var order = this.context.ChemicalOrder.Where(x => x.Id == id).FirstOrDefault();
            order.IsComplete =  true;
            order.UpdatedBy = user;
            order.UpdatedDate = DateTime.Now;
            var orderLines = await this.context.ChemicalOrderLines.Where(x => x.ChemicalOrderId == id).ToListAsync();
            foreach (var orderLine in orderLines)
            {
                var chemical = this.context.Chemicals.Where(x => x.Id == orderLine.ChemicalId).FirstOrDefault();
                var quantity = orderLine.Quantity;
                if(!order.IsRecive){
                    quantity = quantity * -1;
                }
                if(order.IsComplete) {
                    var his = new ChemicalHistory
                    {
                        ChemicalId = orderLine.ChemicalId,
                        Quantity = quantity,
                        Reference = "OrderId. " + order.Id ,
                        CreatedBy = order.UpdatedBy,
                        CreatedDate = order.UpdatedDate
                    };
                    chemical.Quantity = chemical.Quantity + quantity;
                    chemical.UpdatedDate = order.UpdatedDate;
                    chemical.UpdatedBy = order.UpdatedBy;
                    this.context.ChemicalHistories.Add(his);
                }
            }
            await this.context.SaveChangesAsync();
            return order;
        }

        public async Task<ChemicalOrder> CancelOrder(int id, string user)
        {
            var order = this.context.ChemicalOrder.Where(x => x.Id == id).FirstOrDefault();
            order.IsActive =  false;
            order.UpdatedBy = user;
            order.UpdatedDate = DateTime.Now;
            var orderLines = await this.context.ChemicalOrderLines.Where(x => x.ChemicalOrderId == id).ToListAsync();
            foreach (var orderLine in orderLines)
            {
                var chemical = this.context.Chemicals.Where(x => x.Id == orderLine.ChemicalId).FirstOrDefault();
                var quantity = orderLine.Quantity;
                if(order.IsRecive){
                    quantity = quantity * -1;
                }
                if(order.IsComplete) {
                    var his = new ChemicalHistory
                    {
                        ChemicalId = orderLine.ChemicalId,
                        Quantity = quantity,
                        Reference = "Cancel OrderId. " + order.Id ,
                        CreatedBy = order.UpdatedBy,
                        CreatedDate = order.UpdatedDate
                    };
                    chemical.Quantity = chemical.Quantity + quantity;
                    chemical.UpdatedDate = order.UpdatedDate;
                    chemical.UpdatedBy = order.UpdatedBy;
                    this.context.ChemicalHistories.Add(his);
                }
            }
            await this.context.SaveChangesAsync();
            return order;
        }
    }
}