using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using blgm_data.models;
using blgm_data.views;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using Microsoft.EntityFrameworkCore;

namespace blgm_data.Repository
{
    public class CustomerRepository
    {
        private BLGMDbContext context;
        public CustomerRepository(BLGMDbContext context)
        {
            this.context = context;
        }

        public async Task<List<Customer>> GetAll()
        {
            return await this.context.Customers.Where(x => x.IsActive == true).ToListAsync();
        }

        public async Task<List<Customer>> GetAllSuppliers()
        {
            return await this.context.Customers.Where(x => x.IsActive == true && x.Type == "supplier").ToListAsync();
        }

        public async Task<List<Customer>> GetAllCoporations()
        {
            return await this.context.Customers.Where(x => x.IsActive == true && x.Type == "coporation").ToListAsync();
        }

        public async Task<Customer> Get(int id)
        {
            return await this.context.Customers
            .Where(x => x.Id == id)
            .FirstOrDefaultAsync();
        }

        public Customer Add(Customer data)
        {
            data.IsActive = true;
            this.context.Customers.Add(data);
            this.context.SaveChanges();
            return data;
        }

        public Customer Update(Customer data)
        {
            var customer = this.context.Customers.Where(x => x.Id == data.Id).FirstOrDefault();
            if (customer != null)
            {
                customer.Name = data.Name;
                customer.Phone = data.Phone;
                customer.Email = data.Email;
                customer.TaxNo = data.TaxNo;
                customer.Address = data.Address;
                customer.Zipcode = data.Zipcode;
                this.context.SaveChanges();
                return customer;
            }else{
                return null;
            }
        }

        public void Delete(int id)
        {
            var customer = this.context.Customers.Where(x => x.Id == id).FirstOrDefault();
            if (customer != null)
            {
                customer.IsActive = false;
                this.context.SaveChanges();
            }
        }
    }
}