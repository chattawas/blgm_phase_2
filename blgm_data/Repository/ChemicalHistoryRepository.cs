using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using blgm_data.models;
using Microsoft.EntityFrameworkCore;

namespace blgm_data.Repository
{
    public class ChemicalHistoryRepository
    {
        private BLGMDbContext context;
        public ChemicalHistoryRepository(BLGMDbContext context)
        {
            this.context = context;
        }

        public async Task<List<ChemicalHistory>> GetAll(int id)
        {
            var query = this.context.ChemicalHistories.Where(x => x.ChemicalId == id).OrderByDescending(x => x.CreatedDate);
            return await query.ToListAsync();
        }
    }
}