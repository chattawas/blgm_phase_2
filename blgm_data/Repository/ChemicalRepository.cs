using System;
using System.Reflection.Emit;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using blgm_data.models;
using Microsoft.EntityFrameworkCore;

namespace blgm_data.Repository
{
    public class ChemicalRepository
    {
        private BLGMDbContext context;
        public ChemicalRepository(BLGMDbContext context)
        {
            this.context = context;
        }

        public async Task<List<Chemical>> GetAllAsync(Chemical search)
        {
            var query = (from c in this.context.Chemicals
                where c.IsActive == true
                    && (c.Name.Contains(search.Name) || String.IsNullOrEmpty(search.Name))
                    && (c.Code.Contains(search.Code) || String.IsNullOrEmpty(search.Code))
                    && (c.Size.Contains(search.Size) || String.IsNullOrEmpty(search.Size))
                    && (c.Color.Contains(search.Color) || String.IsNullOrEmpty(search.Color))
                select new Chemical{
                    Id = c.Id,
                    Code = c.Code,
                    Name = c.Name,
                    Size = c.Size,
                    Color = c.Color,
                    Cost = c.Cost,
                    Price = c.Price,
                    Quantity = c.Quantity,
                    WaitingImport = (from ol in this.context.ChemicalOrderLines
                                    join o in this.context.ChemicalOrder on ol.ChemicalOrderId equals o.Id
                                    where ol.ChemicalId == c.Id && o.IsActive == true && o.IsComplete == false && o.IsRecive == true
                                    select ol.Quantity
                                    ).Sum(),
                    WaitingExport = (from ol in this.context.ChemicalOrderLines
                                    join o in this.context.ChemicalOrder on ol.ChemicalOrderId equals o.Id
                                    where ol.ChemicalId == c.Id && o.IsActive == true && o.IsComplete == false && o.IsRecive == false
                                    select ol.Quantity
                                    ).Sum()
                });
            return await query.ToListAsync();
        }

        public async Task<List<Chemical>> GetAllAsync()
        {
            var query = (from c in this.context.Chemicals
                where c.IsActive == true
                select new Chemical{
                    Id = c.Id,
                    Code = c.Code,
                    Name = c.Name,
                    Size = c.Size,
                    Color = c.Color,
                    Cost = c.Cost,
                    Price = c.Price,
                    Quantity = c.Quantity,
                    WaitingImport = (from ol in this.context.ChemicalOrderLines
                                    join o in this.context.ChemicalOrder on ol.ChemicalOrderId equals o.Id
                                    where ol.ChemicalId == c.Id && o.IsActive == true && o.IsComplete == false && o.IsRecive == true
                                    select ol.Quantity
                                    ).Sum(),
                    WaitingExport = (from ol in this.context.ChemicalOrderLines
                                    join o in this.context.ChemicalOrder on ol.ChemicalOrderId equals o.Id
                                    where ol.ChemicalId == c.Id && o.IsActive == true && o.IsComplete == false && o.IsRecive == false
                                    select ol.Quantity
                                    ).Sum()
                });
            return await query.ToListAsync();
        }

        public async Task<List<Chemical>> GetAllOutOfStock()
        {
            var query = (from c in this.context.Chemicals
                where c.IsActive == true && c.Quantity < 100
                select new Chemical{
                    Id = c.Id,
                    Code = c.Code,
                    Name = c.Name,
                    Size = c.Size,
                    Color = c.Color,
                    Cost = c.Cost,
                    Price = c.Price,
                    Quantity = c.Quantity,
                    WaitingImport = (from ol in this.context.ChemicalOrderLines
                                    join o in this.context.ChemicalOrder on ol.ChemicalOrderId equals o.Id
                                    where ol.ChemicalId == c.Id && o.IsActive == true && o.IsComplete == false && o.IsRecive == true
                                    select ol.Quantity
                                    ).Sum(),
                    WaitingExport = (from ol in this.context.ChemicalOrderLines
                                    join o in this.context.ChemicalOrder on ol.ChemicalOrderId equals o.Id
                                    where ol.ChemicalId == c.Id && o.IsActive == true && o.IsComplete == false && o.IsRecive == false
                                    select ol.Quantity
                                    ).Sum()
                });
            return await query.ToListAsync();
        }

        public async Task<Chemical> Get(int id)
        {
            return await this.context.Chemicals.Where(x => x.Id == id).FirstOrDefaultAsync();
        }

        public async Task<int> GetMaxQuantityValue(int id,int orderId)
        {
            var package = await this.context.Chemicals.Where(x => x.Id == id).FirstOrDefaultAsync();
            var bookingPackage = this.context.ChemicalOrderLines
                    .Include(x => x.ChemicalOrder)
                    .Where(x => x.ChemicalId == id && x.ChemicalOrderId != orderId
                        && x.ChemicalOrder.IsComplete == false
                        && x.ChemicalOrder.IsRecive == false
                        && x.ChemicalOrder.IsActive == true).Sum(x => x.Quantity);

            return package.Quantity - bookingPackage;
        }

        public async Task<Chemical> CreateAsync(Chemical data)
        {
            data.IsActive = true;
            this.context.Chemicals.Add(data);
            var his = new ChemicalHistory
            {
                ChemicalId = data.Id,
                Quantity = data.Quantity,
                Reason = "สร้างข้อมูลใหม่",
                CreatedBy = data.UpdatedBy,
                CreatedDate = data.UpdatedDate
            };
            this.context.ChemicalHistories.Add(his);
            await this.context.SaveChangesAsync();
            return data;
        }

        public async Task<Chemical> UpdateAsync(Chemical data)
        {
            var chemical = await this.context.Chemicals.Where(x => x.Id == data.Id).FirstOrDefaultAsync();
            var changeValue = data.Quantity - chemical.Quantity;
            if (changeValue != 0)
            {
                var his = new ChemicalHistory
                {
                    ChemicalId = data.Id,
                    Quantity = changeValue,
                    Reason = "แก้ไขข้อมูล",
                    CreatedBy = data.UpdatedBy,
                    CreatedDate = data.UpdatedDate
                };
                this.context.ChemicalHistories.Add(his);
            }
            if (chemical != null)
            {
                chemical.Name = data.Name;
                chemical.Code = data.Code;
                chemical.Color = data.Color;
                chemical.Size = data.Size;
                chemical.Cost = data.Cost;
                chemical.Price = data.Price;
                chemical.Quantity = data.Quantity;
                chemical.UpdatedBy = data.UpdatedBy;
                chemical.UpdatedDate = data.UpdatedDate;
            }
            this.context.SaveChanges();

            return data;
        }

        public async Task<Chemical> Delete(int id, string user)
        {
            var data = await this.context.Chemicals.Where(x => x.Id == id).FirstOrDefaultAsync();
            if (data != null)
            {
                data.IsActive = false;
                data.UpdatedBy = user;
                data.UpdatedDate = DateTime.Now;
                await this.context.SaveChangesAsync();
            }

            return data;

        }

    }
}