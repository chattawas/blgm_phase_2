using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using blgm_data.models;
using blgm_data.views;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using Microsoft.EntityFrameworkCore;

namespace blgm_data.Repository
{
    public class UserRepository
    {
        private BLGMDbContext context;
        public UserRepository(BLGMDbContext context)
        {
            this.context = context;
        }

        public async Task<List<UserView>> GetAll()
        {
            return await this.context.Users.Select(x => new UserView{
                Id = x.Id,
                Username = x.Username,
                FirstName = x.FirstName,
                LastName = x.LastName,
            }).ToListAsync();
        }

        public async Task<UserView> Get(int id)
        {
            return await this.context.Users.Select(x => new UserView{
                Id = x.Id,
                Username = x.Username,
                FirstName = x.FirstName,
                LastName = x.LastName,
            })
            .Where(x => x.Id == id)
            .FirstOrDefaultAsync();
        }

        public User Add(UserView data)
        {
            var user = new User
            {
                Username = data.Username,
                FirstName = data.FirstName,
                LastName = data.LastName
            };
            user.Salt = GeneratSalt();
            user.Password = Create(data.Password, user.Salt);
            this.context.Users.Add(user);
            this.context.SaveChanges();
            return user;
        }

        public User UpdateDetail(UserView data)
        {
            var user = this.context.Users.Where(x => x.Id == data.Id).FirstOrDefault();
            if (user != null)
            {
                user.FirstName = data.FirstName;
                user.LastName = data.LastName;
                this.context.SaveChanges();
                return user;
            }else{
                return null;
            }
        }

        public User UpdatePassword(UserView data)
        {
            var user = this.context.Users.Where(x => x.Id == data.Id).FirstOrDefault();
            if (user != null)
            {
                user.Salt = GeneratSalt();
                user.Password = Create(data.Password, user.Salt);
                this.context.SaveChanges();
                return user;
            }else{
                return null;
            }

        }

        public void Delete(int id)
        {
            var user = this.context.Users.Where(x => x.Id == id).FirstOrDefault();
            if (user != null)
            {
                this.context.Users.Remove(user);
                this.context.SaveChanges();
            }
        }

        public async Task<User> AuthenticateAsync(string username, string password)
        {
            var user = await this.context.Users.SingleOrDefaultAsync(x => x.Username == username);
            if(user != null){
                var checkpassword = this.Create(password,user.Salt);
                if (user.Password != checkpassword){
                    return null;
                }
            }
            else {
                return null;
            }
            return user;
        }

        public string Create(string value, string salt)
        {
            var valueBytes = KeyDerivation.Pbkdf2(
                                password: value,
                                salt: Encoding.UTF8.GetBytes(salt),
                                prf: KeyDerivationPrf.HMACSHA512,
                                iterationCount: 10000,
                                numBytesRequested: 256 / 8);

            return Convert.ToBase64String(valueBytes);
        }

        public string GeneratSalt()
        {
            byte[] randomBytes = new byte[128 / 8];
            using (var generator = RandomNumberGenerator.Create())
            {
                generator.GetBytes(randomBytes);
                return Convert.ToBase64String(randomBytes);
            }
        }
    }
}