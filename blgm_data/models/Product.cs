using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace blgm_data.models
{
    public class Product
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string AltName { get; set; }
        public string Note { get; set; }
        public string CratfTime { get; set; }
        public string Size { get; set; }
        public decimal Width { get; set; }
        public decimal Height { get; set; }
        public decimal Length { get; set; }
        public decimal Weight { get; set; }
        public decimal Cost { get; set; }
        public decimal Price { get; set; }
        public int Quantity { get; set; }
        public virtual List<ProductMaterial> ProductMaterial { get; set; }
        public virtual List<ProductImage> ProductImage { get; set; }
        public virtual List<ProductPrice> ProductPrice { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }
        [NotMapped]
        public bool isCollapsed { get; set; }
        [NotMapped]
        public List<int> DeleteImage { get; set; }
        [NotMapped]
        public int WaitingImport { get; set; }
        [NotMapped]
        public int WaitingExport { get; set; }

    }
}