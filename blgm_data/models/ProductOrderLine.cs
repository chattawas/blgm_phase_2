using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace blgm_data.models
{
    public class ProductOrderLine
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int ProductId { get; set; }
        public virtual Product Product { get; set; }
        public int Quantity { get; set; }
        public bool IsMDF { get; set; }
        public decimal Price { get; set; }
        public decimal TotalPrice { get; set; }
        public int ProductOrderId { get; set; }
        public virtual ProductOrder ProductOrder { get; set; }
        public virtual List<MaterialOrderLine> MaterialOrderLines { get; set; }
        [NotMapped]
        public bool isCollapsed { get; set; }
    }
}