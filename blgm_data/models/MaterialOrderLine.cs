using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace blgm_data.models
{
    public class MaterialOrderLine
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int MaterialId { get; set; }
        public virtual Material Material { get; set; }
        public int Quantity { get; set; }
        public decimal Price { get; set; }
        public decimal TotalPrice { get; set; }
        public int? MaterialOrderId { get; set; }
        public virtual MaterialOrder MaterialOrder { get; set; }
        public int? ProductOrderLineId { get; set; }
        public virtual ProductOrderLine ProductOrderLine { get; set; }
        

    }
}