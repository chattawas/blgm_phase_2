namespace blgm_data.models
{
    public class LoginModel
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}