using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace blgm_data.models
{
    public class MaterialOrder
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [NotMapped]
        public Date OrderedCompleteDateObj { get; set; }
        public DateTime OrderedCompleteDate { get; set; }
        public virtual Customer Customer { get; set; }
        public int CustomerId { get; set; }
        public string Reason { get; set; }
        public bool IsActive { get; set; }
        public bool IsRecive { get; set; }
        public bool IsComplete { get; set; }
        public decimal Amount { get; set; }
        public virtual List<MaterialOrderLine> OrderLine { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }
        [NotMapped]
        public bool isCollapsed { get; set; }

    }
}