using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace blgm_data.models
{
    public class ChemicalOrderLine
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int ChemicalId { get; set; }
        public virtual Chemical Chemical { get; set; }
        public int Quantity { get; set; }
        public int ChemicalOrderId { get; set; }
        public decimal Price { get; set; }
        public decimal TotalPrice { get; set; }
        public virtual ChemicalOrder ChemicalOrder { get; set; }

    }
}