using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using System.IO;
namespace blgm_data
{
    public class BLGMDbContextFactory : IDesignTimeDbContextFactory<BLGMDbContext>
    {
        public BLGMDbContext CreateDbContext(string[] args)
        {
            IConfigurationRoot configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();

            var builder = new DbContextOptionsBuilder<BLGMDbContext>();
            var connectionString = configuration.GetConnectionString("production");

            builder.UseSqlServer(connectionString);

            return new BLGMDbContext(builder.Options);
        }
    }
}