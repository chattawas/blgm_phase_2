using blgm_data.models;

namespace blgm_data.views
{
    public class SearchView
    {
        public int CustomerId { get; set; }
        public int ChemicalId { get; set; }
        public Date OrderedCompleteDateObj { get; set; }
        public bool? IsRecive { get; set; }
        public bool? IsComplete { get; set; }
    }
}