﻿using blgm_data.models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace blgm_data
{

    public class BLGMDbContext : DbContext
    {
        public BLGMDbContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<Contact> Contacts { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Chemical> Chemicals { get; set; }
        public DbSet<ChemicalHistory> ChemicalHistories { get; set; }
        public DbSet<ChemicalOrder> ChemicalOrder { get; set; }
        public DbSet<ChemicalOrderLine> ChemicalOrderLines { get; set; }
        public DbSet<Material> Materials { get; set; }
        public DbSet<MaterialHistory> MaterialHistories { get; set; }
        public DbSet<MaterialOrder> MaterialOrders { get; set; }
        public DbSet<MaterialOrderLine> MaterialOrderLines { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<ProductHistory> ProductHistories { get; set; }
        public DbSet<ProductImage> ProductImages { get; set; }
        public DbSet<ProductMaterial> ProductMaterials { get; set; }
        public DbSet<ProductPrice> ProductPrices { get; set; }
        public DbSet<ProductOrder> ProductOrders { get; set; }
        public DbSet<ProductOrderLine> ProductOrderLines { get; set; }
    }
}
