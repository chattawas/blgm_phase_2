﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace blgm_data.Migrations
{
    public partial class remove_chemicalPackage : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ChemicalOrderLines_ChemicalPackages_ChemicalPackageId",
                table: "ChemicalOrderLines");

            migrationBuilder.DropTable(
                name: "ChemicalPackages");

            migrationBuilder.DropTable(
                name: "MaterialPackages");

            migrationBuilder.DropColumn(
                name: "Unit",
                table: "ChemicalHistories");

            migrationBuilder.RenameColumn(
                name: "ChemicalPackageId",
                table: "ChemicalOrderLines",
                newName: "ChemicalId");

            migrationBuilder.RenameIndex(
                name: "IX_ChemicalOrderLines_ChemicalPackageId",
                table: "ChemicalOrderLines",
                newName: "IX_ChemicalOrderLines_ChemicalId");

            migrationBuilder.AddColumn<decimal>(
                name: "Cost",
                table: "Materials",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<string>(
                name: "CreatedBy",
                table: "Materials",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedDate",
                table: "Materials",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<decimal>(
                name: "Price",
                table: "Materials",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<int>(
                name: "Quantity",
                table: "Materials",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "UpdatedBy",
                table: "Materials",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "UpdatedDate",
                table: "Materials",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<decimal>(
                name: "Cost",
                table: "Chemicals",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "Price",
                table: "Chemicals",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<int>(
                name: "Quantity",
                table: "Chemicals",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddForeignKey(
                name: "FK_ChemicalOrderLines_Chemicals_ChemicalId",
                table: "ChemicalOrderLines",
                column: "ChemicalId",
                principalTable: "Chemicals",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ChemicalOrderLines_Chemicals_ChemicalId",
                table: "ChemicalOrderLines");

            migrationBuilder.DropColumn(
                name: "Cost",
                table: "Materials");

            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "Materials");

            migrationBuilder.DropColumn(
                name: "CreatedDate",
                table: "Materials");

            migrationBuilder.DropColumn(
                name: "Price",
                table: "Materials");

            migrationBuilder.DropColumn(
                name: "Quantity",
                table: "Materials");

            migrationBuilder.DropColumn(
                name: "UpdatedBy",
                table: "Materials");

            migrationBuilder.DropColumn(
                name: "UpdatedDate",
                table: "Materials");

            migrationBuilder.DropColumn(
                name: "Cost",
                table: "Chemicals");

            migrationBuilder.DropColumn(
                name: "Price",
                table: "Chemicals");

            migrationBuilder.DropColumn(
                name: "Quantity",
                table: "Chemicals");

            migrationBuilder.RenameColumn(
                name: "ChemicalId",
                table: "ChemicalOrderLines",
                newName: "ChemicalPackageId");

            migrationBuilder.RenameIndex(
                name: "IX_ChemicalOrderLines_ChemicalId",
                table: "ChemicalOrderLines",
                newName: "IX_ChemicalOrderLines_ChemicalPackageId");

            migrationBuilder.AddColumn<string>(
                name: "Unit",
                table: "ChemicalHistories",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "ChemicalPackages",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ChemicalId = table.Column<int>(nullable: false),
                    Cost = table.Column<decimal>(nullable: false),
                    CreatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    Index = table.Column<int>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Price = table.Column<decimal>(nullable: false),
                    Quantity = table.Column<int>(nullable: false),
                    QuantityPerPack = table.Column<int>(nullable: false),
                    UpdatedBy = table.Column<string>(nullable: true),
                    UpdatedDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ChemicalPackages", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ChemicalPackages_Chemicals_ChemicalId",
                        column: x => x.ChemicalId,
                        principalTable: "Chemicals",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "MaterialPackages",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    IsActive = table.Column<bool>(nullable: false),
                    MaterialId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Parent = table.Column<int>(nullable: true),
                    Quantity = table.Column<int>(nullable: false),
                    QuantityPerPack = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MaterialPackages", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MaterialPackages_Materials_MaterialId",
                        column: x => x.MaterialId,
                        principalTable: "Materials",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ChemicalPackages_ChemicalId",
                table: "ChemicalPackages",
                column: "ChemicalId");

            migrationBuilder.CreateIndex(
                name: "IX_MaterialPackages_MaterialId",
                table: "MaterialPackages",
                column: "MaterialId");

            migrationBuilder.AddForeignKey(
                name: "FK_ChemicalOrderLines_ChemicalPackages_ChemicalPackageId",
                table: "ChemicalOrderLines",
                column: "ChemicalPackageId",
                principalTable: "ChemicalPackages",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
