﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace blgm_data.Migrations
{
    public partial class add_index : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Parent",
                table: "ChemicalPackages");

            migrationBuilder.AddColumn<int>(
                name: "Index",
                table: "ChemicalPackages",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Index",
                table: "ChemicalPackages");

            migrationBuilder.AddColumn<int>(
                name: "Parent",
                table: "ChemicalPackages",
                nullable: true);
        }
    }
}
