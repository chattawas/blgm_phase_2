﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace blgm_data.Migrations
{
    public partial class add_isComplete : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsRecive",
                table: "ChemicalOrder");

            migrationBuilder.RenameColumn(
                name: "IsStock",
                table: "ChemicalOrder",
                newName: "IsComplete");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "IsComplete",
                table: "ChemicalOrder",
                newName: "IsStock");

            migrationBuilder.AddColumn<bool>(
                name: "IsRecive",
                table: "ChemicalOrder",
                nullable: false,
                defaultValue: false);
        }
    }
}
