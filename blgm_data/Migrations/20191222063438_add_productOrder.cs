﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace blgm_data.Migrations
{
    public partial class add_productOrder : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MaterialOrderLines_MaterialOrders_MaterialOrderId",
                table: "MaterialOrderLines");

            migrationBuilder.AlterColumn<int>(
                name: "MaterialOrderId",
                table: "MaterialOrderLines",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<int>(
                name: "ProductOrderLineId",
                table: "MaterialOrderLines",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "ProductOrders",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    OrderedCompleteDate = table.Column<DateTime>(nullable: false),
                    CustomerId = table.Column<int>(nullable: false),
                    Reason = table.Column<string>(nullable: true),
                    IsActive = table.Column<bool>(nullable: false),
                    IsRecive = table.Column<bool>(nullable: false),
                    IsComplete = table.Column<bool>(nullable: false),
                    Amount = table.Column<decimal>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    CreatedBy = table.Column<string>(nullable: true),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedBy = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductOrders", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProductOrders_Customers_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "Customers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ProductOrderLines",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ProductId = table.Column<int>(nullable: false),
                    Quantity = table.Column<int>(nullable: false),
                    Price = table.Column<decimal>(nullable: false),
                    TotalPrice = table.Column<decimal>(nullable: false),
                    ProductOrderId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductOrderLines", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProductOrderLines_Products_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Products",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProductOrderLines_ProductOrders_ProductOrderId",
                        column: x => x.ProductOrderId,
                        principalTable: "ProductOrders",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_MaterialOrderLines_ProductOrderLineId",
                table: "MaterialOrderLines",
                column: "ProductOrderLineId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductOrderLines_ProductId",
                table: "ProductOrderLines",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductOrderLines_ProductOrderId",
                table: "ProductOrderLines",
                column: "ProductOrderId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductOrders_CustomerId",
                table: "ProductOrders",
                column: "CustomerId");

            migrationBuilder.AddForeignKey(
                name: "FK_MaterialOrderLines_MaterialOrders_MaterialOrderId",
                table: "MaterialOrderLines",
                column: "MaterialOrderId",
                principalTable: "MaterialOrders",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_MaterialOrderLines_ProductOrderLines_ProductOrderLineId",
                table: "MaterialOrderLines",
                column: "ProductOrderLineId",
                principalTable: "ProductOrderLines",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MaterialOrderLines_MaterialOrders_MaterialOrderId",
                table: "MaterialOrderLines");

            migrationBuilder.DropForeignKey(
                name: "FK_MaterialOrderLines_ProductOrderLines_ProductOrderLineId",
                table: "MaterialOrderLines");

            migrationBuilder.DropTable(
                name: "ProductOrderLines");

            migrationBuilder.DropTable(
                name: "ProductOrders");

            migrationBuilder.DropIndex(
                name: "IX_MaterialOrderLines_ProductOrderLineId",
                table: "MaterialOrderLines");

            migrationBuilder.DropColumn(
                name: "ProductOrderLineId",
                table: "MaterialOrderLines");

            migrationBuilder.AlterColumn<int>(
                name: "MaterialOrderId",
                table: "MaterialOrderLines",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_MaterialOrderLines_MaterialOrders_MaterialOrderId",
                table: "MaterialOrderLines",
                column: "MaterialOrderId",
                principalTable: "MaterialOrders",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
