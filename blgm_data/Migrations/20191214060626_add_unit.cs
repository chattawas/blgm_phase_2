﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace blgm_data.Migrations
{
    public partial class add_unit : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Unit",
                table: "ChemicalHistories",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Unit",
                table: "ChemicalHistories");
        }
    }
}
