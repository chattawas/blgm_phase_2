﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace blgm_data.Migrations
{
    public partial class add_chemicalOrder_and_chemicalHis : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ChemicalHistories",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ChemicalId = table.Column<int>(nullable: false),
                    Quantity = table.Column<int>(nullable: false),
                    Reference = table.Column<string>(nullable: true),
                    Reason = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ChemicalHistories", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ChemicalHistories_Chemicals_ChemicalId",
                        column: x => x.ChemicalId,
                        principalTable: "Chemicals",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ChemicalOrder",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    OrderedCompleteDate = table.Column<DateTime>(nullable: false),
                    CustomerId = table.Column<int>(nullable: false),
                    Reason = table.Column<string>(nullable: true),
                    IsActive = table.Column<bool>(nullable: false),
                    IsStock = table.Column<bool>(nullable: false),
                    IsRecive = table.Column<bool>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    CreatedBy = table.Column<string>(nullable: true),
                    UpdateedDate = table.Column<DateTime>(nullable: false),
                    UpdatedBy = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ChemicalOrder", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ChemicalOrder_Customers_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "Customers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ChemicalOrderLines",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ChemicalPackageId = table.Column<int>(nullable: false),
                    Quentity = table.Column<int>(nullable: false),
                    ChemicalOrderId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ChemicalOrderLines", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ChemicalOrderLines_ChemicalOrder_ChemicalOrderId",
                        column: x => x.ChemicalOrderId,
                        principalTable: "ChemicalOrder",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ChemicalOrderLines_ChemicalPackages_ChemicalPackageId",
                        column: x => x.ChemicalPackageId,
                        principalTable: "ChemicalPackages",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ChemicalHistories_ChemicalId",
                table: "ChemicalHistories",
                column: "ChemicalId");

            migrationBuilder.CreateIndex(
                name: "IX_ChemicalOrder_CustomerId",
                table: "ChemicalOrder",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_ChemicalOrderLines_ChemicalOrderId",
                table: "ChemicalOrderLines",
                column: "ChemicalOrderId");

            migrationBuilder.CreateIndex(
                name: "IX_ChemicalOrderLines_ChemicalPackageId",
                table: "ChemicalOrderLines",
                column: "ChemicalPackageId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ChemicalHistories");

            migrationBuilder.DropTable(
                name: "ChemicalOrderLines");

            migrationBuilder.DropTable(
                name: "ChemicalOrder");
        }
    }
}
