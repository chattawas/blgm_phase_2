﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace blgm_data.Migrations
{
    public partial class add_IsMDF : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ProductOrderLines_ProductOrders_ProductOrderId",
                table: "ProductOrderLines");

            migrationBuilder.AlterColumn<int>(
                name: "ProductOrderId",
                table: "ProductOrderLines",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsMDF",
                table: "ProductOrderLines",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddForeignKey(
                name: "FK_ProductOrderLines_ProductOrders_ProductOrderId",
                table: "ProductOrderLines",
                column: "ProductOrderId",
                principalTable: "ProductOrders",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ProductOrderLines_ProductOrders_ProductOrderId",
                table: "ProductOrderLines");

            migrationBuilder.DropColumn(
                name: "IsMDF",
                table: "ProductOrderLines");

            migrationBuilder.AlterColumn<int>(
                name: "ProductOrderId",
                table: "ProductOrderLines",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddForeignKey(
                name: "FK_ProductOrderLines_ProductOrders_ProductOrderId",
                table: "ProductOrderLines",
                column: "ProductOrderId",
                principalTable: "ProductOrders",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
