﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace blgm_data.Migrations
{
    public partial class add_create_update_to_chemical : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "UpdateedDate",
                table: "ChemicalOrder",
                newName: "UpdatedDate");

            migrationBuilder.AddColumn<string>(
                name: "CreatedBy",
                table: "Chemicals",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedDate",
                table: "Chemicals",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "UpdatedBy",
                table: "Chemicals",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "UpdatedDate",
                table: "Chemicals",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "CreatedBy",
                table: "ChemicalPackages",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedDate",
                table: "ChemicalPackages",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "UpdatedBy",
                table: "ChemicalPackages",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "UpdatedDate",
                table: "ChemicalPackages",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "CreatedBy",
                table: "ChemicalHistories",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedDate",
                table: "ChemicalHistories",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "Chemicals");

            migrationBuilder.DropColumn(
                name: "CreatedDate",
                table: "Chemicals");

            migrationBuilder.DropColumn(
                name: "UpdatedBy",
                table: "Chemicals");

            migrationBuilder.DropColumn(
                name: "UpdatedDate",
                table: "Chemicals");

            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "ChemicalPackages");

            migrationBuilder.DropColumn(
                name: "CreatedDate",
                table: "ChemicalPackages");

            migrationBuilder.DropColumn(
                name: "UpdatedBy",
                table: "ChemicalPackages");

            migrationBuilder.DropColumn(
                name: "UpdatedDate",
                table: "ChemicalPackages");

            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "ChemicalHistories");

            migrationBuilder.DropColumn(
                name: "CreatedDate",
                table: "ChemicalHistories");

            migrationBuilder.RenameColumn(
                name: "UpdatedDate",
                table: "ChemicalOrder",
                newName: "UpdateedDate");
        }
    }
}
