﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using blgm_data.Repository;
using blgm_data.models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace blgm_phase_2.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ContactsController : ControllerBase
    {
        private ContactRepository contactRepository;

        public ContactsController(ContactRepository contactRepository)
        {
            this.contactRepository = contactRepository;
        }

        [HttpPost("Add")]
        public IActionResult Add([FromBody]Contact contact)
        {
            var data = contactRepository.AddContact(contact);
            return Ok(data);
        }

        [HttpPost("update")]
        public IActionResult Update([FromBody]Contact contact)
        {
            return Ok();
        }

        [HttpGet("")]
        [Authorize]
        public async Task<IActionResult> GetAsync()
        {
            var data = await contactRepository.GetAll();
            return Ok(data);
        }
    }
}
