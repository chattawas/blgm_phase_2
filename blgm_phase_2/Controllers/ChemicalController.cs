using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using blgm_data.models;
using blgm_data.Repository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
//using blgm_phase_2.Models;

namespace blgm_phase_2.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class ChemicalController : ControllerBase
    {
        ChemicalRepository chemicalRepository; 
        public ChemicalController(ChemicalRepository chemicalRepository)
        {
            this.chemicalRepository = chemicalRepository;
        }

        // GET api/chemical
        [HttpGet("")]
        public async Task<IActionResult> GetAsync()
        {
            var data = await chemicalRepository.GetAllAsync();
            return Ok(data);
        }

        [HttpPost("search")]
        public async Task<IActionResult> Search([FromBody]Chemical search)
        {
            var data = await chemicalRepository.GetAllAsync(search);
            return Ok(data);
        }

        [HttpGet("OutOfStock")]
        public async Task<IActionResult> GetOutOfStock()
        {
            var data = await chemicalRepository.GetAllOutOfStock();
            return Ok(data);
        }

        // GET api/chemical/5
        [HttpGet("{id}")]
        public  async Task<IActionResult> GetById(int id)
        {
            var data = await chemicalRepository.Get(id);
            return Ok(data);
        }

        // GET api/chemical/5
        [HttpGet("package")]
        public  async Task<IActionResult> GetPackageMaxValue(int id,int orderId)
        {
            var data = await chemicalRepository.GetMaxQuantityValue(id, orderId);
            return Ok(data);
        }


        // POST api/chemical
        [HttpPost("Add")]
        public async Task<IActionResult> PostAsync([FromBody]Chemical data)
        {
            var claimsIdentity = this.User.Identity as ClaimsIdentity;
            var username = claimsIdentity.FindFirst(ClaimTypes.Name)?.Value;
            data.CreatedBy = username;
            data.CreatedDate = DateTime.Now;
            data.UpdatedBy = username;
            data.UpdatedDate = DateTime.Now;
            var result = await this.chemicalRepository.CreateAsync(data);
            return Ok();

        }

        // POST api/chemical
        [HttpPost("Update")]
        public async Task<IActionResult> UpdateAsync([FromBody]Chemical data)
        {
            var claimsIdentity = this.User.Identity as ClaimsIdentity;
            var username = claimsIdentity.FindFirst(ClaimTypes.Name)?.Value;
            data.UpdatedBy = username;
            data.UpdatedDate = DateTime.Now;
            var result = await this.chemicalRepository.UpdateAsync(data);
            return Ok();

        }
        // DELETE api/chemical/5
        [HttpDelete("{id}")]
        public async Task DeleteAsync(int id)
        {
            var claimsIdentity = this.User.Identity as ClaimsIdentity;
            var username = claimsIdentity.FindFirst(ClaimTypes.Name)?.Value;
            await this.chemicalRepository.Delete(id, username);
        }
    }
}