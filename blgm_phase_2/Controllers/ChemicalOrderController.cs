using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using blgm_data.models;
using blgm_data.Repository;
using blgm_data.views;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace blgm_phase_2.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class ChemicalOrderController : ControllerBase
    {
        ChemicalOrderRepository chemicalOrderRepository;
        public ChemicalOrderController(ChemicalOrderRepository chemicalOrderRepository)
        {
            this.chemicalOrderRepository = chemicalOrderRepository;
        }

        // GET api/chemical
        [HttpGet("")]
        public async Task<IActionResult> GetAsync()
        {
            var data = await chemicalOrderRepository.GetAllAsync();
            return Ok(data);
        }

        [HttpPost("search")]
        public async Task<IActionResult> Search([FromBody]SearchView search)
        {
            var data = await chemicalOrderRepository.GetAllAsync(search);
            return Ok(data);
        }

        [HttpGet("CompleteToday")]
        public async Task<IActionResult> GetAllCompleteToday()
        {
            var data = await chemicalOrderRepository.GetAllCompleteToday();
            return Ok(data);
        }

        // GET api/chemical/5
        [HttpGet("{id}")]
        public  async Task<IActionResult> GetById(int id)
        {
            var data = await chemicalOrderRepository.Get(id);
            return Ok(data);
        }

        [HttpPost("Add")]
        public async Task<IActionResult> PostAsync([FromBody]ChemicalOrder data)
        {
            var claimsIdentity = this.User.Identity as ClaimsIdentity;
            var username = claimsIdentity.FindFirst(ClaimTypes.Name)?.Value;
            data.CreatedBy = username;
            data.CreatedDate = DateTime.Now;
            data.UpdatedBy = username;
            data.UpdatedDate = DateTime.Now;
            var result = await this.chemicalOrderRepository.CreateAsync(data);
            return Ok(result);
        }

        [HttpPost("Update")]
        public async Task<IActionResult> Update([FromBody]ChemicalOrder data)
        {
            var claimsIdentity = this.User.Identity as ClaimsIdentity;
            var username = claimsIdentity.FindFirst(ClaimTypes.Name)?.Value;
            data.UpdatedBy = username;
            data.UpdatedDate = DateTime.Now;
            var result = await this.chemicalOrderRepository.UpdateAsync(data);
            return Ok(result);
        }

        [HttpGet("OrderComlpete/{id}")]
        public async Task<IActionResult> OrderComlpete(int id)
        {
            var claimsIdentity = this.User.Identity as ClaimsIdentity;
            var username = claimsIdentity.FindFirst(ClaimTypes.Name)?.Value;
            var result = await this.chemicalOrderRepository.SetCompleteOrder(id, username);
            return Ok(result);
        }

        [HttpDelete("{id}")]
        public async Task DeleteAsync(int id)
        {
            var claimsIdentity = this.User.Identity as ClaimsIdentity;
            var username = claimsIdentity.FindFirst(ClaimTypes.Name)?.Value;
            await this.chemicalOrderRepository.CancelOrder(id, username);
        }
    }
}