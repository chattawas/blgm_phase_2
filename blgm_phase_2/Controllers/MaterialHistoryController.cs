using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using blgm_data.Repository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
//using blgm_phase_2.Models;

namespace blgm_phase_2.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class MaterialHistoryController : ControllerBase
    {
        MaterialHistoryRepository materialHistoryRepository; 

        public MaterialHistoryController(MaterialHistoryRepository materialHistoryRepository)
        {
            this.materialHistoryRepository = materialHistoryRepository;
        }
        // GET api/chemicalhistory/5
        [HttpGet("{id}")]
        public async Task<IActionResult>  GetByIdAsync(int id)
        {
            var data = await this.materialHistoryRepository.GetAll(id);
            return Ok(data);
        }
    }
}