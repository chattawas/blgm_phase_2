using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using blgm_data.models;
using blgm_data.Repository;
using blgm_data.views;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace blgm_phase_2.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class MaterialOrderController : ControllerBase
    {
        MaterialOrderRepository materialOrderRepository;
        public MaterialOrderController(MaterialOrderRepository materialOrderRepository)
        {
            this.materialOrderRepository = materialOrderRepository;
        }

        // GET api/chemical
        [HttpGet("")]
        public async Task<IActionResult> GetAsync()
        {
            var data = await materialOrderRepository.GetAllAsync();
            return Ok(data);
        }

        [HttpPost("search")]
        public async Task<IActionResult> Search([FromBody]SearchView search)
        {
            var data = await materialOrderRepository.Search(search);
            return Ok(data);
        }

        [HttpGet("CompleteToday")]
        public async Task<IActionResult> GetAllCompleteToday()
        {
            var data = await materialOrderRepository.GetAllCompleteToday();
            return Ok(data);
        }


        // GET api/chemical/5
        [HttpGet("{id}")]
        public  async Task<IActionResult> GetById(int id)
        {
            var data = await materialOrderRepository.Get(id);
            return Ok(data);
        }

        [HttpPost("Add")]
        public async Task<IActionResult> PostAsync([FromBody]MaterialOrder data)
        {
            var claimsIdentity = this.User.Identity as ClaimsIdentity;
            var username = claimsIdentity.FindFirst(ClaimTypes.Name)?.Value;
            data.CreatedBy = username;
            data.CreatedDate = DateTime.Now;
            data.UpdatedBy = username;
            data.UpdatedDate = DateTime.Now;
            var result = await this.materialOrderRepository.CreateAsync(data);
            return Ok(result);
        }

        [HttpPost("Update")]
        public async Task<IActionResult> Update([FromBody]MaterialOrder data)
        {
            var claimsIdentity = this.User.Identity as ClaimsIdentity;
            var username = claimsIdentity.FindFirst(ClaimTypes.Name)?.Value;
            data.UpdatedBy = username;
            data.UpdatedDate = DateTime.Now;
            var result = await this.materialOrderRepository.UpdateAsync(data);
            return Ok(result);
        }

        [HttpGet("OrderComlpete/{id}")]
        public async Task<IActionResult> OrderComlpete(int id)
        {
            var claimsIdentity = this.User.Identity as ClaimsIdentity;
            var username = claimsIdentity.FindFirst(ClaimTypes.Name)?.Value;
            var result = await this.materialOrderRepository.SetCompleteOrder(id, username);
            return Ok(result);
        }

        [HttpDelete("{id}")]
        public async Task DeleteAsync(int id)
        {
            var claimsIdentity = this.User.Identity as ClaimsIdentity;
            var username = claimsIdentity.FindFirst(ClaimTypes.Name)?.Value;
            await this.materialOrderRepository.CancelOrder(id, username);
        }
    }
}