using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using blgm_data.models;
using blgm_data.Repository;
using blgm_data.views;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
//using blgm_phase_2.Models;

namespace blgm_phase_2.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class ProductOrderController : ControllerBase
    {
        
        ProductOrderRepository productOrderRepository;
        public ProductOrderController(ProductOrderRepository productOrderRepository)
        {
            this.productOrderRepository = productOrderRepository;
        }
        // GET api/productorder
        [HttpGet("")]
        public async Task<IActionResult> GetAsync()
        {
            var data = await productOrderRepository.GetAllAsync();
            return Ok(data);
        }

        [HttpGet("CompleteToday")]
        public async Task<IActionResult> GetAllCompleteToday()
        {
            var data = await productOrderRepository.GetAllCompleteToday();
            return Ok(data);
        }

        // GET api/productorder/5
        [HttpGet("{id}")]
        public  async Task<IActionResult> GetById(int id)
        {
            var data = await productOrderRepository.Get(id);
            return Ok(data);
        }

        // POST api/productorder
        [HttpPost("Add")]
        public async Task<IActionResult> PostAsync([FromBody]ProductOrder data)
        {
            var claimsIdentity = this.User.Identity as ClaimsIdentity;
            var username = claimsIdentity.FindFirst(ClaimTypes.Name)?.Value;
            data.CreatedBy = username;
            data.CreatedDate = DateTime.Now;
            data.UpdatedBy = username;
            data.UpdatedDate = DateTime.Now;
            var result = await this.productOrderRepository.CreateAsync(data);
            return Ok(result);
        }

        [HttpPost("search")]
        public async Task<IActionResult> Search([FromBody]SearchView search)
        {
            var data = await productOrderRepository.GetAllAsync(search);
            return Ok(data);
        }

        [HttpPost("Update")]
        public async Task<IActionResult> Update([FromBody]ProductOrder data)
        {
            var claimsIdentity = this.User.Identity as ClaimsIdentity;
            var username = claimsIdentity.FindFirst(ClaimTypes.Name)?.Value;
            data.UpdatedBy = username;
            data.UpdatedDate = DateTime.Now;
            var result = await this.productOrderRepository.UpdateAsync(data);
            return Ok(result);
        }

        [HttpGet("OrderComlpete/{id}")]
        public async Task<IActionResult> OrderComlpete(int id)
        {
            var claimsIdentity = this.User.Identity as ClaimsIdentity;
            var username = claimsIdentity.FindFirst(ClaimTypes.Name)?.Value;
            var result = await this.productOrderRepository.SetCompleteOrder(id, username);
            return Ok(result);
        }

        // DELETE api/productorder/5
        [HttpDelete("{id}")]
        public async Task DeletestringByIdAsync(int id)
        {
            var claimsIdentity = this.User.Identity as ClaimsIdentity;
            var username = claimsIdentity.FindFirst(ClaimTypes.Name)?.Value;
            await this.productOrderRepository.CancelOrder(id, username);
        }
    }
}