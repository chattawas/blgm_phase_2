using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using blgm_data.Repository;
using Microsoft.AspNetCore.Mvc;
//using blgm_phase_2.Models;

namespace blgm_phase_2.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductHistoryController : ControllerBase
    {
        ProductHistoryRepository productHistoryRepository; 

        public ProductHistoryController(ProductHistoryRepository productHistoryRepository)
        {
            this.productHistoryRepository = productHistoryRepository;
        }
        // GET api/chemicalhistory/5
        [HttpGet("{id}")]
        public async Task<IActionResult>  GetByIdAsync(int id)
        {
            var data = await this.productHistoryRepository.GetAll(id);
            return Ok(data);
        }
    }
}