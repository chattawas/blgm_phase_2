using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using blgm_data.models;
using blgm_data.Repository;
using blgm_phase_2.Views;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
//using blgm_phase_2.Models;

namespace blgm_phase_2.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class ProductController : ControllerBase
    {
        ProductRepository productRepository;
        public ProductController(ProductRepository productRepository)
        {
            this.productRepository = productRepository;
        }

        // GET api/chemical
        [HttpGet("")]
        public async Task<IActionResult> GetAsync()
        {
            var data = await productRepository.GetAllAsync();
            return Ok(data);
        }

        [HttpPost("search")]
        public async Task<IActionResult> Search([FromBody]Product search)
        {
            var data = await productRepository.GetAllAsync(search);
            return Ok(data);
        }

        [HttpGet("OutOfStock")]
        public async Task<IActionResult> GetOutOfStock()
        {
            var data = await productRepository.GetAllOutOfStock();
            return Ok(data);
        }

        // GET api/chemical/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            var data = await productRepository.Get(id);
            return Ok(data);
        }

        //GET api/chemical/5
        [HttpGet("package")]
        public  async Task<IActionResult> GetPackageMaxValue(int id,int orderId)
        {
            var data = await productRepository.GetMaxQuantityValue(id, orderId);
            return Ok(data);
        }


        // POST api/chemical
        [HttpPost("Add")]
        public async Task<IActionResult> PostAsync([FromBody]Product data)
        {
            var claimsIdentity = this.User.Identity as ClaimsIdentity;
            var username = claimsIdentity.FindFirst(ClaimTypes.Name)?.Value;
            data.CreatedBy = username;
            data.CreatedDate = DateTime.Now;
            data.UpdatedBy = username;
            data.UpdatedDate = DateTime.Now;
            var result = await this.productRepository.CreateAsync(data);
            return Ok(result);
        }

        // POST api/chemical
        [HttpPost("Update")]
        public async Task<IActionResult> UpdateAsync([FromBody]Product data)
        {
            var claimsIdentity = this.User.Identity as ClaimsIdentity;
            var username = claimsIdentity.FindFirst(ClaimTypes.Name)?.Value;
            data.UpdatedBy = username;
            data.UpdatedDate = DateTime.Now;
            var result = await this.productRepository.UpdateAsync(data);
            return Ok(result);
        }

        [HttpPost("UploadImage")]
        public async Task<IActionResult> OnPostUploadAsync([FromForm]ProductImageView data)
        {
            if (data.Image != null && data.Image.Length > 0)
            {
                using (var ms = new MemoryStream())
                {
                    data.Image.CopyTo(ms);
                    var fileBytes = ms.ToArray();
                    data.ImageByte = fileBytes;
                }
            }
            await this.productRepository.UploadImage(data.ProductId, data.ImageByte);
            return Ok();
        }
        // DELETE api/chemical/5
        [HttpDelete("{id}")]
        public async Task DeleteAsync(int id)
        {
            var claimsIdentity = this.User.Identity as ClaimsIdentity;
            var username = claimsIdentity.FindFirst(ClaimTypes.Name)?.Value;
            await this.productRepository.Delete(id, username);
        }
    }
}