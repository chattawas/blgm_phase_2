using System.Threading.Tasks;
using blgm_data.models;
using blgm_data.Repository;
using blgm_data.views;
using Microsoft.AspNetCore.Mvc;

namespace blgm_phase_2.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private UserRepository userRepository;

        public UserController(UserRepository userRepository)
        {
            this.userRepository = userRepository;
        }
        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            var data = await userRepository.GetAll();
            return Ok(data);
        }

        [HttpPost("Add")]
        public IActionResult Add([FromBody]UserView user)
        {
            var data = userRepository.Add(user);
            return Ok(data);
        }

        [HttpPost("update")]
        public IActionResult Update([FromBody]UserView user)
        {
            var data = userRepository.UpdateDetail(user);
            return Ok(data);
        }

        [HttpPost("changePassword")]
        public IActionResult ChangePassword([FromBody]UserView user)
        {
            var data = userRepository.UpdatePassword(user);
            return Ok(data);
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            userRepository.Delete(id);
            return Ok();
        }
    }
}