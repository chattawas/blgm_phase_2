using System.Threading.Tasks;
using blgm_data.models;
using blgm_data.Repository;
using blgm_data.views;
using Microsoft.AspNetCore.Mvc;

namespace blgm_phase_2.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomerController : ControllerBase
    {
        private CustomerRepository customerRepository;

        public CustomerController(CustomerRepository customerRepository)
        {
            this.customerRepository = customerRepository;
        }

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            var data = await customerRepository.GetAll();
            return Ok(data);
        }

        [HttpGet("GetAllSupplier")]
        public async Task<IActionResult> GetAllSupplier()
        {
            var data = await customerRepository.GetAllSuppliers();
            return Ok(data);
        }

        [HttpGet("GetAllCoporation")]
        public async Task<IActionResult> GetAllCoporation()
        {
            var data = await customerRepository.GetAllCoporations();
            return Ok(data);
        }

        [HttpPost("Add")]
        public IActionResult Add([FromBody]Customer customer)
        {
            var data = customerRepository.Add(customer);
            return Ok(data);
        }

        [HttpPost("update")]
        public IActionResult Update([FromBody]Customer customer)
        {
            var data = customerRepository.Update(customer);
            return Ok(data);
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            customerRepository.Delete(id);
            return Ok();
        }
    }
}