import { ProductOrderService } from './services/product-order.service';
import { ProductDetailComponent } from './managements/products/product-detail/product-detail.component';
import { ProductService } from './services/product.service';
import { ProductComponent } from './managements/products/product/product.component';
import { MaterialOrderAddComponent } from './managements/materials/material-order-add/material-order-add.component';
import { MaterialDetailComponent } from './managements/materials/material-detail/material-detail.component';
import { MaterialAddComponent } from './managements/materials/material-add/material-add.component';
import { MaterialService } from './services/material.service';
import { MaterialOrderComponent } from './managements/materials/material-order/material-order.component';
import { MaterialComponent } from './managements/materials/material/material.component';
import { ChemicalOrderAddComponent } from './managements/chemicals/chemical-order-add/chemical-order-add.component';
import { ChemicalService } from './services/chemical.service';
import { ChemicalComponent } from './managements/chemicals/chemical/chemical.component';
import { CustomerService } from './services/customer.service';
import { CustommerModalComponent } from './modals/custommer-modal/custommer-modal.component';
import { UserModalsComponent } from './modals/user-modals/user-modals.component';
import { AuthGuard } from './services/auth-guard.service';
import { LoadingScreenService } from './services/loading-screen.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppComponent } from './app.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { HomeComponent } from './home/home.component';
import { ComponentModule } from './components/component.module';
import { LoadingScreenInterceptor } from './Interceptors/loading.interceptor';
import { RoutingModule } from './routing/routing.module';
import { MatSidenavModule, MatTabsModule, MatToolbarModule } from '@angular/material';
import { BrowserAnimationsModule, NoopAnimationsModule } from '@angular/platform-browser/animations';
import { CommonModule } from '@angular/common';
import { NgbModule, NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';
import { WINDOW_PROVIDERS } from './services/window.service';
import { ProductsModalsComponent } from './modals/products-modals/products-modals.component';
import { ServicesModalsComponent } from './modals/services-modals/services-modals.component';
import { ManagementModule } from './managements/management.module';
import { ContactService } from './services/contact.service';
import { ToastrModule } from 'ngx-toastr';
import { JwtModule } from '@auth0/angular-jwt';
import { UserService } from './services/user.service';
import { ChemicalAddComponent } from './managements/chemicals/chemical-add/chemical-add.component';
import { OnlyNumberDirective } from './directives/only-number.directive';
import { ChemicalDetailComponent } from './managements/chemicals/chemical-detail/chemical-detail.component';
import { ChemicalOrderComponent } from './managements/chemicals/chemical-order/chemical-order.component';
import { NgbDateCustomParserFormatter } from './date-formatter/dateformat';
import { ChemicalOrderService } from './services/chemical-order.service';
import { MaterialOrderService } from './services/material-order.service';
import { ProductAddComponent } from './managements/products/product-add/product-add.component';
export function tokenGetter() {
  return localStorage.getItem('jwt');
}
@NgModule({
  declarations: [
    AppComponent,
    OnlyNumberDirective,
    NavMenuComponent,
    HomeComponent,
    ProductsModalsComponent,
    ServicesModalsComponent,
    UserModalsComponent,
    CustommerModalComponent,
  ],
  imports: [
    BrowserModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    ComponentModule,
    RoutingModule,
    MatSidenavModule,
    MatTabsModule,
    MatToolbarModule,
    NgbModule.forRoot(),
    ManagementModule,
    BrowserAnimationsModule, // required animations module
    ToastrModule.forRoot(), // ToastrModule added
    JwtModule.forRoot({
      config: {
        tokenGetter: tokenGetter,
        whitelistedDomains: ['localhost:5001'],
        blacklistedRoutes: []
      }
    })
  ],
  providers: [LoadingScreenService, ContactService
    , UserService
    , CustomerService
    , ChemicalService
    , ChemicalOrderService
    , MaterialService
    , MaterialOrderService
    , ProductService
    , ProductOrderService
    , AuthGuard
    , {
      provide: HTTP_INTERCEPTORS,
      useClass: LoadingScreenInterceptor,
      multi: true
    }
    , WINDOW_PROVIDERS
    , { provide: NgbDateParserFormatter, useClass: NgbDateCustomParserFormatter}
  ],
  bootstrap: [AppComponent],
  entryComponents: [
    ProductsModalsComponent,
    ServicesModalsComponent,
    UserModalsComponent,
    CustommerModalComponent
  ],
})
export class AppModule { }
