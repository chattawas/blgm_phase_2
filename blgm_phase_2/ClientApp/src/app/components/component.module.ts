import { HeaderComponent } from './navigation/header/header.component';

import { NgModule } from '@angular/core';
import { LoadingScreenService } from '../services/loading-screen.service';
import { LoadingScreenComponent } from './loading-screen/loading-screen.component';
import { CommonModule } from '@angular/common';
import { MatButtonModule, MatIconModule, MatToolbarModule, MatListModule } from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';
import { BrowserAnimationsModule, NoopAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RoutingModule } from '../routing/routing.module';
import { SidenavListComponent } from './navigation/sidenav-list/sidenav-list.component';
@NgModule({
  declarations: [
    LoadingScreenComponent,
    HeaderComponent,
    SidenavListComponent
  ],
  imports: [
    BrowserModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatIconModule,
    MatToolbarModule,
    FlexLayoutModule,
    RoutingModule,
    MatListModule,
  ],
  providers: [LoadingScreenService],
  exports: [
      LoadingScreenComponent,
      HeaderComponent,
      SidenavListComponent,
      FlexLayoutModule
    ]
})
export class ComponentModule { }
