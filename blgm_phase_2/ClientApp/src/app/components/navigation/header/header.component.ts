import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  @Output() public sidenavToggle = new EventEmitter();
  isManagement = true;
  currentUser: Object;

  constructor(private router: Router,
    private userService: UserService) {
    this.router.events.subscribe((val: any) => {
      if (val.url) {
        this.isManagement = val.url.includes('management');
      }
    });
    this.userService.currentUser.subscribe(x => this.currentUser = x);
  }


  ngOnInit() {
    console.log(this.currentUser);
  }

  logout() {
    this.userService.logout();
    this.router.navigateByUrl('/management/login');
  }

  public onToggleSidenav = () => {
    this.sidenavToggle.emit();
  }

  scroll(id) {
    if (id === 'home') {
      window.scrollTo(0, 0);
    } else {
      const el = document.getElementById(id);
      el.scrollIntoView();
    }
  }
}
