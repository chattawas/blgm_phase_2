import { ChemicalService } from '../../../services/chemical.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup, FormArray } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-chemical-add',
  templateUrl: './chemical-add.component.html',
  styleUrls: ['./chemical-add.component.css']
})
export class ChemicalAddComponent implements OnInit {
  form: FormGroup;
  packages: FormArray;
  changeIndex = true;
  chemicalId: any;
  isReady = true;
  constructor(private fb: FormBuilder,
    private chemicalService: ChemicalService,
    private toastr: ToastrService,
    private activatedRoute: ActivatedRoute,
    private router: Router) {
      this.activatedRoute.params.subscribe(params => {
        this.chemicalId = params.id;
      });
    }

  ngOnInit() {
    if (!this.chemicalId) {
      this.form = this.fb.group({
        Name: ['', [Validators.required]],
        Code: ['', [Validators.required]],
        Size: [''],
        Color: [''],
        Cost: ['', [Validators.required]],
        Price: ['', [Validators.required]],
        Quantity: ['', [Validators.required]],
      });
    } else {
      this.isReady = false;
      this.chemicalService.get(this.chemicalId).subscribe((res: any) => {
        this.form = this.fb.group({
          Id: [res.id, [Validators.required]],
          Name: [res.name, [Validators.required]],
          Code: [res.code, [Validators.required]],
          Size: [res.size],
          Color: [res.color],
          Cost: [res.cost, [Validators.required]],
          Price: [res.price, [Validators.required]],
          Quantity: [res.quantity, [Validators.required]],
        });
        this.isReady = true;
      });
    }
  }

  formSubmit() {
    if (this.form.valid) {
      if (this.chemicalId) {
        this.chemicalService.update(this.form.value).subscribe(res => {
          this.toastr.success('Success');
          this.router.navigateByUrl('/management/chemical');
        });
      } else {
        this.chemicalService.add(this.form.value).subscribe(res => {
          this.toastr.success('Success');
          this.router.navigateByUrl('/management/chemical');
        });
      }
    }
  }

}
