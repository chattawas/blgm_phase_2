import { Component, OnInit } from '@angular/core';
import { FormGroup, FormArray, FormBuilder, Validators } from '@angular/forms';
import { ChemicalService } from 'src/app/services/chemical.service';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-chemical-detail',
  templateUrl: './chemical-detail.component.html',
  styleUrls: ['./chemical-detail.component.css']
})
export class ChemicalDetailComponent implements OnInit {
  form: FormGroup;
  packages: FormArray;
  changeIndex = true;
  chemicalId: any;
  isReady = true;
  histories: Object;
  constructor(private fb: FormBuilder,
    private chemicalService: ChemicalService,
    private toastr: ToastrService,
    private activatedRoute: ActivatedRoute,
    private router: Router) {
      this.activatedRoute.params.subscribe(params => {
        this.chemicalId = params.id;
        this.chemicalService.getAllHistory(this.chemicalId).subscribe(res => {
          this.histories = res;
        });
      });
    }

    ngOnInit() {
      if (!this.chemicalId) {
        this.form = this.fb.group({
          Name: ['', [Validators.required]],
          Code: ['', [Validators.required]],
          Size: [''],
          Color: [''],
          Cost: ['', [Validators.required]],
          Price: ['', [Validators.required]],
          Quantity: ['', [Validators.required]],
        });
      } else {
        this.isReady = false;
        this.chemicalService.get(this.chemicalId).subscribe((res: any) => {
          this.form = this.fb.group({
            Id: [res.id, [Validators.required]],
            Name: [res.name, [Validators.required]],
            Code: [res.code, [Validators.required]],
            Size: [res.size],
            Color: [res.color],
            Cost: [res.cost, [Validators.required]],
            Price: [res.price, [Validators.required]],
            Quantity: [res.quantity, [Validators.required]],
          });
          this.form.disable();
          this.isReady = true;
        });
      }
    }
}
