import { CustomerService } from 'src/app/services/customer.service';
import { ChemicalService } from 'src/app/services/chemical.service';
import { ChemicalOrderService } from './../../../services/chemical-order.service';
import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-chemical-order',
  templateUrl: './chemical-order.component.html',
  styleUrls: ['./chemical-order.component.css']
})
export class ChemicalOrderComponent implements OnInit {
  chemicalOrders: Object[];
  chemicalList: any[];
  customerList: any[];
  page = 1;
  pageSize = 5;
  CustomerId = '0';
  OrderedCompleteDateObj = null;
  IsRecive = '';
  IsComplete = '';
  constructor(private chemicalOrderService: ChemicalOrderService,
    private toastr: ToastrService,
    private chemicalService: ChemicalService,
    private customerService: CustomerService,
    private router: Router) { }

  ngOnInit() {
    this.loadData();
    this.initalDDL();
  }

  loadData() {
    const search = {
      CustomerId: this.CustomerId,
      OrderedCompleteDateObj: this.OrderedCompleteDateObj,
      IsRecive: this.IsRecive,
      IsComplete: this.IsComplete
    };
    this.chemicalOrderService.search(search).subscribe((res: any[]) => {
      this.chemicalOrders = res;
    });
  }

  clear() {
    this.CustomerId = '0';
    this.OrderedCompleteDateObj = null;
    this.IsRecive = null;
    this.IsComplete = null;
    this.loadData();
  }

  get getChemicalOrders() {
    return this.chemicalOrders.slice((this.page - 1) * this.pageSize, (this.page - 1) * this.pageSize + this.pageSize);
  }


  initalDDL() {
    this.chemicalService.getAll().subscribe((res: any[]) => {
      this.chemicalList = res;
    });
    this.customerService.getAll().subscribe((res: any[]) => {
      this.customerList = res;
    });
  }

  deleteChemicalOrder(data) {
    if (confirm('Are you sure to delete Order Id ' + data.id)) {
      this.chemicalOrderService.delete(data.id).subscribe(res => {
        this.loadData();
      });
    }
  }

  editChemicalOrder(data) {
    this.router.navigateByUrl('/management/chemical-order/edit/' + data.id);
  }

  orderComplete(data) {
    if (confirm('Are you sure to set Order Id ' + data.id + ' is Completed ?')) {
      this.chemicalOrderService.orderComplete(data.id).subscribe(res => {
        this.loadData();
      });
    }
  }
}
