import { Component, OnInit } from '@angular/core';
import { ChemicalService } from 'src/app/services/chemical.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-chemical',
  templateUrl: './chemical.component.html',
  styleUrls: ['./chemical.component.css']
})
export class ChemicalComponent implements OnInit {
  chemicals: any[];
  page = 1;
  pageSize = 5;
  code = '';
  name = '';
  size = '';
  color = '';
  constructor(private chemicalService: ChemicalService,
    private toastr: ToastrService,
    private router: Router) { }

  ngOnInit() {
    this.loadData();
  }

  get getChemicals() {
    return this.chemicals.slice((this.page - 1) * this.pageSize, (this.page - 1) * this.pageSize + this.pageSize);
  }

  loadData() {
    const search = {
      name: this.name,
      code: this.code,
      size: this.size,
      color: this.color,
    };
    this.chemicalService.search(search).subscribe((res: any[]) => {
      this.chemicals = res;
    });
  }

  clear() {
    this.name = '';
    this.code = '';
    this.size = '';
    this.color = '';
    this.loadData();
  }

  deleteChemical(data) {
    if (confirm('Are you sure to delete ' + data.name)) {
      this.chemicalService.delete(data.id).subscribe(res => {
        this.loadData();
      });
    }
  }

  editChemical(data) {
    this.router.navigateByUrl('/management/chemical/edit/' + data.id);
  }

  viewDetail(data) {
    this.router.navigateByUrl('/management/chemical/detail/' + data.id);
  }

}
