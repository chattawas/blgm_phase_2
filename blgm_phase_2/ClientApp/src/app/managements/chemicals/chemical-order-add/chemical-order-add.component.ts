import { ChemicalOrderService } from './../../../services/chemical-order.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormArray, FormBuilder, Validators } from '@angular/forms';
import { ChemicalService } from 'src/app/services/chemical.service';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';
import { CustomerService } from 'src/app/services/customer.service';
import { DISABLED } from '@angular/forms/src/model';

@Component({
  selector: 'app-chemical-order-add',
  templateUrl: './chemical-order-add.component.html',
  styleUrls: ['./chemical-order-add.component.css']
})
export class ChemicalOrderAddComponent implements OnInit {

  orderForm: FormGroup;
  orderLine: FormArray;
  changeIndex = true;
  chemicalOrderId: any;
  isReady = true;
  chemicalList = [];
  customerList: any[];
  constructor(private fb: FormBuilder,
    private chemicalService: ChemicalService,
    private toastr: ToastrService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private customerService: CustomerService,
    private chemicalOrderService: ChemicalOrderService) {
      this.activatedRoute.params.subscribe(params => {
        this.chemicalOrderId = params.id;
      });
    }

  ngOnInit() {
    if (!this.chemicalOrderId) {
      this.orderForm = this.fb.group({
        OrderedCompleteDateObj: ['', [Validators.required]],
        CustomerId: ['', [Validators.required]],
        Reason: [''],
        Amount: ['', [Validators.required]],
        IsComplete: [true, [Validators.required]],
        IsRecive: [true, [Validators.required]],
        OrderLine: this.fb.array([], Validators.required),
      });
      this.addOrderLine();
    } else {
      this.isReady = false;
      this.chemicalOrderService.get(this.chemicalOrderId).subscribe((res: any) => {
        console.log(res);
        this.orderForm = this.fb.group({
          Id: [res.id, [Validators.required]],
          OrderedCompleteDateObj: [res.orderedCompleteDateObj, [Validators.required]],
          CustomerId: [res.customerId, [Validators.required]],
          Reason: [res.reason],
          Amount: [res.amount, [Validators.required]],
          IsComplete: [{value: res.isComplete, disabled: true}, [Validators.required]],
          IsRecive: [{value: res.isRecive, disabled: true}, [Validators.required]],
          OrderLine: this.fb.array([], Validators.required),
        });
        res.orderLine.forEach(value => {
          this.addOrderLine(value);
        });
        this.isReady = true;
      });
    }

    this.initalDDL();
  }

  initalDDL() {
    this.chemicalService.getAll().subscribe((res: any[]) => {
      this.chemicalList = res;
    });
    this.customerService.getAll().subscribe((res: any[]) => {
      this.customerList = res;
    });
  }

  addOrderLine(data?): void {
    this.orderLine = this.orderForm.get('OrderLine') as FormArray;
    this.orderLine.push(this.createOrder(data));
  }

  createOrder(data?): FormGroup {
    if (!data) {
      return this.fb.group({
        Id: [0, [Validators.required]],
        ChemicalId: ['', [Validators.required]],
        ChemicalName: ['', [Validators.required]],
        Quantity: ['', [Validators.required]],
        MaxQuantity: [0, [Validators.required]],
        Price: ['', [Validators.required]],
        TotalPrice: ['', [Validators.required]],
      });
    } else {
      const fb = this.fb.group({
        Id: [data.id, [Validators.required]],
        ChemicalId: [data.chemicalId, [Validators.required]],
        ChemicalName: [data.chemical.name, [Validators.required]],
        Quantity: [data.quantity, [Validators.required]],
        MaxQuantity: [0, [Validators.required]],
        Price: [data.price, [Validators.required]],
        TotalPrice: [data.totalPrice, [Validators.required]],
      });
      if (!this.orderForm.controls.IsRecive.value) {
        this.chemicalService.getMaxPackageValue(data.chemicalId, this.chemicalOrderId).subscribe((res: number) => {
          fb.get('Quantity').setValidators([Validators.required, Validators.max(res)]);
          fb.patchValue({
            MaxQuantity: res
          });
        });
      }

      return fb;
    }
  }

  onChemicalChange(event, index) {
    const chemical = this.chemicalList.find(x => x.id === +event);
    let price = 0;
    if (this.orderForm.controls.IsRecive.value) {
      price = +chemical.cost;
    } else {
      price = +chemical.price;
    }
    this.orderLine.controls[index].patchValue({
      ChemicalName: chemical.name,
      Price: price,
    });
    if (!this.orderForm.controls.IsRecive.value) {
      this.chemicalService.getMaxPackageValue(event, this.chemicalOrderId).subscribe((res: number) => {
        this.orderLine.controls[index].get('Quantity').setValidators([Validators.required, Validators.max(res)]);
        this.orderLine.controls[index].patchValue({
          MaxQuantity: res
        });
      });
    }
  }

  isReciveChange(event) {
    const isRecive = this.orderForm.controls.IsRecive.value;
    const orderlines = this.orderLine.controls;
    orderlines.forEach((element: FormGroup) => {
      let price = 0;
      const chemicalId = element.controls.ChemicalId.value;
      if (chemicalId) {
        const chemical = this.chemicalList.find(x => x.id === +chemicalId);
        if (isRecive) {
          price = +chemical.cost;
          element.get('Quantity').setValidators([Validators.required]);
        } else {
          price = +chemical.price;
          this.chemicalService.getMaxPackageValue(chemicalId, this.chemicalOrderId).subscribe((res: number) => {
            element.get('Quantity').setValidators([Validators.required, Validators.max(res)]);
            element.patchValue({
              MaxQuantity: res
            });
          });
        }
        element.patchValue({
          Price: price
        });
      }
    });

  }

  onQuantityChange(quantity, index) {
    const price = +this.orderLine.controls[index].get('Price').value;
    const totalPrice = price * +quantity;
    this.orderLine.controls[index].patchValue({
      TotalPrice: totalPrice
    });
    this.calculateAmount();
  }

  onPriceChange(price, index) {
    const quantity = +this.orderLine.controls[index].get('Quantity').value;
    const totalPrice = +price * quantity;
    this.orderLine.controls[index].patchValue({
      TotalPrice: totalPrice
    });
    this.calculateAmount();
  }

  calculateAmount() {
    let amount = 0;
    const orderlines = this.orderLine.controls;
    orderlines.forEach((element: FormGroup) => {
      const totalPrice = element.controls.TotalPrice.value;
      amount += +totalPrice;
    });
    this.orderForm.patchValue({
      Amount: amount
    });
  }

  removeOrderLine(index) {
    this.orderLine.removeAt(index);
    this.calculateAmount();
  }

  formSubmit() {
    console.log(this.orderForm);
    if (this.orderForm.valid) {
      if (this.chemicalOrderId) {
        this.chemicalOrderService.update(this.orderForm.value).subscribe(res => {
          this.toastr.success('Success');
          this.router.navigateByUrl('/management/chemical-order');
        });
      } else {
        this.chemicalOrderService.add(this.orderForm.value).subscribe(res => {
          this.toastr.success('Success');
          this.router.navigateByUrl('/management/chemical-order');
        });
      }
    }
  }
}
