import { MaterialService } from './../../../services/material.service';
import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-material',
  templateUrl: './material.component.html',
  styleUrls: ['./material.component.css']
})
export class MaterialComponent implements OnInit {

  materials: any[];
  page = 1;
  pageSize = 5;
  code = '';
  name = '';
  size = '';
  color = '';
  constructor(private materialService: MaterialService,
    private toastr: ToastrService,
    private router: Router) { }

  ngOnInit() {
    this.loadData();
  }

  get getMaterials() {
    return this.materials.slice((this.page - 1) * this.pageSize, (this.page - 1) * this.pageSize + this.pageSize);
  }

  loadData() {
    const search = {
      name: this.name,
      code: this.code,
      size: this.size,
      color: this.color,
    };
    this.materialService.search(search).subscribe((res: any[]) => {
      this.materials = res;
    });
  }

  clear() {
    this.name = '';
    this.code = '';
    this.size = '';
    this.color = '';
    this.loadData();
  }

  delete(data) {
    if (confirm('Are you sure to delete ' + data.name)) {
      this.materialService.delete(data.id).subscribe(res => {
        this.loadData();
      });
    }
  }

  edit(data) {
    this.router.navigateByUrl('/management/material/edit/' + data.id);
  }

  viewDetail(data) {
    this.router.navigateByUrl('/management/material/detail/' + data.id);
  }


}
