import { MaterialService } from './../../../services/material.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormArray, FormBuilder, Validators } from '@angular/forms';
import { ChemicalService } from 'src/app/services/chemical.service';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-material-add',
  templateUrl: './material-add.component.html',
  styleUrls: ['./material-add.component.css']
})
export class MaterialAddComponent implements OnInit {
  form: FormGroup;
  changeIndex = true;
  marterialId: any;
  isReady = true;
  constructor(private fb: FormBuilder,
    private materialService: MaterialService,
    private toastr: ToastrService,
    private activatedRoute: ActivatedRoute,
    private router: Router) {
      this.activatedRoute.params.subscribe(params => {
        this.marterialId = params.id;
      });
    }

  ngOnInit() {
    if (!this.marterialId) {
      this.form = this.fb.group({
        Name: ['', [Validators.required]],
        Code: ['', [Validators.required]],
        Size: [''],
        Color: [''],
        Cost: ['', [Validators.required]],
        Price: ['', [Validators.required]],
        Quantity: ['', [Validators.required]],
      });
    } else {
      this.isReady = false;
      this.materialService.get(this.marterialId).subscribe((res: any) => {
        this.form = this.fb.group({
          Id: [res.id, [Validators.required]],
          Name: [res.name, [Validators.required]],
          Code: [res.code, [Validators.required]],
          Size: [res.size],
          Color: [res.color],
          Cost: [res.cost, [Validators.required]],
          Price: [res.price, [Validators.required]],
          Quantity: [res.quantity, [Validators.required]],
        });
        this.isReady = true;
      });
    }
  }

  formSubmit() {
    if (this.form.valid) {
      if (this.marterialId) {
        this.materialService.update(this.form.value).subscribe(res => {
          this.toastr.success('Success');
          this.router.navigateByUrl('/management/material');
        });
      } else {
        this.materialService.add(this.form.value).subscribe(res => {
          this.toastr.success('Success');
          this.router.navigateByUrl('/management/material');
        });
      }
    }
  }

}
