import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MaterialService } from 'src/app/services/material.service';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-material-detail',
  templateUrl: './material-detail.component.html',
  styleUrls: ['./material-detail.component.css']
})
export class MaterialDetailComponent implements OnInit {
  form: FormGroup;
  changeIndex = true;
  marterialId: any;
  isReady = true;
  histories: Object;
  constructor(private fb: FormBuilder,
    private materialService: MaterialService,
    private toastr: ToastrService,
    private activatedRoute: ActivatedRoute,
    private router: Router) {
      this.activatedRoute.params.subscribe(params => {
        this.marterialId = params.id;
      });
      this.materialService.getAllHistory(this.marterialId).subscribe(res => {
        this.histories = res;
      });
    }

  ngOnInit() {
    if (!this.marterialId) {
      this.form = this.fb.group({
        Name: ['', [Validators.required]],
        Code: ['', [Validators.required]],
        Size: [''],
        Color: [''],
        Cost: ['', [Validators.required]],
        Price: ['', [Validators.required]],
        Quantity: ['', [Validators.required]],
      });
    } else {
      this.isReady = false;
      this.materialService.get(this.marterialId).subscribe((res: any) => {
        this.form = this.fb.group({
          Id: [res.id, [Validators.required]],
          Name: [res.name, [Validators.required]],
          Code: [res.code, [Validators.required]],
          Size: [res.size],
          Color: [res.color],
          Cost: [res.cost, [Validators.required]],
          Price: [res.price, [Validators.required]],
          Quantity: [res.quantity, [Validators.required]],
        });
        this.isReady = true;
        this.form.disable();
      });
    }
  }

}
