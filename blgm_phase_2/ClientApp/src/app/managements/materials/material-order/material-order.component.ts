import { CustomerService } from './../../../services/customer.service';
import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { MaterialOrderService } from 'src/app/services/material-order.service';

@Component({
  selector: 'app-material-order',
  templateUrl: './material-order.component.html',
  styleUrls: ['./material-order.component.css']
})
export class MaterialOrderComponent implements OnInit {
  materialOrders: Object[];
  chemicalList: any[];
  customerList: any[];
  page = 1;
  pageSize = 5;
  CustomerId = '0';
  OrderedCompleteDateObj = null;
  IsRecive = '';
  IsComplete = '';
  constructor(private materailOrderService: MaterialOrderService,
    private customerService: CustomerService,
    private toastr: ToastrService,
    private router: Router) { }

  ngOnInit() {
    this.loadData();
    this.initalDDL();
  }

  loadData() {
    const search = {
      CustomerId: this.CustomerId,
      OrderedCompleteDateObj: this.OrderedCompleteDateObj,
      IsRecive: this.IsRecive,
      IsComplete: this.IsComplete
    };
    this.materailOrderService.search(search).subscribe((res: any) => {
      this.materialOrders = res;
    });
  }
  clear() {
    this.CustomerId = '0';
    this.OrderedCompleteDateObj = null;
    this.IsRecive = null;
    this.IsComplete = null;
    this.loadData();
  }

  get getMaterialOrders() {
    return this.materialOrders.slice((this.page - 1) * this.pageSize, (this.page - 1) * this.pageSize + this.pageSize);
  }

  initalDDL() {
    this.customerService.getAll().subscribe((res: any[]) => {
      this.customerList = res;
    });
  }

  deleteOrder(data) {
    if (confirm('Are you sure to delete Order Id ' + data.id)) {
      this.materailOrderService.delete(data.id).subscribe(res => {
        this.loadData();
      });
    }
  }

  editOrder(data) {
    this.router.navigateByUrl('/management/material-order/edit/' + data.id);
  }

  orderComplete(data) {
    if (confirm('Are you sure to set Order Id ' + data.id + ' is Completed ?')) {
      this.materailOrderService.orderComplete(data.id).subscribe(res => {
        this.loadData();
      });
    }
  }

}
