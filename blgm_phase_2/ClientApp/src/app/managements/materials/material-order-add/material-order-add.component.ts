import { MaterialOrderService } from './../../../services/material-order.service';
import { MaterialService } from './../../../services/material.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormArray, FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';
import { CustomerService } from 'src/app/services/customer.service';

@Component({
  selector: 'app-material-order-add',
  templateUrl: './material-order-add.component.html',
  styleUrls: ['./material-order-add.component.css']
})
export class MaterialOrderAddComponent implements OnInit {

  orderForm: FormGroup;
  orderLine: FormArray;
  changeIndex = true;
  materialOrderId: any;
  isReady = true;
  materialList = [];
  customerList: any[];
  constructor(private fb: FormBuilder,
    private materialService: MaterialService,
    private toastr: ToastrService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private customerService: CustomerService,
    private materialOrderService: MaterialOrderService) {
      this.activatedRoute.params.subscribe(params => {
        this.materialOrderId = params.id;
      });
    }

  ngOnInit() {
    if (!this.materialOrderId) {
      this.orderForm = this.fb.group({
        OrderedCompleteDateObj: ['', [Validators.required]],
        CustomerId: ['', [Validators.required]],
        Reason: [''],
        Amount: ['', [Validators.required]],
        IsComplete: [true, [Validators.required]],
        IsRecive: [true, [Validators.required]],
        OrderLine: this.fb.array([], Validators.required),
      });
      this.addOrderLine();
    } else {
      this.isReady = false;
      this.materialOrderService.get(this.materialOrderId).subscribe((res: any) => {
        console.log(res);
        this.orderForm = this.fb.group({
          Id: [res.id, [Validators.required]],
          OrderedCompleteDateObj: [res.orderedCompleteDateObj, [Validators.required]],
          CustomerId: [res.customerId, [Validators.required]],
          Reason: [res.reason],
          Amount: [res.amount, [Validators.required]],
          IsComplete: [{value: res.isComplete, disabled: true}, [Validators.required]],
          IsRecive: [{value: res.isRecive, disabled: true}, [Validators.required]],
          OrderLine: this.fb.array([], Validators.required),
        });
        res.orderLine.forEach(value => {
          this.addOrderLine(value);
        });
        this.isReady = true;
      });
    }

    this.initalDDL();
  }

  initalDDL() {
    this.materialService.getAll().subscribe((res: any[]) => {
      this.materialList = res;
    });
    this.customerService.getAll().subscribe((res: any[]) => {
      this.customerList = res;
    });
  }

  addOrderLine(data?): void {
    this.orderLine = this.orderForm.get('OrderLine') as FormArray;
    this.orderLine.push(this.createOrder(data));
  }

  createOrder(data?): FormGroup {
    if (!data) {
      return this.fb.group({
        Id: [0, [Validators.required]],
        MaterialId: ['', [Validators.required]],
        MaterialName: ['', [Validators.required]],
        Quantity: ['', [Validators.required]],
        MaxQuantity: [0, [Validators.required]],
        Price: ['', [Validators.required]],
        TotalPrice: ['', [Validators.required]],
      });
    } else {
      const fb = this.fb.group({
        Id: [data.id, [Validators.required]],
        MaterialId: [data.materialId, [Validators.required]],
        MaterialName: [data.material.name, [Validators.required]],
        Quantity: [data.quantity, [Validators.required]],
        MaxQuantity: [0, [Validators.required]],
        Price: [data.price, [Validators.required]],
        TotalPrice: [data.totalPrice, [Validators.required]],
      });
      if (!this.orderForm.controls.IsRecive.value) {
        this.materialService.getMaxPackageValue(data.materialId, this.materialOrderId).subscribe((res: number) => {
          fb.get('Quantity').setValidators([Validators.required, Validators.max(res)]);
          fb.patchValue({
            MaxQuantity: res
          });
        });
      }

      return fb;
    }
  }

  onMaterialChange(event, index) {
    const material = this.materialList.find(x => x.id === +event);
    let price = 0;
    if (this.orderForm.controls.IsRecive.value) {
      price = +material.cost;
    } else {
      price = +material.price;
    }
    this.orderLine.controls[index].patchValue({
      MaterialName: material.name,
      Price: price,
    });
    if (!this.orderForm.controls.IsRecive.value) {
      this.materialService.getMaxPackageValue(event, this.materialOrderId).subscribe((res: number) => {
        this.orderLine.controls[index].get('Quantity').setValidators([Validators.required, Validators.max(res)]);
        this.orderLine.controls[index].patchValue({
          MaxQuantity: res
        });
      });
    }
  }

  isReciveChange(event) {
    const isRecive = this.orderForm.controls.IsRecive.value;
    const orderlines = this.orderLine.controls;
    orderlines.forEach((element: FormGroup) => {
      let price = 0;
      const materialId = element.controls.MaterialId.value;
      if (materialId) {
        const material = this.materialList.find(x => x.id === +materialId);
        if (isRecive) {
          price = +material.cost;
          element.get('Quantity').setValidators([Validators.required]);
        } else {
          price = +material.price;
          this.materialService.getMaxPackageValue(materialId, this.materialOrderId).subscribe((res: number) => {
            element.get('Quantity').setValidators([Validators.required, Validators.max(res)]);
            element.patchValue({
              MaxQuantity: res
            });
          });
        }
        element.patchValue({
          Price: price
        });
      }
    });

  }

  onQuantityChange(quantity, index) {
    const price = +this.orderLine.controls[index].get('Price').value;
    const totalPrice = price * +quantity;
    this.orderLine.controls[index].patchValue({
      TotalPrice: totalPrice
    });
    this.calculateAmount();
  }

  onPriceChange(price, index) {
    const quantity = +this.orderLine.controls[index].get('Quantity').value;
    const totalPrice = +price * quantity;
    this.orderLine.controls[index].patchValue({
      TotalPrice: totalPrice
    });
    this.calculateAmount();
  }

  onTotalPriceChange(price, index) {
    this.calculateAmount();
  }

  calculateAmount() {
    let amount = 0;
    const orderlines = this.orderLine.controls;
    orderlines.forEach((element: FormGroup) => {
      const totalPrice = element.controls.TotalPrice.value;
      amount += +totalPrice;
    });
    this.orderForm.patchValue({
      Amount: amount
    });
  }

  removeOrderLine(index) {
    this.orderLine.removeAt(index);
    this.calculateAmount();
  }

  formSubmit() {
    console.log(this.orderForm.value);
    if (this.orderForm.valid) {
      if (this.materialOrderId) {
        this.materialOrderService.update(this.orderForm.value).subscribe(res => {
          this.toastr.success('Success');
          this.router.navigateByUrl('/management/material-order');
        });
      } else {
        this.materialOrderService.add(this.orderForm.value).subscribe(res => {
          this.toastr.success('Success');
          this.router.navigateByUrl('/management/material-order');
        });
      }
    }
  }

}
