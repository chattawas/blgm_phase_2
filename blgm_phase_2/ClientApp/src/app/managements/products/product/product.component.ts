import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MaterialService } from 'src/app/services/material.service';
import { CustomerService } from 'src/app/services/customer.service';
import { ProductService } from 'src/app/services/product.service';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {
  productList: Object[];
  page = 1;
  pageSize = 5;
  code = '';
  name = '';
  size = '';
  color = '';
  constructor(private fb: FormBuilder,
    private materialService: MaterialService,
    private customerService: CustomerService,
    private productService: ProductService,
    private toastr: ToastrService,
    private activatedRoute: ActivatedRoute,
    private router: Router) {
    }

  ngOnInit() {
    this.loadData();
  }

  get getProducts() {
    return this.productList.slice((this.page - 1) * this.pageSize, (this.page - 1) * this.pageSize + this.pageSize);
  }

  loadData() {
    const search = {
      name: this.name,
      code: this.code,
      size: this.size,
      color: this.color,
    };
    this.productService.search(search).subscribe((res: any[]) => {
      this.productList = res;
    });
  }

  clear() {
    this.name = '';
    this.code = '';
    this.size = '';
    this.color = '';
    this.loadData();
  }


  edit(data) {
    this.router.navigateByUrl('/management/product/edit/' + data.id);
  }

  viewDetail(data) {
    this.router.navigateByUrl('/management/product/detail/' + data.id);
  }

  delete(data) {
    if (confirm('Are you sure to delete ' + data.code)) {
      this.productService.delete(data.id).subscribe(res => {
        this.loadData();
      });
    }
  }

}
