import { Component, OnInit } from '@angular/core';
import { FormGroup, FormArray, FormBuilder, Validators } from '@angular/forms';
import { MaterialService } from 'src/app/services/material.service';
import { CustomerService } from 'src/app/services/customer.service';
import { ProductService } from 'src/app/services/product.service';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.css']
})
export class ProductDetailComponent implements OnInit {
  form: FormGroup;
  productMaterial: FormArray;
  productPrice: any;
  productImage: FormArray;
  isReady = true;
  message: string;
  imagePath: any;
  imgURL: string | ArrayBuffer;
  materialList: any[];
  customerList: any[];
  productId: any;
  deleteImage = [];
  histories: Object;

  constructor(private fb: FormBuilder,
    private materialService: MaterialService,
    private customerService: CustomerService,
    private productService: ProductService,
    private toastr: ToastrService,
    private activatedRoute: ActivatedRoute,
    private router: Router) {
      this.activatedRoute.params.subscribe(params => {
        this.productId = params.id;
      });
      this.productService.getAllHistory(this.productId).subscribe(res => {
        this.histories = res;
      });
    }

  ngOnInit() {
    if (!this.productId) {
      this.form = this.fb.group({
        Code: ['', [Validators.required]],
        Name: ['', [Validators.required]],
        AltName: [''],
        CratfTime: [''],
        Size: [''],
        Width: [''],
        Height: [''],
        Length: [''],
        Weight: [''],
        Price: ['', [Validators.required]],
        Cost: ['', [Validators.required]],
        Quantity: ['', [Validators.required]],
        ProductMaterial: this.fb.array([], Validators.required),
        ProductImage: this.fb.array([]),
        ProductPrice: this.fb.array([], Validators.required),
        DeleteImage: [''],
      });
    } else {
      this.isReady = false;
      this.productService.get(this.productId).subscribe((res: any) => {
        this.form = this.fb.group({
          ID: [res.id, [Validators.required]],
          Code: [res.code, [Validators.required]],
          Name: [res.name, [Validators.required]],
          AltName: [res.altName],
          CratfTime: [res.catfTime],
          Size: [res.size],
          Width: [res.width],
          Height: [res.height],
          Length: [res.length],
          Weight: [res.weight],
          Price: [res.price, [Validators.required]],
          Cost: [res.cost, [Validators.required]],
          Quantity: [res.quantity, [Validators.required]],
          ProductMaterial: this.fb.array([], Validators.required),
          ProductImage: this.fb.array([]),
          ProductPrice: this.fb.array([], Validators.required),
          DeleteImage: [''],
        });
        res.productMaterial.forEach(value => {
          this.addProductMaterial(value);
        });
        res.productPrice.forEach(value => {
          this.addProductPrice('', value);
        });
        res.productImage.forEach(value => {
          this.addProductImage(value);
        });
        this.isReady = true;
        this.form.disable();
      });

    }
    this.initalDDL();
  }

  initalDDL() {
    this.materialService.getAll().subscribe((res: any[]) => {
      this.materialList = res;
    });
    this.customerService.getAll().subscribe((res: any[]) => {
      this.customerList = res;
    });
  }

  addProductMaterial(data?): void {
    this.productMaterial = this.form.get('ProductMaterial') as FormArray;
    this.productMaterial.push(this.createProductMaterial(data));
  }

  removeProductMaterial(index) {
    this.productMaterial.removeAt(index);
  }

  onMaterialChange(event, index) {
    const material = this.materialList.find(x => x.id === +event);
    this.productMaterial.controls[index].patchValue({
      MaterialName: material.name,
    });
  }

  createProductMaterial(data?): FormGroup {
      if (!data) {
        return this.fb.group({
          Id: [0, [Validators.required]],
          Quantity: ['', [Validators.required]],
          MaterialId: ['', [Validators.required]],
          MaterialName: ['', [Validators.required]],
        });
      } else {
        return this.fb.group({
          Id: [data.id, [Validators.required]],
          Quantity: [data.quantity, [Validators.required]],
          MaterialId: [data.materialId, [Validators.required]],
          MaterialName: [data.material.name, [Validators.required]],
        });
      }
  }

  addProductPrice(type, data?): void {
    this.productPrice = this.form.get('ProductPrice') as FormArray;
    this.productPrice.push(this.createProductPrice(type, data));
  }

  removeProductPrice(index) {
    this.productPrice.removeAt(index);
  }

  createProductPrice(type, data?): FormGroup {
    if (!data) {
      return this.fb.group({
        Id: [0, [Validators.required]],
        CustomerId: ['', [Validators.required]],
        Price: ['', [Validators.required]],
        Type: [type, [Validators.required]],
      });
    } else {
      return this.fb.group({
        Id: [data.id, [Validators.required]],
        CustomerId: [data.customerId, [Validators.required]],
        Price: [data.price, [Validators.required]],
        Type: [data.type, [Validators.required]],
      });
    }
  }

  addProductImage(data?): void {
    this.productImage = this.form.get('ProductImage') as FormArray;
    this.productImage.push(this.createProductImage(data));
  }

  removeProductImage(index) {
    const image = this.productImage.value[index];
    if (image.Id !== 0) {
      this.deleteImage.push(image.Id);
    }
    this.productImage.removeAt(index);
  }

  createProductImage(data?): FormGroup {
    if (!data.id) {
      return this.fb.group({
        Id: [0, [Validators.required]],
        Image: [data.file],
        PreviewImage: [data.imgURL],
      });
    } else {
      return this.fb.group({
        Id: [data.id, [Validators.required]],
        Image: [''],
        PreviewImage: ['data:image/jpeg;base64,' + data.image],
      });
    }
  }

  preview(files) {
    if (files.length === 0) {
      return;
    }
    const mimeType = files[0].type;
    if (mimeType.match(/image\/*/) == null) {
      this.message = 'Only images are supported.';
      return;
    }

    const reader = new FileReader();
    const file = files[0];
    reader.readAsDataURL(file);
    reader.onload = (_event) => {
      const data = {
        file: file,
        imgURL: reader.result
      };
      console.log(data);
      this.addProductImage(data);
      this.imgURL = reader.result;
    };
  }
}
