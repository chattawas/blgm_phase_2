import { ProductOrderService } from './../../../services/product-order.service';

import { ProductService } from './../../../services/product.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup, FormArray } from '@angular/forms';
import { MaterialService } from 'src/app/services/material.service';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';
import { CustomerService } from 'src/app/services/customer.service';
import { MaterialOrderService } from 'src/app/services/material-order.service';

@Component({
  selector: 'app-product-order-add',
  templateUrl: './product-order-add.component.html',
  styleUrls: ['./product-order-add.component.css']
})
export class ProductOrderAddComponent implements OnInit {
  orderId: any;
  isReady = true;
  form: FormGroup;
  customerList: any[];
  productOrderLines: FormArray;
  productList: any[];
  materialOrderLines: FormArray;

  constructor(private fb: FormBuilder,
    private materialService: MaterialService,
    private productService: ProductService,
    private toastr: ToastrService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private customerService: CustomerService,
    private productOrderService: ProductOrderService) {
      this.activatedRoute.params.subscribe(params => {
        this.orderId = params.id;
      });
    }

  ngOnInit() {
    if (!this.orderId) {
      this.form = this.fb.group({
        OrderedCompleteDateObj: ['', [Validators.required]],
        CustomerId: ['', [Validators.required]],
        Reason: [''],
        Amount: ['', [Validators.required]],
        IsComplete: [true, [Validators.required]],
        IsRecive: [true, [Validators.required]],
        ProductOrderLines: this.fb.array([], Validators.required),
      });
      this.addOrderLine();
    } else {
      this.isReady = false;
      this.productOrderService.get(this.orderId).subscribe((res: any) => {
        this.form = this.fb.group({
          Id: [res.id, [Validators.required]],
          OrderedCompleteDateObj: [res.orderedCompleteDateObj, [Validators.required]],
          CustomerId: [res.customerId, [Validators.required]],
          Reason: [res.reason],
          Amount: [res.amount, [Validators.required]],
          IsComplete: [{value: res.isComplete, disabled: true}, [Validators.required]],
          IsRecive: [{value: res.isRecive, disabled: true}, [Validators.required]],
          ProductOrderLines: this.fb.array([], Validators.required),
        });
        res.productOrderLines.forEach(data => {
          this.productOrderLines = this.form.get('ProductOrderLines') as FormArray;
          const fb = this.fb.group({
            Id: [data.id, [Validators.required]],
            ProductId: [data.productId, [Validators.required]],
            ProductName: [data.product.name, [Validators.required]],
            IsMDF: [data.isMDF, [Validators.required]],
            Quantity: [data.quantity, [Validators.required]],
            MaxQuantity: [0, [Validators.required]],
            Price: [data.price, [Validators.required]],
            TotalPrice: [data.totalPrice, [Validators.required]],
            MaterialOrderLines: this.fb.array([]),
          });
          if (!this.form.controls.IsRecive.value) {
            this.productService.getMaxPackageValue(data.productId, this.orderId).subscribe((max: number) => {
              fb.get('Quantity').setValidators([Validators.required, Validators.max(max)]);
              fb.patchValue({
                MaxQuantity: max
              });
            });
          }
          data.materialOrderLines.forEach(async matValue => {
            const matdata = await this.materialService.getMaxPackageValue(matValue.material.id, this.orderId).toPromise();
            const quantityPerProduct = data.product.productMaterial.find(x => x.materialId === matValue.material.id ).quantity;
            const matFb = this.fb.group({
                Id: [matValue.id],
                MaterialId: [matValue.materialId, [Validators.required]],
                MaterialName: [matValue.material.code + '-' + matValue.material.name],
                QuantityPerProduct: [quantityPerProduct],
                Quantity: [matValue.quantity, [Validators.required, Validators.max(+matdata)]],
                MaxQuantity: [matdata],
            });
            this.materialOrderLines =  fb.get('MaterialOrderLines') as FormArray;
            this.materialOrderLines.push(matFb);
          });
          this.productOrderLines.push(fb);
        });
        this.isReady = true;
      });
    }
    this.initalDDL();
  }

  initalDDL() {
    this.customerService.getAll().subscribe((res: any[]) => {
      this.customerList = res;
    });
    this.productService.getAll().subscribe((res: any[]) => {
      this.productList = res;
    });
  }

  addOrderLine(data?): void {
    this.productOrderLines = this.form.get('ProductOrderLines') as FormArray;
    this.productOrderLines.push(this.createOrder(data));
  }

  removeOrderLine(index) {
    this.productOrderLines.removeAt(index);
  }

  createOrder(data?): FormGroup {
    if (!data) {
      return this.fb.group({
        ProductId: ['', [Validators.required]],
        ProductName: ['', [Validators.required]],
        IsMDF: [true, [Validators.required]],
        Quantity: ['', [Validators.required]],
        MaxQuantity: [0, [Validators.required]],
        Price: ['', [Validators.required]],
        TotalPrice: ['', [Validators.required]],
        MaterialOrderLines: this.fb.array([]),
      });
    } else {
      const fb = this.fb.group({
        Id: [data.id, [Validators.required]],
        ProductId: [data.productId, [Validators.required]],
        ProductName: [data.product.name, [Validators.required]],
        IsMDF: [data.isMDF, [Validators.required]],
        Quantity: [data.quantity, [Validators.required]],
        MaxQuantity: [0, [Validators.required]],
        Price: [data.price, [Validators.required]],
        TotalPrice: [data.totalPrice, [Validators.required]],
        MaterialOrderLines: this.fb.array([]),
      });
      if (!this.form.controls.IsRecive.value) {
        this.productService.getMaxPackageValue(data.productId, this.orderId).subscribe((res: number) => {
          fb.get('Quantity').setValidators([Validators.required, Validators.max(res)]);
          fb.patchValue({
            MaxQuantity: res
          });
        });
      }
      return fb;
    }
  }


  onProductChange(event, index) {
    const product = this.productList.find(x => x.id === +event);
    const customerId = this.form.get('CustomerId').value;
    const isMDF = this.productOrderLines.controls[index].get('IsMDF').value;
    const productType = isMDF ? 'MDF' : 'MonkeyPod';
    let price = 0;
    if (customerId) {
      const productPrice = product.productPrice.find(x => x.customerId === +customerId && x.type === productType);
      if (productPrice) {
        if (this.form.controls.IsRecive.value) {
          price = +product.cost;
        } else {
          price = +productPrice.price;
        }
      } else {
        if (this.form.controls.IsRecive.value) {
          price = +product.cost;
        } else {
          price = +product.price;
        }
      }
    } else {
      if (this.form.controls.IsRecive.value) {
        price = +product.cost;
      } else {
        price = +product.price;
      }
    }
    this.productOrderLines.controls[index].patchValue({
      ProductName: product.name,
      Price: price,
    });
    this.materialOrderLines = this.productOrderLines.controls[index].get('MaterialOrderLines') as FormArray;
    while (this.materialOrderLines.length !== 0) {
      this.materialOrderLines.removeAt(0);
    }
    if (!this.form.controls.IsRecive.value) {
      product.productMaterial.forEach(async element => {
        await this.addMaterialOrderLine(index, element);
      });
    }
    if (!this.form.controls.IsRecive.value) {
      this.productService.getMaxPackageValue(event, this.orderId).subscribe((res: number) => {
        this.productOrderLines.controls[index].get('Quantity').setValidators([Validators.required, Validators.max(res)]);
        this.productOrderLines.controls[index].patchValue({
          MaxQuantity: res
        });
      });
    }
  }

  async addMaterialOrderLine(index, data?): Promise<void> {
    this.productOrderLines = this.form.get('ProductOrderLines') as FormArray;
    this.materialOrderLines = this.productOrderLines.controls[index].get('MaterialOrderLines') as FormArray;
    const quantity = +this.productOrderLines.controls[index].get('Quantity').value;
    this.materialOrderLines.push(await this.createMaterialOrder(quantity, data));
  }
  async createMaterialOrder(quantity, data?): Promise<FormGroup> {
    const response = await this.materialService.getMaxPackageValue(data.materialId, this.orderId).toPromise();
    return this.fb.group({
            MaterialId: [data.materialId, [Validators.required]],
            MaterialName: [data.material.code + '-' + data.material.name],
            QuantityPerProduct: [data.quantity],
            Quantity: [quantity * data.quantity, [Validators.required, Validators.max(+response)]],
            MaxQuantity: [response],
          });
  }

  isReciveChange(event) {
    const isRecive = this.form.controls.IsRecive.value;
    const orderlines = this.productOrderLines.controls;
    const customerId = this.form.get('CustomerId').value;
    const productList = this.productList;
    orderlines.forEach(function(element: FormGroup, index) {
      let price = 0;
      const productId = element.controls.ProductId.value;
      const isMDF = element.controls.IsMDF.value;
      const productType = isMDF ? 'MDF' : 'MonkeyPod';
      if (productId) {
        const product = this.productList.find(x => x.id === +productId);
        if (customerId) {
          const productPrice = product.productPrice.find(x => x.customerId === +customerId && x.type === productType);
          if (productPrice) {
            if (this.form.controls.IsRecive.value) {
              price = +product.cost;
            } else {
              price = +productPrice.price;
            }
          } else {
            if (this.form.controls.IsRecive.value) {
              price = +product.cost;
            } else {
              price = +product.price;
            }
          }
        } else {
          if (this.form.controls.IsRecive.value) {
            price = +product.cost;
          } else {
            price = +product.price;
          }
        }
        this.materialOrderLines = element.get('MaterialOrderLines') as FormArray;
        while (this.materialOrderLines.length !== 0) {
          this.materialOrderLines.removeAt(0);
        }
        if (isRecive) {
          element.get('Quantity').setValidators([Validators.required]);
        } else {
          product.productMaterial.forEach(async pm => {
            await this.addMaterialOrderLine(index, pm);
          });
          this.productService.getMaxPackageValue(productId, this.orderId).subscribe((res: number) => {
            element.get('Quantity').setValidators([Validators.required, Validators.max(res)]);
            element.patchValue({
              MaxQuantity: res
            });
          });
        }
        element.patchValue({
          Price: price
        });
      }
    }.bind(this));

  }

  isMDFChange(event) {
    const isRecive = this.form.controls.IsRecive.value;
    const orderlines = this.productOrderLines.controls;
    const customerId = this.form.get('CustomerId').value;
    const productList = this.productList;
    orderlines.forEach(function(element: FormGroup, index) {
      let price = 0;
      const productId = element.controls.ProductId.value;
      const isMDF = element.controls.IsMDF.value;
      const productType = isMDF ? 'MDF' : 'MonkeyPod';
      if (productId) {
        const product = this.productList.find(x => x.id === +productId);
        if (customerId) {
          const productPrice = product.productPrice.find(x => x.customerId === +customerId && x.type === productType);
          if (productPrice) {
            if (this.form.controls.IsRecive.value) {
              price = +product.cost;
            } else {
              price = +productPrice.price;
            }
          } else {
            if (this.form.controls.IsRecive.value) {
              price = +product.cost;
            } else {
              price = +product.price;
            }
          }
        } else {
          if (this.form.controls.IsRecive.value) {
            price = +product.cost;
          } else {
            price = +product.price;
          }
        }
        element.patchValue({
          Price: price
        });
      }
    }.bind(this));

  }

  onCustomerChange(event) {
    const isRecive = this.form.controls.IsRecive.value;
    const orderlines = this.productOrderLines.controls;
    const customerId = this.form.get('CustomerId').value;
    const productList = this.productList;
    orderlines.forEach(function(element: FormGroup, index) {
      let price = 0;
      const productId = element.controls.ProductId.value;
      const isMDF = element.controls.IsMDF.value;
      const productType = isMDF ? 'MDF' : 'MonkeyPod';
      if (productId) {
        const product = this.productList.find(x => x.id === +productId);
        if (customerId) {
          const productPrice = product.productPrice.find(x => x.customerId === +customerId && x.type === productType);
          if (productPrice) {
            if (this.form.controls.IsRecive.value) {
              price = +product.cost;
            } else {
              price = +productPrice.price;
            }
          } else {
            if (this.form.controls.IsRecive.value) {
              price = +product.cost;
            } else {
              price = +product.price;
            }
          }
        } else {
          if (this.form.controls.IsRecive.value) {
            price = +product.cost;
          } else {
            price = +product.price;
          }
        }
        element.patchValue({
          Price: price
        });
      }
    }.bind(this));

  }

  onQuantityChange(quantity, index) {
    const price = +this.productOrderLines.controls[index].get('Price').value;
    const totalPrice = price * +quantity;
    this.productOrderLines.controls[index].patchValue({
      TotalPrice: totalPrice
    });
    const matOrderLines = this.materialOrderLines.controls;
    matOrderLines.forEach(element => {
      const qtyPerUnit = element.get('QuantityPerProduct').value;
      const totalQuantity = +quantity * (+qtyPerUnit);
      element.patchValue({
        Quantity: totalQuantity
      });
    });
    this.calculateAmount();
  }

  onPriceChange(price, index) {
    const quantity = +this.productOrderLines.controls[index].get('Quantity').value;
    const totalPrice = +price * quantity;
    this.productOrderLines.controls[index].patchValue({
      TotalPrice: totalPrice
    });
    this.calculateAmount();
  }

  onTotalPriceChange(price, index) {
    this.calculateAmount();
  }

  calculateAmount() {
    let amount = 0;
    const orderlines = this.productOrderLines.controls;
    orderlines.forEach((element: FormGroup) => {
      const totalPrice = element.controls.TotalPrice.value;
      amount += +totalPrice;
    });
    this.form.patchValue({
      Amount: amount
    });
  }

  formSubmit() {
    if (this.form.valid) {
      if (this.orderId) {
        this.productOrderService.update(this.form.value).subscribe(res => {
          this.toastr.success('Success');
          this.router.navigateByUrl('/management/product-order');
        });
      } else {
        this.productOrderService.add(this.form.value).subscribe(res => {
          this.toastr.success('Success');
          this.router.navigateByUrl('/management/product-order');
        });
      }
    }
  }

}
