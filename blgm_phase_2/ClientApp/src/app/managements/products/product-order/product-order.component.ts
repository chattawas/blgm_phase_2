import { ProductOrderService } from './../../../services/product-order.service';
import { Component, OnInit } from '@angular/core';
import { MaterialOrderService } from 'src/app/services/material-order.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { CustomerService } from 'src/app/services/customer.service';

@Component({
  selector: 'app-product-order',
  templateUrl: './product-order.component.html',
  styleUrls: ['./product-order.component.css']
})
export class ProductOrderComponent implements OnInit {
  productOrders: Object[];
  customerList: any[];
  page = 1;
  pageSize = 5;
  CustomerId = '0';
  OrderedCompleteDateObj = null;
  IsRecive = '';
  IsComplete = '';
  constructor(private productOrderService: ProductOrderService,
    private customerService: CustomerService,
    private toastr: ToastrService,
    private router: Router) { }

    ngOnInit() {
      this.loadData();
      this.initalDDL();
    }

    loadData() {
      const search = {
        CustomerId: this.CustomerId,
        OrderedCompleteDateObj: this.OrderedCompleteDateObj,
        IsRecive: this.IsRecive,
        IsComplete: this.IsComplete
      };
      this.productOrderService.search(search).subscribe((res: any[]) => {
        this.productOrders = res;
      });
    }

    clear() {
      this.CustomerId = '0';
      this.OrderedCompleteDateObj = null;
      this.IsRecive = null;
      this.IsComplete = null;
      this.loadData();
    }

    get getProductOrders() {
      return this.productOrders.slice((this.page - 1) * this.pageSize, (this.page - 1) * this.pageSize + this.pageSize);
    }

    initalDDL() {
      this.customerService.getAll().subscribe((res: any[]) => {
        this.customerList = res;
      });
    }

  editOrder(data) {
    this.router.navigateByUrl('/management/product-order/edit/' + data.id);
  }

  orderComplete(data) {
    if (confirm('Are you sure to set Order Id ' + data.id + ' is Completed ?')) {
      this.productOrderService.orderComplete(data.id).subscribe(res => {
        this.loadData();
      });
    }
  }

  deleteOrder(data) {
    if (confirm('Are you sure to delete Order Id ' + data.id)) {
      this.productOrderService.delete(data.id).subscribe(res => {
        this.loadData();
      });
    }
  }


}
