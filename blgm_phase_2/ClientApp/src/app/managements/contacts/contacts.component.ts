import { ContactService } from './../../services/contact.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.component.html',
  styleUrls: ['./contacts.component.css']
})
export class ContactsComponent implements OnInit {
  contacts: Object;

  constructor(private contactService: ContactService) { }

  ngOnInit() {
    this.contactService.getAll().subscribe(res => {
      this.contacts = res;
    });
  }

}
