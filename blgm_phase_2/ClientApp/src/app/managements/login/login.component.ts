import { UserService } from 'src/app/services/user.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { LoginModel } from './../../models/login-model';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  userLogin = new LoginModel({
    username: '',
    password: ''
  });
  firsName: string;
  invalidLogin: boolean;
  constructor(private router: Router,
    private http: HttpClient,
    private userService: UserService) {
  }

  ngOnInit() {

  }


  login() {
    const credentials = JSON.stringify(this.userLogin);
    this.http.post('/api/Authentication', credentials, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    }).subscribe(response => {
      const token = (<any>response).token;
      localStorage.setItem('jwt', token);
      localStorage.setItem('currentUser', JSON.stringify(response));
      this.userService.currentUserSubject.next(response);
      this.invalidLogin = false;
      this.router.navigate(['management/master-data']);
    }, err => {
      this.invalidLogin = true;
    });
  }
}
