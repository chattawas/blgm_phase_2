import { CustomerService } from './../../services/customer.service';
import { CustommerModalComponent } from './../../modals/custommer-modal/custommer-modal.component';
import { UserService } from './../../services/user.service';
import { UserModalsComponent } from './../../modals/user-modals/user-modals.component';
import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-master-data',
  templateUrl: './master-data.component.html',
  styleUrls: ['./master-data.component.css']
})
export class MasterDataComponent implements OnInit {
  users: Object;
  suppliers: Object;
  coporations: Object;

  constructor(private modalService: NgbModal,
    private userService: UserService,
    private customerService: CustomerService) { }

  ngOnInit() {
    this.getUser();
    this.getSupplier();
    this.getCoporation();
  }

  addUser() {
    const modalRef = this.modalService.open(UserModalsComponent, { size : 'sm' });
    modalRef.result.then((result) => {
    }, (reason) => {
      this.getUser();
    });
  }

  getUser() {
    this.userService.getAll().subscribe(res => {
      this.users = res;
    });
  }

  updateUserDetail(data) {
    const modalRef = this.modalService.open(UserModalsComponent, { size : 'sm' });
    modalRef.componentInstance.data = data;
    modalRef.componentInstance.mode = 'updateDetail';
    modalRef.result.then((result) => {
    }, (reason) => {
      this.getUser();
    });
  }

  changePassword(data) {
    const modalRef = this.modalService.open(UserModalsComponent, { size : 'lg' });
    modalRef.componentInstance.data = data;
    modalRef.componentInstance.mode = 'changePassword';
    modalRef.result.then((result) => {
    }, (reason) => {
      this.getUser();
    });
  }

  deleteUser(data) {
    if (confirm('Are you sure to delete ' + data.username)) {
      this.userService.delete(data.id).subscribe(res => {
        this.getUser();
      });
    }
  }

  getSupplier() {
    this.customerService.getAllSupplier().subscribe(res => {
      this.suppliers = res;
    });
  }

  addSupplier() {
    const modalRef = this.modalService.open(CustommerModalComponent, { size : 'lg' });
    modalRef.componentInstance.type = 'supplier';
    modalRef.componentInstance.mode = 'new';
    modalRef.result.then((result) => {
    }, (reason) => {
      this.getSupplier();
    });
  }

  editSupplier(data) {
    const modalRef = this.modalService.open(CustommerModalComponent, { size : 'lg' });
    modalRef.componentInstance.data = data;
    modalRef.componentInstance.type = 'supplier';
    modalRef.componentInstance.mode = 'edit';
    modalRef.result.then((result) => {
    }, (reason) => {
      this.getSupplier();
    });
  }

  deleteSupplier(data) {
    if (confirm('Are you sure to delete ' + data.name)) {
      this.customerService.delete(data.id).subscribe(res => {
        this.getSupplier();
      });
    }
  }

  getCoporation() {
    this.customerService.getAllCoporation().subscribe(res => {
      this.coporations = res;
    });
  }

  addCoporation() {
    const modalRef = this.modalService.open(CustommerModalComponent, { size : 'lg' });
    modalRef.componentInstance.type = 'coporation';
    modalRef.componentInstance.mode = 'new';
    modalRef.result.then((result) => {
    }, (reason) => {
      this.getCoporation();
    });
  }

  editCoporation(data) {
    const modalRef = this.modalService.open(CustommerModalComponent, { size : 'lg' });
    modalRef.componentInstance.data = data;
    modalRef.componentInstance.type = 'Coporation';
    modalRef.componentInstance.mode = 'edit';
    modalRef.result.then((result) => {
    }, (reason) => {
      this.getCoporation();
    });
  }

  deleteCoporation(data) {
    if (confirm('Are you sure to delete ' + data.name)) {
      this.customerService.delete(data.id).subscribe(res => {
        this.getCoporation();
      });
    }
  }
}
