import { ProductService } from './../../services/product.service';
import { MaterialOrderService } from 'src/app/services/material-order.service';
import { MaterialService } from 'src/app/services/material.service';
import { ChemicalService } from 'src/app/services/chemical.service';
import { Component, OnInit } from '@angular/core';
import { ChemicalOrderService } from 'src/app/services/chemical-order.service';
import { ProductOrderService } from 'src/app/services/product-order.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  chemicals: Object;
  materials: Object;
  productList: Object;
  chemicalOrders: Object;
  materialOrders: Object;
  productOrders: Object;

  constructor(private chemicalService: ChemicalService,
    private materialService: MaterialService,
    private productService: ProductService,
    private chemicalOrderService: ChemicalOrderService,
    private materialOrderService: MaterialOrderService,
    private productOrderService: ProductOrderService) { }

  ngOnInit() {
    this.chemicalService.outOfStock().subscribe(res => {
      this.chemicals = res;
    });
    this.materialService.outOfStock().subscribe(res => {
      this.materials = res;
    });
    this.productService.outOfStock().subscribe(res => {
      this.productList = res;
    });
    this.chemicalOrderService.getAllCompleteToday().subscribe(res => {
      this.chemicalOrders = res;
    });
    this.materialOrderService.getAllCompleteToday().subscribe(res => {
      this.materialOrders = res;
    });
    this.productOrderService.getAllCompleteToday().subscribe(res => {
      this.productOrders = res;
    });
  }

  chemicalOrderComplete(data) {
    if (confirm('Are you sure to set Chemical Order Id ' + data.id + ' to Completed ?')) {
      this.chemicalOrderService.orderComplete(data.id).subscribe(res => {
        this.chemicalOrderService.getAllCompleteToday().subscribe(r => {
          this.chemicalOrders = r;
        });
      });
    }
  }

  materialOrderComplete(data) {
    if (confirm('Are you sure to set Material Order Id ' + data.id + ' to Completed ?')) {
      this.materialOrderService.orderComplete(data.id).subscribe(res => {
        this.materialOrderService.getAllCompleteToday().subscribe(r => {
          this.materialOrders = r;
        });
      });
    }
  }

  productOrderComplete(data) {
    if (confirm('Are you sure to set Product Order Id ' + data.id + ' to Completed ?')) {
      this.productOrderService.orderComplete(data.id).subscribe(res => {
        this.productOrderService.getAllCompleteToday().subscribe(r => {
          this.productOrders = r;
        });
      });
    }
  }

}
