import { ContactsComponent } from './contacts/contacts.component';
import { ProductOrderAddComponent } from './products/product-order-add/product-order-add.component';
import { ProductOrderComponent } from './products/product-order/product-order.component';
import { NgbModal, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MasterDataComponent } from './master-data/master-data.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ComponentModule } from '../components/component.module';
import { LoginComponent } from './login/login.component';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ChemicalComponent } from './chemicals/chemical/chemical.component';
import { ChemicalAddComponent } from './chemicals/chemical-add/chemical-add.component';
import { ChemicalDetailComponent } from './chemicals/chemical-detail/chemical-detail.component';
import { ChemicalOrderComponent } from './chemicals/chemical-order/chemical-order.component';
import { ChemicalOrderAddComponent } from './chemicals/chemical-order-add/chemical-order-add.component';
import { MaterialComponent } from './materials/material/material.component';
import { MaterialAddComponent } from './materials/material-add/material-add.component';
import { MaterialDetailComponent } from './materials/material-detail/material-detail.component';
import { MaterialOrderComponent } from './materials/material-order/material-order.component';
import { MaterialOrderAddComponent } from './materials/material-order-add/material-order-add.component';
import { ProductComponent } from './products/product/product.component';
import { ProductAddComponent } from './products/product-add/product-add.component';
import { ProductDetailComponent } from './products/product-detail/product-detail.component';
import { RouterModule } from '@angular/router';
@NgModule({
  declarations: [
    LoginComponent,
    DashboardComponent,
    MasterDataComponent,
    ContactsComponent,
    ChemicalComponent,
    ChemicalAddComponent,
    ChemicalDetailComponent,
    ChemicalOrderComponent,
    ChemicalOrderAddComponent,
    MaterialComponent,
    MaterialAddComponent,
    MaterialDetailComponent,
    MaterialOrderComponent,
    MaterialOrderAddComponent,
    ProductComponent,
    ProductAddComponent,
    ProductDetailComponent,
    ProductOrderComponent,
    ProductOrderAddComponent
  ],
  imports: [
    BrowserModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    ComponentModule,
    RouterModule,
    NgbModule.forRoot(),
  ],
  providers: [],
  exports: [
    LoginComponent,
    DashboardComponent,
    MasterDataComponent,
    ContactsComponent,
    ChemicalComponent,
    ChemicalAddComponent,
    ChemicalDetailComponent,
    ChemicalOrderComponent,
    ChemicalOrderAddComponent,
    MaterialComponent,
    MaterialAddComponent,
    MaterialDetailComponent,
    MaterialOrderComponent,
    MaterialOrderAddComponent,
    ProductComponent,
    ProductAddComponent,
    ProductDetailComponent,
    ProductOrderComponent,
    ProductOrderAddComponent
  ]
})
export class ManagementModule { }
