import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-services-modals',
  templateUrl: './services-modals.component.html',
  styleUrls: ['./services-modals.component.scss']
})
export class ServicesModalsComponent implements OnInit {

  type = '';
  images = [];
  constructor() { }

  ngOnInit() {
    if (this.type === 'game') {
      // tslint:disable-next-line:max-line-length
      this.images = ['assets/services/game/1.jpg', 'assets/services/game/2.jpg', 'assets/services/game/3.jpg', 'assets/services/game/4.jpg'
        , 'assets/services/game/5.jpg'];
    }
    if (this.type === 'kit') {
      // tslint:disable-next-line:max-line-length
      this.images = ['assets/services/kit/1.jpg', 'assets/services/kit/2.jpg', 'assets/services/kit/3.jpg', 'assets/services/kit/4.jpg'
        , 'assets/services/kit/5.jpg', 'assets/services/kit/6.jpg', 'assets/services/kit/7.jpg', 'assets/services/kit/8.jpg'];
    }
    if (this.type === 'diy') {
      // tslint:disable-next-line:max-line-length
      this.images = ['assets/services/diy/1.jpg', 'assets/services/diy/2.jpg', 'assets/services/diy/3.jpg', 'assets/services/diy/4.jpg'
      , 'assets/services/diy/5.jpg', 'assets/services/diy/6.jpg', 'assets/services/diy/7.jpg', 'assets/services/diy/8.jpg'
      , 'assets/services/diy/9.jpg', 'assets/services/diy/10.jpg'];
    }
  }

}
