import { UserService } from './../../services/user.service';
import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-user-modals',
  templateUrl: './user-modals.component.html',
  styleUrls: ['./user-modals.component.css']
})
export class UserModalsComponent implements OnInit {
  userForm: FormGroup;
  mode = 'new';
  data: any;
  title: string;
  constructor(public activeModal: NgbActiveModal,
    private fb: FormBuilder,
    private userService: UserService) {}

  ngOnInit() {
    console.log(this.mode);
    if (this.mode === 'new') {
      this.title = 'New user';
      this.userForm = this.fb.group({
        Username: ['', [Validators.required]],
        Password: ['', [Validators.required]],
        FirstName: ['', [Validators.required]],
        LastName: ['', [Validators.required]],
        ConfirmPassword: ['', [Validators.required]],
      }, {validator: this.checkPasswords });
    } else if (this.mode === 'updateDetail') {
      this.title = 'User detail';
      this.userForm = this.fb.group({
        Id: [this.data.id],
        FirstName: [this.data.firstName , [Validators.required]],
        LastName: [this.data.lastName , [Validators.required]],
      });
    } else if (this.mode === 'changePassword') {
      this.title = 'Change password';
      this.userForm = this.fb.group({
        Id: [this.data.id],
        Password: ['', [Validators.required]],
        ConfirmPassword: ['', [Validators.required]],
      }, {validator: this.checkPasswords });
    }
  }

  checkPasswords(group: FormGroup) { // here we have the 'passwords' group
    const pass = group.get('Password').value;
    const confirmPass = group.get('ConfirmPassword').value;

    return pass === confirmPass ? null : { mismatch: true };
  }

  formSubmit() {
    if (this.userForm.valid) {
      if (this.mode === 'new') {
        this.userService.add(this.userForm.value).subscribe(res => {
          this.activeModal.dismiss('Cross click');
        });
      } else if (this.mode === 'updateDetail') {
      this.userService.updateDetail(this.userForm.value).subscribe(res => {
        this.activeModal.dismiss('Cross click');
      });
      } else if (this.mode === 'changePassword') {
        this.userService.updatePassword(this.userForm.value).subscribe(res => {
          this.activeModal.dismiss('Cross click');
        });
      }
    }
  }

}
