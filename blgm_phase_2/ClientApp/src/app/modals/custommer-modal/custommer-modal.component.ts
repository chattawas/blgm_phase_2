import { CustomerService } from './../../services/customer.service';
import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, Validators } from '@angular/forms';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-custommer-modal',
  templateUrl: './custommer-modal.component.html',
  styleUrls: ['./custommer-modal.component.css']
})
export class CustommerModalComponent implements OnInit {
  title: string;
  customerForm: any;
  type: string;
  mode = 'new';
  data: any;
  constructor(public activeModal: NgbActiveModal,
    private fb: FormBuilder,
    private customerService: CustomerService) {}

  ngOnInit() {
    if (this.mode === 'new') {
      this.title = this.type === 'suppiler' ? 'Add Supplier' : 'Add Coporation';
      this.customerForm = this.fb.group({
        Name: ['', [Validators.required]],
        TaxNo: ['', [Validators.required]],
        Phone: ['', ],
        Email: ['', ],
        Type: [this.type, [Validators.required]],
        Address: ['', [Validators.required]],
        ZipCode: ['', [Validators.required]],
      });
    } else {
      this.title = this.type === 'suppiler' ? 'Edit Supplier' : 'Edit Coporation';
      this.customerForm = this.fb.group({
        Id: [this.data.id],
        Name: [this.data.name, [Validators.required]],
        TaxNo: [this.data.taxNo, [Validators.required]],
        Phone: [this.data.phone ],
        Email: [this.data.email],
        Address: [this.data.address, [Validators.required]],
        ZipCode: [this.data.zipcode, [Validators.required]],
      });
    }
  }

  formSubmit() {
    if (this.customerForm.valid) {
      if (this.mode === 'new') {
        this.customerService.add(this.customerForm.value).subscribe(res => {
          this.activeModal.dismiss('Cross click');
        });
      } else {
        this.customerService.update(this.customerForm.value).subscribe(res => {
          this.activeModal.dismiss('Cross click');
        });
      }
    }
  }

}
