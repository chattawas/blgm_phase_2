import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-products-modals',
  templateUrl: './products-modals.component.html',
  styleUrls: ['./products-modals.component.css']
})
export class ProductsModalsComponent implements OnInit {

  @Input() type;

  constructor(public activeModal: NgbActiveModal) {}

  ngOnInit() {
  }

}
