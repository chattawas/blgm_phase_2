import { ContactsComponent } from './../managements/contacts/contacts.component';
import { ProductOrderAddComponent } from './../managements/products/product-order-add/product-order-add.component';
import { ProductOrderComponent } from './../managements/products/product-order/product-order.component';
import { ProductDetailComponent } from './../managements/products/product-detail/product-detail.component';
import { MaterialOrderAddComponent } from './../managements/materials/material-order-add/material-order-add.component';
import { MaterialDetailComponent } from './../managements/materials/material-detail/material-detail.component';
import { MaterialAddComponent } from './../managements/materials/material-add/material-add.component';
import { ChemicalOrderAddComponent } from './../managements/chemicals/chemical-order-add/chemical-order-add.component';
import { MasterDataComponent } from './../managements/master-data/master-data.component';
import { DashboardComponent } from './../managements/dashboard/dashboard.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from '../home/home.component';
import { LoginComponent } from '../managements/login/login.component';
import { AuthGuard } from '../services/auth-guard.service';
import { ChemicalComponent } from '../managements/chemicals/chemical/chemical.component';
import { ChemicalAddComponent } from '../managements/chemicals/chemical-add/chemical-add.component';
import { ChemicalDetailComponent } from '../managements/chemicals/chemical-detail/chemical-detail.component';
import { ChemicalOrderComponent } from '../managements/chemicals/chemical-order/chemical-order.component';
import { MaterialComponent } from '../managements/materials/material/material.component';
import { MaterialOrderComponent } from '../managements/materials/material-order/material-order.component';
import { ProductComponent } from '../managements/products/product/product.component';
import { ProductAddComponent } from '../managements/products/product-add/product-add.component';

const routes: Routes = [
  { path: '', component: HomeComponent, pathMatch: 'full' },
  { path: 'management/login', component: LoginComponent, pathMatch: 'full' },
  { path: 'management/dashboard', component: DashboardComponent, pathMatch: 'full', canActivate: [AuthGuard] },
  { path: 'management/master-data', component: MasterDataComponent, pathMatch: 'full', canActivate: [AuthGuard] },
  { path: 'management/contact', component: ContactsComponent, pathMatch: 'full', canActivate: [AuthGuard] },
  { path: 'management/chemical', component: ChemicalComponent, pathMatch: 'full', canActivate: [AuthGuard] },
  { path: 'management/chemical/add', component: ChemicalAddComponent, pathMatch: 'full', canActivate: [AuthGuard] },
  { path: 'management/chemical/edit/:id', component: ChemicalAddComponent, pathMatch: 'full', canActivate: [AuthGuard] },
  { path: 'management/chemical/detail/:id', component: ChemicalDetailComponent, pathMatch: 'full', canActivate: [AuthGuard] },
  { path: 'management/chemical-order', component: ChemicalOrderComponent, pathMatch: 'full', canActivate: [AuthGuard] },
  { path: 'management/chemical-order/add', component: ChemicalOrderAddComponent, pathMatch: 'full', canActivate: [AuthGuard] },
  { path: 'management/chemical-order/edit/:id', component: ChemicalOrderAddComponent, pathMatch: 'full', canActivate: [AuthGuard] },
  { path: 'management/material', component: MaterialComponent, pathMatch: 'full', canActivate: [AuthGuard] },
  { path: 'management/material/add', component: MaterialAddComponent, pathMatch: 'full', canActivate: [AuthGuard] },
  { path: 'management/material/edit/:id', component: MaterialAddComponent, pathMatch: 'full', canActivate: [AuthGuard] },
  { path: 'management/material/detail/:id', component: MaterialDetailComponent, pathMatch: 'full', canActivate: [AuthGuard] },
  { path: 'management/material-order', component: MaterialOrderComponent, pathMatch: 'full', canActivate: [AuthGuard] },
  { path: 'management/material-order/add', component: MaterialOrderAddComponent, pathMatch: 'full', canActivate: [AuthGuard] },
  { path: 'management/material-order/edit/:id', component: MaterialOrderAddComponent, pathMatch: 'full', canActivate: [AuthGuard] },
  { path: 'management/product', component: ProductComponent, pathMatch: 'full', canActivate: [AuthGuard] },
  { path: 'management/product/add', component: ProductAddComponent, pathMatch: 'full', canActivate: [AuthGuard] },
  { path: 'management/product/edit/:id', component: ProductAddComponent, pathMatch: 'full', canActivate: [AuthGuard] },
  { path: 'management/product/detail/:id', component: ProductDetailComponent, pathMatch: 'full', canActivate: [AuthGuard] },
  { path: 'management/product-order', component: ProductOrderComponent, pathMatch: 'full', canActivate: [AuthGuard] },
  { path: 'management/product-order/add', component: ProductOrderAddComponent, pathMatch: 'full', canActivate: [AuthGuard] },
  { path: 'management/product-order/edit/:id', component: ProductOrderAddComponent, pathMatch: 'full', canActivate: [AuthGuard] },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ],
  declarations: []
})

export class RoutingModule { }
