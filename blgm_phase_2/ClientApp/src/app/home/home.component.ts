import { ContactService } from './../services/contact.service';
import { ProductsModalsComponent } from './../modals/products-modals/products-modals.component';
import { element } from 'protractor';
import { Component, AfterViewInit, HostListener, ElementRef, ViewChild } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ServicesModalsComponent } from '../modals/services-modals/services-modals.component';
import { ToastrService } from 'ngx-toastr';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements AfterViewInit {
  // tslint:disable-next-line:max-line-length
  images = ['assets/slideShows/5.jpg', 'assets/slideShows/1.jpg', 'assets/slideShows/2.jpg', 'assets/slideShows/3.jpg', 'assets/slideShows/4.jpg'];
  aboutElement: HTMLElement;
  servicesElement: HTMLElement;
  galleryElement: HTMLElement;
  productElement: HTMLElement;
  contactElement: HTMLElement;

  public aboutOffset: Number = null;
  public serviceOffset: Number = null;
  public galleryOffset: Number = null;
  public productOffset: Number = null;
  public contactOffset: Number = null;

  @ViewChild('contactform')
  myContactform;

  contactForm = new FormGroup({
    firstName: new FormControl('', Validators.required),
    lastName: new FormControl('', Validators.required),
    email: new FormControl('', Validators.required),
    comment: new FormControl('', Validators.required),
  });

  constructor(private modalService: NgbModal
    , private contactService: ContactService
    , private toastr: ToastrService) {}

  ngAfterViewInit() {
    this.aboutElement = document.getElementById('about');
    this.servicesElement = document.getElementById('services');
    this.galleryElement = document.getElementById('gallery');
    this.productElement = document.getElementById('product');
    this.contactElement = document.getElementById('contact');
    this.aboutOffset = this.aboutElement.offsetTop;
    this.serviceOffset = this.servicesElement.offsetTop;
    this.galleryOffset = this.galleryElement.offsetTop;
    this.productOffset = this.productElement.offsetTop;
    this.contactOffset = this.contactElement.offsetTop;
  }


  @HostListener('window:scroll', ['$event']) // for window scroll events
  onScrolls(event) {
    console.log(window.pageYOffset);
    console.log( this.aboutOffset);
    const aboutNavElement = document.getElementById('about-nave');
    const servicesNavElement = document.getElementById('service-nave');
    const galleryNavElement = document.getElementById('gallery-nave');
    const productNavElement = document.getElementById('product-nave');
    const contactNavElement = document.getElementById('contact-nave');
    const homeNavElement = document.getElementById('home-nave');
    if (window.pageYOffset >= +this.aboutOffset + 600 && window.pageYOffset < +this.serviceOffset + 600) {
      aboutNavElement.className = 'nav-active';
      servicesNavElement.className = 'nav-unactive';
      galleryNavElement.className = 'nav-unactive';
      productNavElement.className = 'nav-unactive';
      contactNavElement.className = 'nav-unactive';
      homeNavElement.className = 'nav-unactive';
    } else if (window.pageYOffset >= +this.serviceOffset + 600 && window.pageYOffset < +this.galleryOffset + 600) {
      aboutNavElement.className = 'nav-unactive';
      servicesNavElement.className = 'nav-active';
      galleryNavElement.className = 'nav-unactive';
      productNavElement.className = 'nav-unactive';
      contactNavElement.className = 'nav-unactive';
      homeNavElement.className = 'nav-unactive';
    } else if (window.pageYOffset >= +this.galleryOffset + 600 && window.pageYOffset < +this.productOffset + 700) {
      aboutNavElement.className = 'nav-unactive';
      servicesNavElement.className = 'nav-unactive';
      galleryNavElement.className = 'nav-active';
      productNavElement.className = 'nav-unactive';
      contactNavElement.className = 'nav-unactive';
      homeNavElement.className = 'nav-unactive';
    } else if (window.pageYOffset >= +this.productOffset + 700 && window.pageYOffset < +this.contactOffset + 1000) {
      aboutNavElement.className = 'nav-unactive';
      servicesNavElement.className = 'nav-unactive';
      galleryNavElement.className = 'nav-unactive';
      productNavElement.className = 'nav-active';
      contactNavElement.className = 'nav-unactive';
      homeNavElement.className = 'nav-unactive';
    }  else if (window.pageYOffset >= +this.contactOffset + 1000) {
      aboutNavElement.className = 'nav-unactive';
      servicesNavElement.className = 'nav-unactive';
      galleryNavElement.className = 'nav-unactive';
      productNavElement.className = 'nav-unactive';
      contactNavElement.className = 'nav-active';
      homeNavElement.className = 'nav-unactive';
    } else {
      aboutNavElement.className = 'nav-unactive';
      servicesNavElement.className = 'nav-unactive';
      galleryNavElement.className = 'nav-unactive';
      productNavElement.className = 'nav-unactive';
      contactNavElement.className = 'nav-unactive';
      homeNavElement.className = 'nav-active';
    }
  }

  openProductModal(type, title) {
    const modalRef = this.modalService.open(ProductsModalsComponent, { size : 'lg'});
    modalRef.componentInstance.type = type;
    modalRef.componentInstance.title = title;
  }

  openServiceModal(type) {
    const modalRef = this.modalService.open(ServicesModalsComponent, { size : 'lg', windowClass: 'tran-modal', centered: true});
    modalRef.componentInstance.type = type;
  }

  addContact() {
    const data = {
      firstName: this.contactForm.get('firstName').value,
      lastName: this.contactForm.get('lastName').value,
      email: this.contactForm.get('email').value,
      comment: this.contactForm.get('comment').value
    };
    if (this.contactForm.valid) {
      this.contactService.addContact(data)
        .subscribe((res: any) => {
          this.toastr.success( 'we will reply as soon as possible.', 'Thanks for contacting us.');
          this.myContactform.resetForm();
        });
    }
  }

}
