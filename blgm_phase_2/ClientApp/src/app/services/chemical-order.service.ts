import { JwtHelperService } from '@auth0/angular-jwt';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class ChemicalOrderService {

    constructor(private http: HttpClient) { }

    public add(body: any) {
        return this.http.post('api/chemicalOrder/add', body);
    }

    public update(body: any) {
        return this.http.post('api/chemicalOrder/update', body);
    }

    public orderComplete(id: any) {
        return this.http.get('api/chemicalOrder/OrderComlpete/' + id);
    }

    public getAll() {
        return this.http.get('api/chemicalOrder/');
    }

    public search(search) {
        return this.http.post('api/chemicalOrder/search', search);
    }

    public getAllCompleteToday() {
        return this.http.get('api/chemicalOrder/CompleteToday');
    }

    public delete(id) {
        return this.http.delete('api/chemicalOrder/' + id);
    }

    public get(id) {
        return this.http.get('api/chemicalOrder/' + id);
    }

}
