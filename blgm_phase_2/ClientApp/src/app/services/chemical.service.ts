import { JwtHelperService } from '@auth0/angular-jwt';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class ChemicalService {

    constructor(private http: HttpClient) { }

    public add(body: any) {
        return this.http.post('api/chemical/add', body);
    }

    public update(body: any) {
        return this.http.post('api/chemical/update', body);
    }

    public getAll() {
        return this.http.get('api/chemical/');
    }

    public search(searchCriteria) {
        return this.http.post('api/chemical/search', searchCriteria);
    }

    public outOfStock() {
        return this.http.get('api/chemical/OutOfStock');
    }

    public getAllHistory(id) {
        return this.http.get('api/chemicalHistory/' + id);
    }

    public delete(id) {
        return this.http.delete('api/chemical/' + id);
    }

    public get(id) {
        return this.http.get('api/chemical/' + id);
    }

    public getMaxPackageValue(id, orderId) {
        if (orderId === undefined) {
            orderId = 0;
        }
        return this.http.get('api/chemical/package?id=' + id + '&orderId=' + orderId);
    }

}
