import { JwtHelperService } from '@auth0/angular-jwt';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class MaterialService {

    constructor(private http: HttpClient) { }

    public add(body: any) {
        return this.http.post('api/material/add', body);
    }

    public update(body: any) {
        return this.http.post('api/material/update', body);
    }

    public getAll() {
        return this.http.get('api/material/');
    }
    public search(searchCriteria) {
        return this.http.post('api/material/search', searchCriteria);
    }


    public outOfStock() {
        return this.http.get('api/material/OutOfStock');
    }

    public getAllHistory(id) {
        return this.http.get('api/materialHistory/' + id);
    }

    public delete(id) {
        return this.http.delete('api/material/' + id);
    }

    public get(id) {
        return this.http.get('api/material/' + id);
    }

    public getMaxPackageValue(id, orderId) {
        if (orderId === undefined) {
            orderId = 0;
        }
        return this.http.get('api/material/package?id=' + id + '&orderId=' + orderId);
    }

}
