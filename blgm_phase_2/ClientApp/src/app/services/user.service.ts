import { JwtHelperService } from '@auth0/angular-jwt';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable()
export class UserService {
  // tslint:disable-next-line:ban-types
  public currentUserSubject: BehaviorSubject<Object>;
  // tslint:disable-next-line:ban-types
  public currentUser: Observable<Object>;

  constructor(private http: HttpClient) {
    // tslint:disable-next-line:ban-types
    this.currentUserSubject = new BehaviorSubject<Object>(JSON.parse(localStorage.getItem('currentUser')));
    this.currentUser = this.currentUserSubject.asObservable();
}
  public add(body: any) {
    return this.http.post('api/User/add', body);
  }

  public getAll() {
    return this.http.get('api/User/');
  }

  public updateDetail(data) {
    return this.http.post('api/User/update', data);
  }

  public updatePassword(data) {
    return this.http.post('api/User/changePassword', data);
  }

  public delete(id) {
    return this.http.delete('api/User/' + id);
  }

  // tslint:disable-next-line:ban-types
  public get currentUserValue(): Object {
    return this.currentUserSubject.value;
  }
  public set setCurrentUser(user) {
    console.log(user);
    this.currentUserSubject.next(user);
  }

  logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('jwt');
    localStorage.removeItem('currentUser');
    this.currentUserSubject.next(null);
  }
}
