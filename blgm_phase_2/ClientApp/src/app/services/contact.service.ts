import { JwtHelperService } from '@auth0/angular-jwt';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class ContactService {

    constructor(private http: HttpClient) { }

    public addContact(body: any) {
        return this.http.post('api/Contacts/add', body);
    }

    public getAll() {
        return this.http.get('api/Contacts/');
    }


}
