import { JwtHelperService } from '@auth0/angular-jwt';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class MaterialOrderService {

    constructor(private http: HttpClient) { }

    public add(body: any) {
        return this.http.post('api/materialOrder/add', body);
    }

    public update(body: any) {
        return this.http.post('api/materialOrder/update', body);
    }

    public orderComplete(id: any) {
        return this.http.get('api/materialOrder/OrderComlpete/' + id);
    }

    public getAll() {
        return this.http.get('api/materialOrder/');
    }

    public search(search) {
        return this.http.post('api/materialOrder/search', search);
    }

    public getAllCompleteToday() {
        return this.http.get('api/materialOrder/CompleteToday');
    }


    public delete(id) {
        return this.http.delete('api/materialOrder/' + id);
    }

    public get(id) {
        return this.http.get('api/materialOrder/' + id);
    }

}
