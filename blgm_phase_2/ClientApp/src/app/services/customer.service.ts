import { JwtHelperService } from '@auth0/angular-jwt';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class CustomerService {

    constructor(private http: HttpClient) { }

    public add(body: any) {
        return this.http.post('api/customer/add', body);
    }

    public getAll() {
        return this.http.get('api/customer/');
    }

    public getAllCoporation() {
        return this.http.get('api/customer/getAllCoporation');
    }

    public getAllSupplier() {
        return this.http.get('api/customer/GetAllSupplier');
    }

    public update(data) {
        return this.http.post('api/customer/update', data);
    }

    public delete(id) {
        return this.http.delete('api/customer/' + id);
    }

}
