import { JwtHelperService } from '@auth0/angular-jwt';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class ProductService {

    constructor(private http: HttpClient) { }

    public add(body: any) {
        return this.http.post('api/product/add', body);
    }

    public uploadImage(body: any) {
        return this.http.post('api/product/UploadImage', body);
    }

    public update(body: any) {
        return this.http.post('api/product/update', body);
    }

    public getAll() {
        return this.http.get('api/product/');
    }

    public search(searchCriteria) {
        return this.http.post('api/product/search', searchCriteria);
    }


    public getAllHistory(id) {
        return this.http.get('api/productHistory/' + id);
    }

    public outOfStock() {
        return this.http.get('api/product/OutOfStock');
    }

    public delete(id) {
        return this.http.delete('api/product/' + id);
    }

    public get(id) {
        return this.http.get('api/product/' + id);
    }

    public getMaxPackageValue(id, orderId) {
        if (orderId === undefined) {
            orderId = 0;
        }
        return this.http.get('api/product/package?id=' + id + '&orderId=' + orderId);
    }

}
