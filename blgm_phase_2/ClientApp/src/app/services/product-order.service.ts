import { JwtHelperService } from '@auth0/angular-jwt';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class ProductOrderService {

    constructor(private http: HttpClient) { }

    public add(body: any) {
        return this.http.post('api/productOrder/add', body);
    }

    public update(body: any) {
        return this.http.post('api/productOrder/update', body);
    }

    public orderComplete(id: any) {
        return this.http.get('api/productOrder/OrderComlpete/' + id);
    }

    public getAll() {
        return this.http.get('api/productOrder/');
    }

    public search(search) {
        return this.http.post('api/productOrder/search', search);
    }

    public getAllCompleteToday() {
        return this.http.get('api/productOrder/CompleteToday');
    }


    public delete(id) {
        return this.http.delete('api/productOrder/' + id);
    }

    public get(id) {
        return this.http.get('api/productOrder/' + id);
    }

}
