export class LoginModel {
    public username: string;
    public password: string;

    public constructor(object: any) {
        this.username = object.username;
        this.password = object.password;
    }
}
