using System.Collections.Generic;
using Microsoft.AspNetCore.Http;

namespace blgm_phase_2.Views
{
    public class ProductImageView
    {
        public int ProductId { get; set; }
        public IFormFile Image { get; set; }
        public byte[] ImageByte { get; set; }
    }
}